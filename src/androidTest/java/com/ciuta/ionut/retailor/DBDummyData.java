package com.ciuta.ionut.retailor;

import android.content.ContentValues;

import com.ciuta.ionut.retailor.db.RetailorDBContract;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DBDummyData {

    public static ContentValues getVenueData1() {
        ContentValues testValues = new ContentValues();
        testValues.put(RetailorDBContract.VenueEntry.COLUMN_NAME, "Carrefour");
        testValues.put(RetailorDBContract.VenueEntry.COLUMN_ADDRESS, "Spl. Independentei");
        return testValues;
    }

    public static ContentValues getVenueData2() {
        ContentValues testValues = new ContentValues();
        testValues.put(RetailorDBContract.VenueEntry.COLUMN_NAME, "Kaufland");
        testValues.put(RetailorDBContract.VenueEntry.COLUMN_ADDRESS, "Str. Valea Cascadelor");
        return testValues;
    }

    public static ContentValues getVenueData3() {
        ContentValues testValues = new ContentValues();
        testValues.put(RetailorDBContract.VenueEntry.COLUMN_NAME, "Metro");
        testValues.put(RetailorDBContract.VenueEntry.COLUMN_ADDRESS, "Str. Preciziei");
        return testValues;
    }
}
