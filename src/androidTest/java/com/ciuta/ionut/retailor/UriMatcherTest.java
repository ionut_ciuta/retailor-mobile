package com.ciuta.ionut.retailor;

import android.content.UriMatcher;
import android.test.AndroidTestCase;

import com.ciuta.ionut.retailor.db.DBContentProvider;
import com.ciuta.ionut.retailor.db.RetailorDBContract;

/**
 * Created by Ionut on 4/29/2016.
 */
public class UriMatcherTest extends AndroidTestCase {

    public void testUriMatcher() {
        UriMatcher testMatcher = DBContentProvider.initUriMatcher();
        assertEquals(testMatcher.match(RetailorDBContract.VenueEntry.CONTENT_URI), DBContentProvider.VENUE);
        assertEquals(testMatcher.match(RetailorDBContract.ShoppingListEntry.CONTENT_URI), DBContentProvider.SHOPPING_LIST);
    }
}
