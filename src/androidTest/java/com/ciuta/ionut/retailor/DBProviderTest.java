package com.ciuta.ionut.retailor;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.AndroidTestCase;
import android.util.Log;

import com.ciuta.ionut.retailor.db.DBContentProvider;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.RetailorDBContract.ListItemEntry;
import com.ciuta.ionut.retailor.db.RetailorDBContract.ProductEntry;
import com.ciuta.ionut.retailor.db.RetailorDBContract.ShoppingListEntry;
import com.ciuta.ionut.retailor.db.RetailorDBContract.UserEntry;
import com.ciuta.ionut.retailor.db.RetailorDBContract.VenueEntry;
import com.ciuta.ionut.retailor.db.RetailorDBHelper;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.pojo.Venue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DBProviderTest extends AndroidTestCase {
    private static final String TAG = "TEST";

    public void deleteAllRecordsFromDB() {
        RetailorDBHelper dbHelper = new RetailorDBHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(VenueEntry.TABLE_NAME, null, null);
        db.delete(ShoppingListEntry.TABLE_NAME, null, null);
        db.delete(UserEntry.TABLE_NAME, null, null);
        db.delete(ProductEntry.TABLE_NAME, null, null);
        db.delete(ListItemEntry.TABLE_NAME, null, null);
        db.close();
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        deleteAllRecordsFromDB();
    }

    public void testProviderRegistry() {
        PackageManager pm = mContext.getPackageManager();
        ComponentName componentName = new ComponentName(mContext.getPackageName(),
                DBContentProvider.class.getName());

        try {
            ProviderInfo providerInfo = pm.getProviderInfo(componentName, 0);
            assertEquals(
                    "Error: DBContentProvider registered with authority: " +
                    providerInfo.authority + " instead of authority " + RetailorDBContract.CONTENT_AUTHORITY,
                    providerInfo.authority, RetailorDBContract.CONTENT_AUTHORITY
            );
        } catch (PackageManager.NameNotFoundException e) {
            assertTrue("error: DBProvider is not registered at " + mContext.getPackageName(),
                    false);
        }
    }

    public void testQueryVenues() {
        RetailorDBHelper dbHelper = new RetailorDBHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues venueContentValues = DBDummyData.getVenueData1();
        long id = db.insert(RetailorDBContract.VenueEntry.TABLE_NAME, null, venueContentValues);
        assertTrue("Insertion was successful", id > 0);

        Cursor cursor = mContext.getContentResolver().query(
                RetailorDBContract.VenueEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        assertTrue("Query was successful", cursor != null);
        assertTrue("1 row", cursor.getCount() == 1);

        if(cursor.moveToNext()) {
            assertEquals(cursor.getString(1), "Carrefour");
            Log.i("TEST", cursor.getString(0));
            Log.i("TEST", cursor.getString(1));
            Log.i("TEST", cursor.getString(2));
        }

        cursor.close();
        deleteAllRecordsFromDB();
        db.close();
    }

    public void testQueryShoppingList() {

    }

    public void testInsertVenue() {
        ContentValues contentValues = DBDummyData.getVenueData1();
        TestContentObserver observer = TestContentObserver.getTestContentObserver();

        mContext.getContentResolver().registerContentObserver(VenueEntry.CONTENT_URI, true, observer);
        Uri venueUri = mContext.getContentResolver().insert(VenueEntry.CONTENT_URI, contentValues);

        observer.waitForNotificationOrFail();
        mContext.getContentResolver().unregisterContentObserver(observer);

        long venueId = ContentUris.parseId(venueUri);
        assertTrue(venueId > 0);

        Cursor cursor = mContext.getContentResolver().query(
                VenueEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        Log.i("TEST", "Cursor rows: " + cursor.getCount());
        deleteAllRecordsFromDB();
    }

    public void testUpdateAndDeleteVenues() {
        ContentValues values1, values2, values3;
        values1 = DBDummyData.getVenueData1();
        values2 = DBDummyData.getVenueData2();
        values3 = DBDummyData.getVenueData3();

        long id1 = ContentUris.parseId(mContext.getContentResolver().insert(VenueEntry.CONTENT_URI, values1));
        Log.d("TEST", "Inserted row ID: " + id1);
        assertTrue(id1 > 0);

        long id2 = ContentUris.parseId(mContext.getContentResolver().insert(VenueEntry.CONTENT_URI, values2));
        Log.d("TEST", "Inserted row ID: " + id2);
        assertTrue(id2 > 0);

        Cursor cursor = mContext.getContentResolver().query(
                VenueEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        Log.d("TEST", "Cursor row count: " + cursor.getCount());
        assertEquals(cursor.getCount(), 2);

        while(cursor.moveToNext()) {
            Log.d("TEST", cursor.getString(1) + " " + cursor.getString(2));
        }

        int updatedRows = mContext.getContentResolver().update(
                VenueEntry.CONTENT_URI,
                values3,
                VenueEntry._ID + " = ?",
                new String[]{Long.toString(id2)}
        );

        Log.d("TEST", "Updated rows count: " + updatedRows);
        assertEquals(updatedRows, 1);
        cursor.close();

        cursor = mContext.getContentResolver().query(
                VenueEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        Log.d("TEST", "Cursor row count: " + cursor.getCount());
        assertEquals(cursor.getCount(), 2);

        while(cursor.moveToNext()) {
            Log.d("TEST", cursor.getString(1) + " " + cursor.getString(2));
        }
        cursor.close();

        long deletedRows = mContext.getContentResolver().delete(
                VenueEntry.CONTENT_URI,
                null,
                null
        );
        assertEquals(deletedRows, 2);
    }

    private int updateVenue(Long id, String name) {
        ContentResolver contentResolver = getContext().getContentResolver();

        ContentValues contentValues = new ContentValues();
        contentValues.put(VenueEntry.COLUMN_NAME, name);

        return contentResolver.update(
                VenueEntry.CONTENT_URI,
                contentValues,
                VenueEntry._ID + " = ? ",
                new String[]{id.toString()});
    }

    private Uri insertVenue(Venue venue) {
        ContentResolver contentResolver = getContext().getContentResolver();

        ContentValues contentValues = new ContentValues();
        contentValues.put(VenueEntry._ID, venue.getId());
        contentValues.put(VenueEntry.COLUMN_NAME, venue.getName());
        contentValues.put(VenueEntry.COLUMN_ADDRESS, venue.getAddress());

        return contentResolver.insert(
                VenueEntry.CONTENT_URI,
                contentValues);
    }

    private Uri insertUser(User user) {
        ContentResolver contentResolver = getContext().getContentResolver();

        ContentValues contentValues = new ContentValues();
        contentValues.put(UserEntry._ID, user.getId());
        contentValues.put(UserEntry.COLUMN_EMAIL, user.getEmail());
        contentValues.put(UserEntry.COLUMN_FIRSTNAME, user.getFirstName());
        contentValues.put(UserEntry.COLUMN_LASTNAME, user.getLastName());

        return contentResolver.insert(
                UserEntry.CONTENT_URI,
                contentValues);
    }

    private Uri insertShoppingList(ShoppingList list) {
        ContentResolver contentResolver = getContext().getContentResolver();

        ContentValues contentValues = new ContentValues();
        contentValues.put(ShoppingListEntry._ID, list.getId());
        contentValues.put(ShoppingListEntry.COLUMN_NAME, list.getName());
        contentValues.put(ShoppingListEntry.COLUMN_USER_ID, list.getUser().getId());
        contentValues.put(ShoppingListEntry.COLUMN_VENUE_ID, list.getVenue().getId());

        return contentResolver.insert(
                ShoppingListEntry.CONTENT_URI,
                contentValues);
    }

    private List<ShoppingList> queryShoppingLists(User user, Venue venue) {
        ContentResolver contentResolver = getContext().getContentResolver();

        Cursor cursor = contentResolver.query(
                ShoppingListEntry.CONTENT_URI,
                new String[]{"*"},
                ShoppingListEntry.COLUMN_USER_ID + " = ? AND " + ShoppingListEntry.COLUMN_VENUE_ID + " = ? ",
                new String[]{user.getId().toString(), venue.getId().toString()},
                null
                );

        assert cursor != null;
        int nameIndex = cursor.getColumnIndex(ShoppingListEntry.COLUMN_NAME);

        List<ShoppingList> result = new ArrayList<>();
        while(cursor.moveToNext()) {
            ShoppingList list = new ShoppingList();
            list.setName(cursor.getString(nameIndex));
            result.add(list);
        }

        return result;
    }

    private List<Venue> queryVenues() {
        ContentResolver contentResolver = getContext().getContentResolver();

        Cursor cursor = contentResolver.query(
                VenueEntry.CONTENT_URI,
                new String[]{"*"},
                null,
                null,
                null);
        int nameIndex = cursor.getColumnIndex(VenueEntry.COLUMN_NAME);

        List<Venue> result = new ArrayList<>();
        while(cursor.moveToNext()) {
            Venue venue = new Venue();
            venue.setName(cursor.getString(nameIndex));
            result.add(venue);
        }

        return result;
    }
}
