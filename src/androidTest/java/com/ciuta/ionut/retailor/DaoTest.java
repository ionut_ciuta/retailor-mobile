package com.ciuta.ionut.retailor;

import android.test.AndroidTestCase;
import android.util.Log;

import com.ciuta.ionut.retailor.db.dao.UserDao;
import com.ciuta.ionut.retailor.pojo.User;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DaoTest extends AndroidTestCase {
    private final String TAG = this.getClass().getSimpleName();

    public void UserDaoTest() {
        UserDao userDao = new UserDao(mContext);

        User user1 = new User();
        user1.setId(1L);
        user1.setEmail("john@doe.com");
        user1.setFirstName("John");
        user1.setLastName("Doe");

        User user2 = new User();
        user2.setId(2L);
        user2.setEmail("jane@watson.com");
        user2.setFirstName("Jane");
        user2.setLastName("Watson");
        user2.setStatus(User.USER_ACTIVE);

        assertTrue(1L == userDao.insert(user1));
        assertTrue(2L == userDao.insert(user2));
        //assertTrue(1 == userDao.activateUser("john@doe.com"));
        Long r = userDao.getActiveUserId();
        Log.i(TAG, r.toString());
    }
}
