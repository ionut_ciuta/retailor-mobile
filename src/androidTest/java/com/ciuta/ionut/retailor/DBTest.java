package com.ciuta.ionut.retailor;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.ciuta.ionut.retailor.db.RetailorDBContract.VenueEntry;
import com.ciuta.ionut.retailor.db.RetailorDBHelper;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DBTest extends AndroidTestCase {

    @Override
    protected void setUp() throws Exception {
        mContext.deleteDatabase(RetailorDBHelper.DB_NAME);
    }

    public void testCreateDB() throws Throwable {
        SQLiteDatabase db = new RetailorDBHelper(getContext()).getWritableDatabase();
        assertEquals(true, db.isOpen());
    }

    public void testInsertDB() throws Throwable {
        SQLiteDatabase db = new RetailorDBHelper(getContext()).getWritableDatabase();
        ContentValues testValues = DBDummyData.getVenueData1();
        long id = db.insert(VenueEntry.TABLE_NAME, null, testValues);
        assertEquals(id != -1, true);

        Cursor c = db.query(VenueEntry.TABLE_NAME,
                new String[]{"*"},
                null,
                null,
                null,
                null,
                null);

        assertTrue(c != null);

        if(c.moveToNext()) {
            assertEquals(c.getString(1), "Carrefour");
            Log.i("JUnit", c.getString(0));
            Log.i("JUnit", c.getString(1));
            Log.i("JUnit", c.getString(2));
        }
        c.close();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
