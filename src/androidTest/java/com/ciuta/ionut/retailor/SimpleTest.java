package com.ciuta.ionut.retailor;

import android.test.AndroidTestCase;

/**
 * Created by Ionut on 4/22/2016.
 */
public class SimpleTest extends AndroidTestCase {
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void testCase() throws Throwable {
        int a = 5;
        assertEquals("a should be 5", a, 5);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
