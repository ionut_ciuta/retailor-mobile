package com.ciuta.ionut.retailor.map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class MapHeader extends DrawerEntry {
    public static final int HEADER_TYPE_PRODUCT = 1;
    public static final int HEADER_TYPE_SALES = 2;
    public static final int HEADER_TYPE_RECOMMENDATION = 3;

    private String title;
    private int headerType;

    public MapHeader(String title, int headerType) {
        super(TYPE_HEADER);
        this.title = title;
        this.headerType = headerType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
