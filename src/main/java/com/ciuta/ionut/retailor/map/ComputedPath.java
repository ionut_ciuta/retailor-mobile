package com.ciuta.ionut.retailor.map;

import com.ciuta.ionut.retailor.pojo.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ComputedPath {
    private List<Point2D> pathCoordinates;
    private List<Pair<Product, Point2D>> listItems;
    private List<Pair<Product, Point2D>> sales;
    private List<Pair<Product, Point2D>> recommendations;

    public ComputedPath(){
        pathCoordinates = new ArrayList<>();
        listItems = new ArrayList<>();
        sales = new ArrayList<>();
        recommendations = new ArrayList<>();
    }

    public List<Point2D> getPathCoordinates() {
        return pathCoordinates;
    }

    public void setPathCoordinates(List<Point2D> pathCoordinates) {
        this.pathCoordinates = pathCoordinates;
    }

    public List<Pair<Product, Point2D>> getListItems() {
        return listItems;
    }

    public void setListItems(List<Pair<Product, Point2D>> listItems) {
        this.listItems = listItems;
    }

    public List<Pair<Product, Point2D>> getSales() {
        return sales;
    }

    public void setSales(List<Pair<Product, Point2D>> sales) {
        this.sales = sales;
    }

    public List<Pair<Product, Point2D>> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<Pair<Product, Point2D>> recommendations) {
        this.recommendations = recommendations;
    }
}
