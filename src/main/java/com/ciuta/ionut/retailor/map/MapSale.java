package com.ciuta.ionut.retailor.map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class MapSale extends DrawerEntry {
    private String text;
    private Float x, y;

    public MapSale(int index, float x, float y, String text) {
        super(TYPE_SALE, index);
        this.text = text;
        this.x = x;
        this.y = y;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }
}
