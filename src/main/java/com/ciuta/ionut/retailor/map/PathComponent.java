package com.ciuta.ionut.retailor.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class PathComponent extends View {
    private final String TAG = getClass().getSimpleName();
    private final float DEF_DIAMETER = 6;

    float xs, ys, xf, yf, d;
    boolean first;
    Paint paint = new Paint();

    public PathComponent(Context context, float xs, float ys, float xf, float yf, boolean first) {
        super(context);
        this.xs = xs;
        this.ys = ys;
        this.xf = xf;
        this.yf = yf;
        this.d = DEF_DIAMETER;
        this.first = first;
    }

    @Override
    public void onDraw(Canvas canvas) {
        paint.setAntiAlias(true);
        paint.setStrokeWidth(0);
        paint.setColor(Color.parseColor("#00695C"));

        if(first) {
            canvas.drawCircle(xs, ys, d+10, paint);
        } else {
            canvas.drawCircle(xs, ys, d, paint);
        }

        paint.setColor(Color.parseColor("#00695C"));
        canvas.drawCircle(xf, yf, d, paint);
        paint.setStrokeWidth(4);
        canvas.drawLine(xs, ys, xf, yf,paint);
    }

    @Override
    public void invalidateOutline() {
        super.invalidateOutline();
        Log.i(TAG, "PATH INVALIDATED");
    }
}
