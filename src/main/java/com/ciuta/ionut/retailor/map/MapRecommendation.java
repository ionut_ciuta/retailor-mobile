package com.ciuta.ionut.retailor.map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class MapRecommendation extends DrawerEntry {
    private String name;
    private Float x, y;

    public MapRecommendation(int index, float x, float y, String name) {
        super(TYPE_RECOMMENDATION, index);
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }
}
