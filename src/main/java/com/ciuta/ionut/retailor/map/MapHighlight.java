package com.ciuta.ionut.retailor.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class MapHighlight extends View {
    private final String TAG = getClass().getSimpleName();
    private final float DEF_DIAMETER = 14;

    public static final int  PRODUCT = 1;
    public static final int SALE = 2;
    public static final int RECOMMENDATION = 3;

    private float xs, ys, d;
    private int type;
    private Paint paint = new Paint();

    public MapHighlight(Context context, float xs, float ys, int type) {
        super(context);
        this.xs = xs;
        this.ys = ys;
        this.d = DEF_DIAMETER;
        this.type = type;
    }

    @Override
    public void onDraw(Canvas canvas) {
        paint.setAntiAlias(true);
        paint.setStrokeWidth(2);
        paint.setColor(0x000000);
        canvas.drawCircle(xs, ys, d, paint);

        switch (type) {
            case PRODUCT:
                paint.setColor(0xff0277BD);
                break;

            case SALE:
                paint.setColor(0xffF44377);
                break;

            case RECOMMENDATION:
                paint.setColor(0xffFFA726);
                break;
        }

        canvas.drawCircle(xs, ys, d-4, paint);
    }

    @Override
    public void invalidateOutline() {
        super.invalidateOutline();
        Log.i(TAG, "PATH INVALIDATED");
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
