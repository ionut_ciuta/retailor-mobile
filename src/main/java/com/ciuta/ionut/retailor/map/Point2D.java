package com.ciuta.ionut.retailor.map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class Point2D {
    protected float x;
    protected float y;

    public Point2D() {

    }

    public Point2D(float x, float y) {
        super();
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
