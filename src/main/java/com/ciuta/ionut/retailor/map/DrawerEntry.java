package com.ciuta.ionut.retailor.map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DrawerEntry {
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_PRODUCT = 1;
    public static final int TYPE_SALE = 2;
    public static final int TYPE_RECOMMENDATION = 3;

    private int type;
    private int originalListIndex;

    public DrawerEntry(int type) {
        this.type = type;
    }

    public DrawerEntry(int type, int originalListIndex) {
        this.originalListIndex = originalListIndex;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOriginalListIndex() {
        return originalListIndex;
    }

    public void setOriginalListIndex(int originalListIndex) {
        this.originalListIndex = originalListIndex;
    }
}
