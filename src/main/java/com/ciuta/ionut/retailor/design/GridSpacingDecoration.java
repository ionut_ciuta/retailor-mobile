package com.ciuta.ionut.retailor.design;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ionut on 4/19/2016.
 */
public class GridSpacingDecoration extends RecyclerView.ItemDecoration {
    private final int topBottomSpacing, leftRightSpacing;

    public GridSpacingDecoration(int topBottomSpacing, int leftRightSpacing) {
        this.topBottomSpacing = topBottomSpacing;
        this.leftRightSpacing = leftRightSpacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = topBottomSpacing;
        outRect.right = leftRightSpacing;
        outRect.left = leftRightSpacing;

        if(parent.getChildAdapterPosition(view) == 0 ||
           parent.getChildAdapterPosition(view) == 1) {
            outRect.top = topBottomSpacing;
        }
    }
}
