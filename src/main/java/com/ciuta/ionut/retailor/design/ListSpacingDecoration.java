package com.ciuta.ionut.retailor.design;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ListSpacingDecoration extends RecyclerView.ItemDecoration {
    private final int spacing;

    public ListSpacingDecoration(int spacing) {
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = spacing;

        if(parent.getChildAdapterPosition(view) == 0) {
            outRect.top = spacing;
        }
    }
}
