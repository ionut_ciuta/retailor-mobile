package com.ciuta.ionut.retailor.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.fragments.SignUpFragment.GenderHandler;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class GenderDialog extends DialogFragment {
    private final String TAG = this.getClass().getSimpleName();
    private GenderHandler mGenderHandler;
    private Context mContext;

    public static GenderDialog getInstance(Context context, GenderHandler handler) {
        GenderDialog instance = new GenderDialog();
        instance.setGenderHandler(handler);
        instance.setContext(context);
        return instance;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        final CharSequence[] values = new CharSequence[]{"M", "F"};
        builder.setTitle(getString(R.string.title_dialog_gender))
                .setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mGenderHandler.handle(values[which].toString());
                        dismiss();
                    }
                })
                .setNegativeButton(mContext.getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
        return builder.create();
    }

    public void setGenderHandler(GenderHandler mGenderHandler) {
        this.mGenderHandler = mGenderHandler;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }
}
