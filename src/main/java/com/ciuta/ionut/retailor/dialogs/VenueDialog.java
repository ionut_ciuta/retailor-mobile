package com.ciuta.ionut.retailor.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.VenueAdapter;
import com.ciuta.ionut.retailor.fragments.ShoppingListFormFragment;
import com.ciuta.ionut.retailor.pojo.Venue;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueDialog extends DialogFragment {
    private final String TAG = getClass().getSimpleName();
    private ShoppingListFormFragment.VenueHandler mVenueHandler;
    private List<Venue> mVenueList;
    private Context mContext;

    public static VenueDialog getInstance(Context context, List<Venue> venues,
                                          ShoppingListFormFragment.VenueHandler venueHandler) {
        VenueDialog instance = new VenueDialog();
        instance.setVenueHandler(venueHandler);
        instance.setContext(context);
        instance.setmVenueList(venues);
        return instance;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.dialog_venue, null);
        RecyclerView mRvVenues = (RecyclerView)rootView.findViewById(R.id.rv_dialog_venue);
        mRvVenues.setLayoutManager(new LinearLayoutManager(mContext));
        mRvVenues.setAdapter(new VenueAdapter(
                mVenueList,
                R.layout.item_dialog_venue,
                new VenueSelector() {
                    @Override
                    public void handle(Venue venue) {
                        Log.i(TAG, "VENUE SELECTED");
                        mVenueHandler.handle(venue);
                        dismiss();
                    }
                }));
        builder.setView(rootView)
               .setTitle(mContext.getString(R.string.title_dialog_venue))
               .setNegativeButton(mContext.getString(R.string.action_cancel),
                       new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialog, int which) {
                               dismiss();
                           }
                       });

        return builder.create();
    }

    public void setVenueHandler(ShoppingListFormFragment.VenueHandler mVenueHandler) {
        this.mVenueHandler = mVenueHandler;
    }

    public void setmVenueList(List<Venue> mVenueList) {
        this.mVenueList = mVenueList;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public interface VenueSelector {
        void handle(Venue venue);
    }
}
