package com.ciuta.ionut.retailor.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;

import com.ciuta.ionut.retailor.util.DateHandler;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DatePickerFragment extends DialogFragment
                                       implements DatePickerDialog.OnDateSetListener {
    private final String TAG = this.getClass().getSimpleName();
    private Date mStarDate;
    private DateHandler mDateHandler;
    private Context mContext;

    public static DatePickerFragment getInstance(Context context, DateHandler handler, Date startDate) {
        DatePickerFragment instance = new DatePickerFragment();
        instance.setStarDate(startDate);
        instance.setDateHandler(handler);
        instance.setContext(context);
        return instance;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(mContext, this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        mDateHandler.onDateSelected(calendar.getTime());
    }

    public Date getStarDate() {
        return mStarDate;
    }

    public void setStarDate(Date mStarDate) {
        this.mStarDate = mStarDate;
    }

    public DateHandler getDateHandler() {
        return mDateHandler;
    }

    public void setDateHandler(DateHandler mDateHandler) {
        this.mDateHandler = mDateHandler;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContent) {
        this.mContext = mContent;
    }
}
