package com.ciuta.ionut.retailor.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */

/* Fragments of this kind provide content to ContainerActivities */
public abstract class ContentFragment extends Fragment{
    private final String TAG = this.getClass().getSimpleName();
    protected Toolbar mToolbar;

    protected void setupToolbar(String title) {
        if(mToolbar != null) {
            AppCompatActivity parentActivity = (AppCompatActivity)getActivity();
            parentActivity.setSupportActionBar(mToolbar);
            parentActivity.getSupportActionBar().setHomeButtonEnabled(true);
            parentActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            if(title != null) {
                parentActivity.getSupportActionBar().setTitle(title);
            }

        } else {
            Log.e(TAG, "No toolbar declared in fragment!");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }
}
