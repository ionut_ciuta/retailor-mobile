package com.ciuta.ionut.retailor.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.AuthenticationActivity.AuthenticationHandler;
import com.ciuta.ionut.retailor.activities.AuthenticationActivity.FragmentHandler;
import com.ciuta.ionut.retailor.pojo.RetailorToken;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.util.StringUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class SignInFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();
    private AuthenticationHandler mAuthenticationHandler;
    private FragmentHandler mFragmentHandler;
    private String mUserEmail;

    private EditText emailField, passwordField;
    private TextInputLayout emailLayout, passwordLayout;

    public static SignInFragment getInstance(String user,
                                             AuthenticationHandler authenticationHandler,
                                             FragmentHandler fragmentHandler) {
        SignInFragment instance = new SignInFragment();
        instance.setUser(user);
        instance.setAuthenticationHandler(authenticationHandler);
        instance.setFragmentHandler(fragmentHandler);
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);

        emailField = (EditText)rootView.findViewById(R.id.email_field);
        passwordField = (EditText)rootView.findViewById(R.id.password_field);
        emailLayout = (TextInputLayout) rootView.findViewById(R.id.email_layout);
        passwordLayout = (TextInputLayout) rootView.findViewById(R.id.password_layout);

        if(mUserEmail != null) {
            emailField.setText(mUserEmail);
        }

        TextView signUpLink = (TextView)rootView.findViewById(R.id.tv_sign_up);
        signUpLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "SIGN UP");
                mFragmentHandler.handle(SignUpFragment.getInstance(mAuthenticationHandler));
            }
        });

        TextView termsAndConditionsLink = (TextView)rootView.findViewById(R.id.tv_terms);
        termsAndConditionsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "TERMS");
                mFragmentHandler.handle(new TermsAndConditionsFragment());
            }
        });

        Button signInButton = (Button)rootView.findViewById(R.id.button_sign_in);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "SIGN IN");
                if(!checkUserInput()) {
                    Log.e(TAG, "MUST FILL BOTH FIELDS");
                    return;
                }

                final User user = new User();
                user.setEmail(emailField.getText().toString());
                user.setPassword(passwordField.getText().toString());

                VolleyQueueSingleton.getInstance(getActivity()).addRequest(new VolleyRequest<>(
                        NetUtils.getLoginUrl(),
                        RetailorToken.class,
                        user,
                        new Response.Listener<RetailorToken>() {
                            @Override
                            public void onResponse(RetailorToken response) {
                                String token = response.getToken();
                                if(token == null) {
                                    Log.e(TAG, "CAN'T SIGN IN");
                                    mAuthenticationHandler.onAuthenticationError(null);
                                } else {
                                    Log.i(TAG, "SIGN IN WITH SUCCESS");
                                    mAuthenticationHandler.onAuthenticationDone(user, token);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e(TAG, "CAN'T SIGN IN");
                                mAuthenticationHandler.onAuthenticationError(error);
                            }
                        }
                ));
            }
        });

        return rootView;
    }

    private boolean checkUserInput() {
        if(StringUtils.isEmpty(emailField.getText().toString())) {
            emailLayout.setError(getString(R.string.message_error_empty_email));
            return false;
        }

        if(StringUtils.isEmpty(passwordField.getText().toString())) {
            passwordLayout.setError(getString(R.string.message_error_empty_password));
            return false;
        }

        return true;
    }

    public void setUser(String user) {
        this.mUserEmail = user;
    }

    public void setAuthenticationHandler(AuthenticationHandler authenticationHandler) {
        this.mAuthenticationHandler = authenticationHandler;
    }

    public void setFragmentHandler(FragmentHandler fragmentHandler) {
        this.mFragmentHandler = fragmentHandler;
    }
}
