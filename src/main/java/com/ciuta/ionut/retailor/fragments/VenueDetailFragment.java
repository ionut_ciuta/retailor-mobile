package com.ciuta.ionut.retailor.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.MapActivity;
import com.ciuta.ionut.retailor.activities.ShoppingListActivity;
import com.ciuta.ionut.retailor.adapters.ShoppingListCursorAdapter;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.pojo.Venue;
import com.ciuta.ionut.retailor.pojo.VenueStatus;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueDetailFragment extends ContentFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private final String TAG = getClass().getSimpleName();
    private final int ADD_SHOPPING_LIST = 1;
    private final int VENUE_LOADER = 1;
    private Venue mVenue;
    private ShoppingListCursorAdapter mShoppingListAdapter;

    public static VenueDetailFragment getInstance(Venue venue) {
        VenueDetailFragment instance = new VenueDetailFragment();
        instance.setVenue(venue);
        return instance;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "INIT LOADER");
        getLoaderManager().initLoader(VENUE_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if(getVenue() == null) {
            try {
                throw new Exception("Cannot create VenueDetailFragment without Venue param!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail_venue, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add_list:
                Log.d(TAG, "ADD SHOPPING LIST FROM VENUE_DETAIL_FRAGMENT");
                ShoppingList newShoppingList = new ShoppingList();
                newShoppingList.setVenue(mVenue);

                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.key_edit_shopping_list), newShoppingList);

                Intent intent1 = new Intent(getContext(), ShoppingListActivity.class);
                intent1.putExtra(getString(R.string.key_args), args);
                startActivityForResult(intent1, VENUE_LOADER);
                return true;

            case R.id.action_map:
                // TODO: 02.07.2016 maybe fix maps
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail_venue, container,false);

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setupToolbar(mVenue.getName());

        mShoppingListAdapter = new ShoppingListCursorAdapter(
                null,
                R.layout.item_shopping_list,
                rootView.findViewById(R.id.tvEmpty),
                new FragmentCallback() {
                    @Override
                    public void handle(ShoppingList shoppingList) {
                        Bundle args = new Bundle();
                        args.putSerializable(getString(R.string.key_view_shopping_list), shoppingList);

                        Intent intent = new Intent(getContext(), ShoppingListActivity.class);
                        intent.putExtra(getString(R.string.key_args), args);
                        startActivityForResult(intent, ADD_SHOPPING_LIST);
                    }
                }
        );

        RecyclerView mRvList = (RecyclerView) rootView.findViewById(R.id.rv);
        mRvList.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvList.setAdapter(mShoppingListAdapter);

        FloatingActionButton mFab = (FloatingActionButton)rootView.findViewById(R.id.fab_go_shop);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "GOING TO MAP");
                Intent intent = new Intent(getActivity(), MapActivity.class);
                intent.putExtra(getString(R.string.key_venue_id), mVenue.getId());
                startActivity(intent);
            }
        });

        ImageView mToolbarImageView = (ImageView)rootView.findViewById(R.id.iv_toolbar_image);
        Picasso.with(getContext())
                .load(NetUtils.getVenueIconUrl(mVenue.getId()))
                .error(R.drawable.placeholder_venue)
                .into(mToolbarImageView);

        TextView mTvAddress = (TextView) rootView.findViewById(R.id.extra_panel_address);
        mTvAddress.setText(mVenue.getAddress());

        TextView mTvSchedule = (TextView) rootView.findViewById(R.id.extra_panel_schedule);
        mTvSchedule.setText(mVenue.getVenueStatus().getMessage());

        return rootView;
    }

    @Override
    public CursorLoader onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        Long userId = IdentityHandler.getActiveUserId(getContext());

        return new CursorLoader(
                getContext(),
                RetailorDBContract.SHOPPING_LIST_VENUE_JOIN_CONTENT_URI,
                new String[]{"shopping_list.local_id, shopping_list._id, shopping_list.name, " +
                             "shopping_list.description, shopping_list.version, " +
                             "venue._id, venue.name"},
                "shopping_list.user_id = ? and venue._id = ?",
                new String[]{userId.toString(), mVenue.getId().toString()},
                RetailorDBContract.ShoppingListEntry.TABLE_NAME + "." +
                RetailorDBContract.ShoppingListEntry.COLUMN_NAME + " ASC"
        );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ADD_SHOPPING_LIST && resultCode == AppCompatActivity.RESULT_OK) {
            Log.i(TAG, "onActivityResult REFRESH LOADER");
            getLoaderManager().restartLoader(VENUE_LOADER, null, this);
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        mShoppingListAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader loader) {
        Log.i(TAG, "onLoaderReset");
        mShoppingListAdapter.swapCursor(null);
    }

    public Venue getVenue() {
        return mVenue;
    }

    public void setVenue(Venue mVenue) {
        this.mVenue = mVenue;
    }

    public interface FragmentCallback {
        void handle(ShoppingList shoppingList);
    }
}
