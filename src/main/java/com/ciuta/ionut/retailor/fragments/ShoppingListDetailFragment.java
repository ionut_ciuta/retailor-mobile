package com.ciuta.ionut.retailor.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.CatalogueActivity;
import com.ciuta.ionut.retailor.activities.MapActivity;
import com.ciuta.ionut.retailor.activities.ShoppingListActivity.FragmentHandler;
import com.ciuta.ionut.retailor.adapters.ListItemCursorAdapter;
import com.ciuta.ionut.retailor.db.CursorConverter;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.dao.ListItemDao;
import com.ciuta.ionut.retailor.db.dao.ProductDao;
import com.ciuta.ionut.retailor.pojo.Cart;
import com.ciuta.ionut.retailor.pojo.ListItem;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.util.StringUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingListDetailFragment extends ContentFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private final String TAG = this.getClass().getSimpleName();
    private final int LIST_ITEM_LOADER = 1;
    public  final int ADD_PRODUCTS = 1;

    private ShoppingList mShoppingList;
    private List<ListItem> mListItems;

    private FragmentHandler fragmentHandler;
    private ListItemCursorAdapter mListItemAdapter;
    private View mRootView;

    private boolean mFlagDirtyList = false;
    private int mFlagItemsCount = -1;

    private ListItemDao listItemDao;
    private ProductDao productDao;

    private ProgressBar mProgressBar;

    public static ShoppingListDetailFragment getInstance(ShoppingList shoppingList,
                                                         FragmentHandler fragmentHandler) {
        ShoppingListDetailFragment instance = new ShoppingListDetailFragment();
        instance.setFragmentHandler(fragmentHandler);
        instance.setShoppingList(shoppingList);
        return instance;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(LIST_ITEM_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        listItemDao = new ListItemDao(getContext());
        productDao  = new ProductDao(getContext());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                Log.d(TAG, "BACK PRESSED");
                getActivity().finish();
                return true;

            case R.id.action_edit_list:
                Log.d(TAG, "EDIT PRESSED");
                fragmentHandler.handle(ShoppingListFormFragment.getInstance(mShoppingList));
                return true;

            case R.id.action_shop:
                Log.d(TAG, "SHOP PRESSED");
                if(mShoppingList.getProducts().size() == 0) {
                    Snackbar.make(mRootView,
                            getString(R.string.message_no_products), Snackbar.LENGTH_LONG).show();
                    return true;
                }

                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.key_list_id), mShoppingList);

                Intent intent = new Intent(getActivity(), MapActivity.class);
                intent.putExtra(getString(R.string.key_args), args);

                startActivity(intent);
                return true;

            default:
                Log.d(TAG, "NOTHING PRESSED");
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_detail_shopping_list, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        setupToolbar(mShoppingList.getName());

        mProgressBar = (ProgressBar)mRootView.findViewById(R.id.progressBar);

        TextView mExtraVenueName = (TextView)mRootView.findViewById(R.id.extra_panel_venue);
        mExtraVenueName.setText(mShoppingList.getVenue().getName());

        TextView mExtraCreationDate = (TextView) mRootView.findViewById(R.id.extra_panel_date);
        //mExtraCreationDate.setText(StringUtils.formatDate(mShoppingList.getCreationDate()));

        TextView mExtraDescription = (TextView) mRootView.findViewById(R.id.extra_panel_description);
        mExtraDescription.setText(mShoppingList.getDescription());

        mListItemAdapter = new ListItemCursorAdapter(
                null,
                R.layout.item_simple_product,
                mRootView.findViewById(R.id.tvEmpty),
                new ProductListFragment.Callback() {
                    @Override
                    public void handle(Product product) {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public void handle(ListItem item) {
                        Log.d(TAG, "ITEM TAPPED FROM SHOPPING_LIST_DETAIL_FRAGMENT");
                    }
                });
        RecyclerView mRvList = (RecyclerView)mRootView.findViewById(R.id.rvProducts);
        mRvList.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvList.setAdapter(mListItemAdapter);

        FloatingActionButton mFab = (FloatingActionButton) mRootView.findViewById(R.id.fab_add_product);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "ADD PRODUCT");
                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.key_venue), mShoppingList.getVenue());

                Intent intent = new Intent(getContext(), CatalogueActivity.class);
                intent.putExtra(getString(R.string.key_args), args);
                startActivityForResult(intent, ADD_PRODUCTS);
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mFlagItemsCount == -1) {
            mFlagItemsCount = listItemDao.count(mShoppingList.getLocalId());
        }

        if(mFlagItemsCount == 0 && mShoppingList.getId() != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            String token = IdentityHandler.getRetailorToken(getContext());

            VolleyQueueSingleton.getInstance(getContext()).addRequest(new VolleyRequest<>(
                    NetUtils.getShoppingListUrl(mShoppingList.getId()),
                    ShoppingList.class,
                    NetUtils.getAuthenticationHeader(token),
                    new Response.Listener<ShoppingList>() {
                        @Override
                        public void onResponse(ShoppingList response) {
                            Log.i(TAG, "FETCHED LIST CONTENT");
                            if(response.getProducts().size() == 0) {
                                mFlagItemsCount = 1;
                            } else {
                                List<Product> products = extractProductsFromList(response);
                                productDao.bulkInsertWithIndexCheck(products);
                                listItemDao.bulkInsert(response.getProducts());
                                refreshLoader();
                                mProgressBar.setVisibility(View.GONE);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ErrorHandler.handleVolleyError(error, getActivity());
                            mProgressBar.setVisibility(View.GONE);
                        }
                    }
            ).setTag(getString(R.string.volley_tag_items)));
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        return new CursorLoader(
                getContext(),
                RetailorDBContract.LIST_ITEM_PRODUCT_JOIN_CONTENT_URI,
                new String[]{"list_item.local_id, list_item._id", "list_item.taken",
                             "product.name", "product.price", "product._id"},
                "list_item.list_id = ?",
                new String[]{mShoppingList.getLocalId().toString()},
                RetailorDBContract.ProductEntry.COLUMN_NAME + " ASC"
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        mListItemAdapter.swapCursor(data);
        data.moveToPosition(-1);
        setListItems(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        mListItemAdapter.swapCursor(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ADD_PRODUCTS && resultCode == AppCompatActivity.RESULT_OK) {
            final Cart cart = (Cart) data
                        .getBundleExtra(getString(R.string.key_args))
                        .getSerializable(getString(R.string.key_added_products));

            if(cart == null) {
                Log.e(TAG, "ERROR RETRIEVING PRODUCTS");
                return;
            }

            if(cart.getProducts().size() == 0) {
                Log.e(TAG, "NO PRODUCTS ADDED");
                return;
            }

            mProgressBar.setVisibility(View.VISIBLE);
            Log.d(TAG, "PRODUCTS WERE ADDED: " + cart.getProducts().toString());
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    Log.d(TAG, "ASYNC ADDING PRODUCTS");
                    String token = IdentityHandler.getRetailorToken(getActivity());
                    ShoppingList shoppingList = getDummyShoppingList();


                    List<ListItem> newItems = createListItems(shoppingList, cart.getProducts());
                    mShoppingList.getProducts().addAll(newItems);

                    VolleyQueueSingleton.getInstance(getActivity()).addRequest(new VolleyRequest<>(
                            NetUtils.getShoppingListsUrl(),
                            ShoppingList.class,
                            mShoppingList,
                            NetUtils.getAuthenticationHeader(token),
                            new Response.Listener<ShoppingList>() {
                                @Override
                                public void onResponse(ShoppingList response) {
                                    Log.i(TAG, "SAVED: INSERTING NEW PRODUCTS");
                                    mListItems = new ArrayList<>();

                                    List<Product> newProducts = extractProductsFromList(response);
                                    productDao.bulkInsertWithIndexCheck(newProducts);

                                    Log.i(TAG, "SAVED: CLEANING LIST; INSERTING NEW ITEMS");
                                    listItemDao.deleteFromList(mShoppingList.getId());
                                    listItemDao.bulkInsert(response.getProducts());
                                    refreshLoader();
                                    mProgressBar.setVisibility(View.GONE);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    ErrorHandler.handleVolleyError(error, getActivity());
                                    mProgressBar.setVisibility(View.GONE);
                                }
                            }
                    ).setTag(getString(R.string.volley_tag_items)));

                    return null;
                }
            }.execute();
        } else {
            Log.e(TAG, "PRODUCTS WERE NOT ADDED");
        }
    }

    private void refreshLoader() {
        getLoaderManager().restartLoader(LIST_ITEM_LOADER, null, this);
    }

    /**
     * Transform the cursor into items for the current list; needed to save the list from form
     * @param cursor
     */
    private void setListItems(Cursor cursor) {
        mListItems = new ArrayList<>();
        while(cursor.moveToNext()) {
            mListItems.add(CursorConverter.toListItem(cursor));
        }
        mShoppingList.setProducts(mListItems);
        Log.i(TAG, "SET PRODUCTS");
    }

    /**
     * Transform a list of product into a list of items for a given list
     * @param list
     * @param products
     * @return
     */
    private List<ListItem> createListItems(ShoppingList list, List<Product> products) {
        List<ListItem> result = new ArrayList<>();
        List<Product> listProducts = extractProductsFromList(list);
        boolean found = false;

        for(Product newProduct : products) {
            for (Product listProduct : listProducts) {
                if(newProduct.getId().equals(listProduct.getId())) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                ListItem listItem = new ListItem();
                listItem.setShoppingList(list);
                listItem.setProduct(newProduct);
                result.add(listItem);
            }
        }

        return result;
    }

    /**
     * Produce a dummy list with the ids of the current list
     * @return
     */
    private ShoppingList getDummyShoppingList() {
        ShoppingList dummyList = new ShoppingList();
        dummyList.setId(mShoppingList.getId());
        dummyList.setLocalId(mShoppingList.getLocalId());
        return dummyList;
    }

    /**
     * Extract all products from a list
     * @param list
     * @return
     */
    private List<Product> extractProductsFromList(ShoppingList list) {
        List<Product> products = new ArrayList<>();
        for(ListItem listItem : list.getProducts()) {
            products.add(listItem.getProduct());
            listItem.setShoppingList(mShoppingList);
        }
        return products;
    }

    public void setShoppingList(ShoppingList mShoppingList) {
        this.mShoppingList = mShoppingList;
    }

    public void setFragmentHandler(FragmentHandler fragmentHandler) {
        this.fragmentHandler = fragmentHandler;
    }
}
