package com.ciuta.ionut.retailor.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class DrawerFragment extends Fragment {
    private final String TAG = this.getClass().toString();

    @SuppressWarnings("ConstantConditions")
    protected void setTitle(String title) {
        if(((AppCompatActivity)getActivity()).getSupportActionBar() != null) {
            Log.d(TAG, "ACTIVITY TILE SET FROM DRAWER_FRAGMENT");
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(title);
        }
    }
}
