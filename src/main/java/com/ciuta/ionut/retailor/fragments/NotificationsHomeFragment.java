package com.ciuta.ionut.retailor.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.BarcodeActivity;
import com.ciuta.ionut.retailor.adapters.NotificationCursorAdapter;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.dao.ListItemDao;
import com.ciuta.ionut.retailor.db.dao.NotificationDao;
import com.ciuta.ionut.retailor.db.dao.ProductDao;
import com.ciuta.ionut.retailor.design.ListSpacingDecoration;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.pojo.notifications.DiscountNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RecommendationNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotification;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class NotificationsHomeFragment extends DrawerFragment implements LoaderManager.LoaderCallbacks<Cursor>{
    private final String TAG = getClass().getSimpleName();
    private final int NOTIFICATIONS_LOADER = 1;

    private RecyclerView mVenuesRv;
    private NotificationCursorAdapter mNotificationAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "INIT LOADER");
        getLoaderManager().initLoader(NOTIFICATIONS_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.title_dashboard));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_list, container, false);

        mVenuesRv = (RecyclerView) rootView.findViewById(R.id.rvHomeList);
        mNotificationAdapter = new NotificationCursorAdapter(
                getContext(),
                null,
                R.layout.item_notification_discount,
                R.layout.item_notification_recommendation,
                rootView.findViewById(R.id.tvEmpty),
                new FragmentCallback() {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

                    @Override
                    public void handleDiscount(final DiscountNotification discount) {
                        Log.d(TAG, "DISCOUNT CLICKED");
                        dialogBuilder.setMessage("Preview will only last 30 seconds.")
                                     .setTitle("Wraning")
                                     .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                         @Override
                                         public void onClick(DialogInterface dialog, int which) {
                                             Bundle args = new Bundle();
                                             args.putSerializable(getString(R.string.key_discount), discount);

                                             Intent intent = new Intent(getActivity(), BarcodeActivity.class);
                                             intent.putExtra(getString(R.string.key_args), args);
                                             startActivity(intent);
                                         }
                                     })
                                     .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                         @Override
                                         public void onClick(DialogInterface dialog, int which) {
                                             dialog.dismiss();
                                         }
                                     });
                        dialogBuilder.create().show();
                    }

                    @Override
                    public void handleRecommendation(final RecommendationNotification recommendation) {
                        Log.d(TAG, "RECOMMENDATION CLICKED");
                        dialogBuilder.setMessage("Product will be added to list.")
                                     .setTitle("Warning")
                                     .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                         @Override
                                         public void onClick(DialogInterface dialog, int which) {
                                             addRecommendedProduct(recommendation);
                                         }
                                     })
                                     .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                         @Override
                                         public void onClick(DialogInterface dialog, int which) {
                                             dialog.dismiss();
                                         }
                                     });
                        dialogBuilder.create().show();
                    }

                    @Override
                    public void dismissNotification(RetailorNotification notification) {
                        new NotificationDao(getContext()).delete(notification);
                    }
                });

        mVenuesRv.setAdapter(mNotificationAdapter);
        mVenuesRv.addItemDecoration(new ListSpacingDecoration(22));
        mVenuesRv.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

    public void addRecommendedProduct(RecommendationNotification recommendation) {
        final ProductDao productDao = new ProductDao(getContext());
        ListItemDao listItemDao = new ListItemDao(getContext());

        int count = productDao.count("_id = ?", new String[]{recommendation.getProductId().toString()});
        if(count == 0) {
            Log.i(TAG, "RECOMMENDED PRODUCT NOT PRESENT - MUST FETCH");
            VolleyQueueSingleton.getInstance(getContext()).addRequest(new VolleyRequest<Product>(
                    NetUtils.getProductsUrl(0L), //replace this
                    Product.class,
                    new Response.Listener<Product>() {
                        @Override
                        public void onResponse(Product response) {
                            productDao.insert(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            ));
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        return new CursorLoader(
                getContext(),
                RetailorDBContract.NotificationEntry.CONTENT_URI,
                new String[]{"*"},
                "user_id = ?",
                new String[]{IdentityHandler.getActiveUserId(getActivity()).toString()},
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        mNotificationAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        mNotificationAdapter.swapCursor(null);
    }

    public interface FragmentCallback {
        void handleDiscount(DiscountNotification discount);
        void handleRecommendation(RecommendationNotification recommendation);
        void dismissNotification(RetailorNotification notification);
    }
}
