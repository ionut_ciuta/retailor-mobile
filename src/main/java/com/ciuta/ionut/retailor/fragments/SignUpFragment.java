package com.ciuta.ionut.retailor.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.AuthenticationActivity.AuthenticationHandler;
import com.ciuta.ionut.retailor.db.dao.UserDao;
import com.ciuta.ionut.retailor.dialogs.DatePickerFragment;
import com.ciuta.ionut.retailor.dialogs.GenderDialog;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.util.DateHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.util.StringUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

import java.util.Date;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class SignUpFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();
    private AuthenticationHandler mAuthenticationHandler;
    private EditText firstName, lastName, email, password, birthDate, gender;
    private User user = new User();

    public static SignUpFragment getInstance(AuthenticationHandler handler) {
        SignUpFragment instance = new SignUpFragment();
        instance.setAuthenticationHandler(handler);
        return instance;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);

        firstName = (EditText) rootView.findViewById(R.id.first_name_field);
        lastName = (EditText) rootView.findViewById(R.id.last_name_field);
        email = (EditText) rootView.findViewById(R.id.email_field);
        password = (EditText) rootView.findViewById(R.id.password_field);
        birthDate = (EditText) rootView.findViewById(R.id.birthday_field);
        gender = (EditText) rootView.findViewById(R.id.gender_field);

        birthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment datePicker = DatePickerFragment.getInstance(
                        getActivity(),
                        new DateHandler() {
                            @Override
                            public void onDateSelected(Date date) {
                                birthDate.setText(StringUtils.formatDate(date));
                                user.setBirthDate(date);
                            }
                        },
                        null
                );
                datePicker.show(getFragmentManager(), null);
            }
        });

        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GenderDialog genderDialog = GenderDialog.getInstance(
                        getActivity(),
                        new GenderHandler() {
                            @Override
                            public void handle(String result) {
                                user.setGender(result);
                                gender.setText(result);
                            }
                        });
                genderDialog.show(getFragmentManager(), null);
            }
        });

        Button createAccountButton = (Button)rootView.findViewById(R.id.button_create_account);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkFields(rootView)) {
                    createAccount();
                }
            }
        });

        return rootView;
    }

    private boolean checkFields(View v) {
        boolean r;
        r = checkField(firstName, (TextInputLayout)v.findViewById(R.id.first_name_layout));
        r = checkField(lastName, (TextInputLayout)v.findViewById(R.id.last_name_layout)) && r;
        r = checkField(email, (TextInputLayout)v.findViewById(R.id.email_layout))        && r;
        r = checkField(password, (TextInputLayout)v.findViewById(R.id.password_layout))  && r;
        r = checkField(birthDate, (TextInputLayout)v.findViewById(R.id.birthday_layout)) && r;
        r = checkField(gender, (TextInputLayout)v.findViewById(R.id.gender_layout))      && r;
        return r;
    }

    private boolean checkField(EditText field, TextInputLayout layout) {
        String value = field.getText().toString();
        if(!StringUtils.isEmpty(value)) {
            return true;
        } else {
            layout.setError(getString(R.string.message_error_empty));
            return false;
        }
    }

    private void createAccount() {
        Log.d(TAG, "CREATE ACCOUNT");
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());
        user.setFirstName(firstName.getText().toString());
        user.setLastName(lastName.getText().toString());

        VolleyQueueSingleton.getInstance(getActivity()).addRequest(new VolleyRequest<User>(
                NetUtils.getSignupUrl(),
                User.class,
                user,
                new Response.Listener<User>() {
                    @Override
                    public void onResponse(User response) {
                        new UserDao(getActivity()).insert(response);
                        Log.i(TAG, "RECEIVED TOKEN O SIGNUP: " + response.getToken());
                        response.setPassword(user.getPassword());
                        mAuthenticationHandler.onAuthenticationDone(response, response.getToken());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "CAN'T SIGN UP");
                        mAuthenticationHandler.onAuthenticationError(null);
                    }
                }
        ));
    }

    public void setAuthenticationHandler(AuthenticationHandler handler) {
        this.mAuthenticationHandler = handler;
    }

    public interface GenderHandler {
        void handle(String gender);
    }
}
