package com.ciuta.ionut.retailor.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.db.dao.ShoppingListDao;
import com.ciuta.ionut.retailor.db.dao.UserDao;
import com.ciuta.ionut.retailor.db.dao.VenueDao;
import com.ciuta.ionut.retailor.dialogs.VenueDialog;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.pojo.Venue;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.util.StringUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingListFormFragment extends ContentFragment {
    private final String TAG = this.getClass().getSimpleName();
    private ShoppingList mShoppingList;
    private EditText name, description;

    public static ShoppingListFormFragment getInstance(ShoppingList shoppingList) {
        ShoppingListFormFragment instance = new ShoppingListFormFragment();
        instance.setShoppingList(shoppingList);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                if(!getActivity().getSupportFragmentManager().popBackStackImmediate()) {
                    getActivity().finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_form_shopping_list, container, false);
        mToolbar = (Toolbar)rootView.findViewById(R.id.toolbar);
        setupToolbar("");

        FloatingActionButton doneFab = (FloatingActionButton) rootView.findViewById(R.id.fab_submit_list);
        doneFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!checkIfSaveIsPossible()) {
                    return;
                }

                Log.d(TAG, "ADDING SHOPPING LIST");
                rootView.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                VolleyQueueSingleton.getInstance(getActivity()).addRequest(new VolleyRequest<ShoppingList>(
                        NetUtils.getShoppingListsUrl(),
                        ShoppingList.class,
                        mShoppingList,
                        NetUtils.getAuthenticationHeader(IdentityHandler.getRetailorToken(getActivity())),
                        new Response.Listener<ShoppingList>() {
                            @Override
                            public void onResponse(ShoppingList response) {
                                Log.d(TAG, "SAVED BY JESUS");
                                response.setLocalId(mShoppingList.getLocalId());
                                mShoppingList = response;
                                saveList();
                                if (!getActivity().getSupportFragmentManager().popBackStackImmediate()) {
                                    getActivity().finish();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ErrorHandler.handleVolleyError(error, getActivity());
                                rootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
                            }
                        }

                ));
            }
        });

        name = (EditText)rootView.findViewById(R.id.form_list_name);
        name.setText(mShoppingList.getName());

        description = (EditText)rootView.findViewById(R.id.form_list_description);
        description.setText(mShoppingList.getDescription());

        final TextView mTvDate = (TextView)rootView.findViewById(R.id.form_field_createdon);
        mTvDate.setText(StringUtils.formatDate(mShoppingList.getCreationDate()));

        final TextView mTvVenue = (TextView)rootView.findViewById(R.id.form_field_venue);
        if(mShoppingList.getVenue() != null && mShoppingList.getVenue().getName() != null) {
            mTvVenue.setText(mShoppingList.getVenue().getName());
        }

        if(mShoppingList.getVenue() == null) {
            rootView.findViewById(R.id.form_venue_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Log.i(TAG, "SELECT VENUE");
                    List<Venue> venues = new VenueDao(getContext()).selectAllAsList();

                    if(venues.size() == 0) {
                        Log.e(TAG, "NO VENUES TO DISPLAY IN DIALOG");
                        return;
                    }

                    VenueDialog venueDialog = VenueDialog.getInstance(
                            getActivity(),
                            venues,
                            new VenueHandler() {
                                @Override
                                public void handle(Venue venue) {
                                    mShoppingList.setVenue(venue);
                                    mTvVenue.setText(venue.getName());
                                }
                            }
                    );
                    venueDialog.show(getFragmentManager(), null);
                }
            });
        }

        return rootView;
    }

    private boolean checkIfSaveIsPossible() {
        if(description.getText() != null && !StringUtils.isEmpty(description.getText().toString())) {
            mShoppingList.setDescription(description.getText().toString());
        }

        if(name.getText() != null && !StringUtils.isEmpty(name.getText().toString())) {
            mShoppingList.setName(name.getText().toString());
        } else {
            Toast.makeText(getContext(),
                    getString(R.string.message_error_empty),
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        if(mShoppingList.getVenue() == null) {
            Log.e(TAG, "VENUE NOT SELECTED");
            Toast.makeText(getContext(),
                    getContext().getString(R.string.message_error_empty_venue),
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void saveList() {
        ShoppingListDao shoppingListDao = new ShoppingListDao(getActivity());
        if(mShoppingList.getLocalId() != null) {
            shoppingListDao.update(mShoppingList);
        } else {
            Long userId = IdentityHandler.getActiveUserId(getContext());
            User user = new User();
            user.setId(userId);

            mShoppingList.setUser(user);
            Long listId = shoppingListDao.insert(mShoppingList);
            Log.i(TAG, "INSERTED SHOPPING LIST: " + listId + " FOR USER " + userId);
        }

        getActivity().setResult(Activity.RESULT_OK);
    }

    public void setShoppingList(ShoppingList shoppingList) {
        this.mShoppingList = shoppingList;
    }

    public interface VenueHandler {
        void handle(Venue venue);
    }
}
