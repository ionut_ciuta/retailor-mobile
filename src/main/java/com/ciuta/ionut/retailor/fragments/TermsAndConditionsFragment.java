package com.ciuta.ionut.retailor.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import com.ciuta.ionut.retailor.R;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class TermsAndConditionsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_terms, container, false);
        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setActionBar(mToolbar);
        getActivity().getActionBar().setTitle(getString(R.string.message_terms));

        return rootView;
    }
}
