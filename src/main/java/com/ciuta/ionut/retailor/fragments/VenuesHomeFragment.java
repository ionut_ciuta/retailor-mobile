package com.ciuta.ionut.retailor.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.VenueActivity;
import com.ciuta.ionut.retailor.adapters.VenueCursorAdapter;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.design.ListSpacingDecoration;
import com.ciuta.ionut.retailor.pojo.Venue;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenuesHomeFragment extends DrawerFragment implements LoaderManager.LoaderCallbacks<Cursor>{
    private final String TAG = getClass().getSimpleName();
    private final int VENUES_LOADER = 1;

    private RecyclerView mVenuesRv;
    private VenueCursorAdapter mVenueAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "INIT LOADER");
        getLoaderManager().initLoader(VENUES_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.title_venues));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_list, container, false);

        mVenuesRv = (RecyclerView) rootView.findViewById(R.id.rvHomeList);
        mVenueAdapter = new VenueCursorAdapter(
                getContext(),
                null,
                R.layout.item_venue,
                rootView.findViewById(R.id.tvEmpty),
                new Callback() {
                    @Override
                    public void handle(Venue venue) {
                        openDetailActivity(venue);
                    }
                }
        );

        mVenuesRv.setAdapter(mVenueAdapter);
        mVenuesRv.addItemDecoration(new ListSpacingDecoration(22));
        mVenuesRv.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        return new CursorLoader(
                getContext(),
                RetailorDBContract.VenueEntry.CONTENT_URI,
                new String[]{"*"},
                null,
                null,
                RetailorDBContract.VenueEntry.COLUMN_NAME + " ASC"
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        mVenueAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        mVenueAdapter.swapCursor(null);
    }

    private void openDetailActivity(Venue venue) {
        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.key_venue), venue);

        Intent intent = new Intent(getActivity(), VenueActivity.class);
        intent.putExtra(getString(R.string.key_args), args);

        startActivity(intent);
    }

    public interface Callback {
        void handle(Venue venue);
    }
}
