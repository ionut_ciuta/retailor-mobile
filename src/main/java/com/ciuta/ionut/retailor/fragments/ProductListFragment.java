package com.ciuta.ionut.retailor.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.CatalogueActivity.ProductHandler;
import com.ciuta.ionut.retailor.adapters.ProductAdapter;
import com.ciuta.ionut.retailor.pojo.ListItem;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.pojo.ProductCategory;
import com.ciuta.ionut.retailor.util.DummyData;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductListFragment extends ContentFragment {
    private final String TAG = this.getClass().getSimpleName();
    private ProductCategory mProductCategory;
    private ProductHandler mProductHandler;
    private ProductAdapter mProductAdapter;
    private List<Product> cachedProducts;

    public static ProductListFragment getInstance(ProductCategory productCategory,
                                                  ProductHandler productHandler) {
        ProductListFragment instance = new ProductListFragment();
        instance.setProductCategory(productCategory);
        instance.setProductHandler(productHandler);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return getActivity()
                        .getSupportFragmentManager()
                        .popBackStackImmediate();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setupToolbar(mProductCategory.getName());

        mProductAdapter = new ProductAdapter(
                new ArrayList<Product>(),
                R.layout.item_simple_product,
                rootView.findViewById(R.id.tvEmpty),
                new Callback() {
                    @Override
                    public void handle(final Product product) {
                        Log.d(TAG, "PASSING PRODUCT TO ACTIVITY");
                        mProductHandler.add(product);
                        cachedProducts.remove(cachedProducts.indexOf(product));
                        Snackbar.make(rootView,
                                      getString(R.string.message_product_added),
                                      Snackbar.LENGTH_LONG)
                                .setAction(getString(R.string.action_snackbar_undo),
                                           new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    mProductHandler.remove(product);
                                                }
                                })
                                .setActionTextColor(getResources().getColor(R.color.accent))
                                .show();
                    }

                    @Override
                    public void handle(ListItem item) {
                        throw new UnsupportedOperationException();
                    }
                }
            );

        RecyclerView mProductsRv = (RecyclerView) rootView.findViewById(R.id.rvLayout);
        mProductsRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mProductsRv.setAdapter(mProductAdapter);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

        if(cachedProducts != null) {
            mProductAdapter.setContent(cachedProducts);
            return;
        }

        VolleyQueueSingleton.getInstance(getContext()).addRequest(new VolleyRequest<>(
                NetUtils.getProductsUrl(mProductCategory.getId()),
                Product[].class,
                new Response.Listener<Product[]>() {
                    @Override
                    public void onResponse(Product[] response) {
                        Log.i(TAG, "FETCHED CATEGORY PRODUCTS");
                        cachedProducts = new ArrayList<>(Arrays.asList(response));
                        mProductAdapter.setContent(cachedProducts);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ErrorHandler.handleVolleyError(error);
                    }
                }
        ).setTag(getString(R.string.volley_tag_products)));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        VolleyQueueSingleton.cancelRequest(getString(R.string.volley_tag_products));
    }

    public void setProductCategory(ProductCategory mProductCategory) {
        this.mProductCategory = mProductCategory;
    }

    public void setProductHandler(ProductHandler productHandler) {
        this.mProductHandler = productHandler;
    }

    public interface Callback {
        void handle(ListItem item);
        void handle(Product product);
    }
}
