package com.ciuta.ionut.retailor.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.CatalogueActivity.FragmentHandler;
import com.ciuta.ionut.retailor.activities.CatalogueActivity.ProductHandler;
import com.ciuta.ionut.retailor.adapters.ProductCategoryCursorAdapter;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.dao.ProductCategoryDao;
import com.ciuta.ionut.retailor.design.ListSpacingDecoration;
import com.ciuta.ionut.retailor.pojo.ProductCategory;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

import java.util.Arrays;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductCategoryListFragment extends ContentFragment
        implements LoaderManager.LoaderCallbacks<Cursor> {
    final private String TAG = this.getClass().getSimpleName();
    final private int CATEGORY_LOADER = 1;
    private ProductCategoryCursorAdapter mProductCategoryAdapter;
    private ProductCategoryDao mProductCategoryDao;
    private FragmentHandler mFragmentHandler;
    private ProductHandler mProductHandler;
    private ProductCategory mParentCategory;
    private Long mVenueId;

    public static ProductCategoryListFragment getInstance(Long venueId,
                                                          FragmentHandler fragmentHandler,
                                                          ProductHandler productHandler) {
        ProductCategoryListFragment instance = new ProductCategoryListFragment();
        instance.setVenueId(venueId);
        instance.setFragmentHandler(fragmentHandler);
        instance.setProductHandler(productHandler);
        return instance;
    }

    public static ProductCategoryListFragment getInstance(ProductCategory parentCategory,
                                                          FragmentHandler fragmentHandler,
                                                          ProductHandler productHandler) {
        ProductCategoryListFragment instance = new ProductCategoryListFragment();
        instance.setParentCategory(parentCategory);
        instance.setFragmentHandler(fragmentHandler);
        instance.setProductHandler(productHandler);
        return instance;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        mProductCategoryDao = new ProductCategoryDao(getContext());
        getLoaderManager().initLoader(CATEGORY_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(!getActivity().getSupportFragmentManager().popBackStackImmediate()) {
                    getActivity().finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        if(mParentCategory != null && mParentCategory.getName() != null) {
            setupToolbar(mParentCategory.getName());
        } else {
            setupToolbar(getString(R.string.title_product_category));
        }

        mProductCategoryAdapter = new ProductCategoryCursorAdapter(
                getContext(),
                null,
                R.layout.item_product_category,
                rootView.findViewById(R.id.tvEmpty),
                new Callback() {
                    @Override
                    public void handle(ProductCategory category) {
                        category.setParentCategory(mParentCategory);
                        if(!mProductCategoryDao.hasSubcategories(category.getId())) {
                            mFragmentHandler.handle(ProductListFragment.getInstance(
                                        category,
                                        mProductHandler)
                            );
                        } else {
                            mFragmentHandler.handle(ProductCategoryListFragment.getInstance(
                                    category,
                                    mFragmentHandler,
                                    mProductHandler)
                            );
                        }
                    }
                });

        RecyclerView mProductCategoriesRv = (RecyclerView) rootView.findViewById(R.id.rvLayout);
        mProductCategoriesRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mProductCategoriesRv.addItemDecoration(new ListSpacingDecoration(16));
        mProductCategoriesRv.setAdapter(mProductCategoryAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

        if(mVenueId != null) {
            Log.i(TAG, "DISPLAY ROOT CATEGORIES FOR VENUE: " + mVenueId);
            int venueCategoriesCount = mProductCategoryDao.count(
                    "venue_id = ?", 
                    new String[]{mVenueId.toString()}
            );
            
            if(venueCategoriesCount == 0) {
                Log.i(TAG, "FETCHING CATEGORIES FOR VENUE: " + mVenueId);
                VolleyQueueSingleton.getInstance(getContext()).addRequest(new VolleyRequest<>(
                        NetUtils.getCategoryTreeUrl(mVenueId),
                        ProductCategory[].class,
                        new Response.Listener<ProductCategory[]>() {
                            @Override
                            public void onResponse(ProductCategory[] response) {
                                Log.i(TAG, "FETCHED CATEGORY TREE");
                                new ProductCategoryDao(getContext()).bulkInsertList(
                                        Arrays.asList(response),
                                        mVenueId
                                );
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i(TAG, "UNABLE TO FETCH CATEGORY TREE");
                                ErrorHandler.handleVolleyError(error);
                                mProductCategoryAdapter.swapCursor(null);
                            }
                        }
                ));
            } else {
                Log.i(TAG, "VENUE CATEGORIES ALREADY FETCHED: " + venueCategoriesCount);
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        if(mParentCategory != null) {
            return new CursorLoader(
                    getContext(),
                    RetailorDBContract.ProductCategoryEntry.CONTENT_URI,
                    new String[]{"*"},
                    "parent_id = ?",
                    new String[]{mParentCategory.getId().toString()},
                    null
            );
        }

        if(mVenueId != null) {
            return new CursorLoader(
                    getContext(),
                    RetailorDBContract.ProductCategoryEntry.CONTENT_URI,
                    new String[]{"*"},
                    "venue_id = ? AND parent_id IS null",
                    new String[]{mVenueId.toString()},
                    null
            );
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        mProductCategoryAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        mProductCategoryAdapter.swapCursor(null);
    }

    public void setVenueId(Long mVenueId) {
        this.mVenueId = mVenueId;
    }

    public void setParentCategory(ProductCategory mParentCategory) {
        this.mParentCategory = mParentCategory;
    }

    public void setFragmentHandler(FragmentHandler mFragmentHandler) {
        this.mFragmentHandler = mFragmentHandler;
    }

    public void setProductHandler(ProductHandler productHandler) {
        this.mProductHandler = productHandler;
    }

    public interface Callback {
        void handle(ProductCategory category);
    }
}
