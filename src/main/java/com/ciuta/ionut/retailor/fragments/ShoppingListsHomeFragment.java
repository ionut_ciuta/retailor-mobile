package com.ciuta.ionut.retailor.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.ShoppingListActivity;
import com.ciuta.ionut.retailor.adapters.ShoppingListCursorAdapter;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.design.ListSpacingDecoration;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.util.IdentityHandler;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingListsHomeFragment extends DrawerFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private final String TAG = getClass().getSimpleName();
    private final int ADD_SHOPPING_LIST = 1;
    private final int SHOPPING_LIST_LOADER  = 1;

    private RecyclerView mSoppingListsRv;
    private ShoppingListCursorAdapter mShoppingListAdapter;
    private FloatingActionButton mFAB;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "INIT LOADER");
        getLoaderManager().initLoader(SHOPPING_LIST_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setTitle(getString(R.string.title_lists));
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_list_shopping_list, container, false);
        mSoppingListsRv = (RecyclerView)rootView.findViewById(R.id.rvShoppingLists);
        mSoppingListsRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mSoppingListsRv.addItemDecoration(new ListSpacingDecoration(22));
        mShoppingListAdapter = new ShoppingListCursorAdapter(
                null,
                R.layout.item_card_shopping_list,
                rootView.findViewById(R.id.tvEmpty),
                new FragmentCallback() {
                    @Override
                    public void handle(ShoppingList shoppingList) {
                        openDetailActivity(shoppingList);
                    }
                }
        );
        mSoppingListsRv.setAdapter(mShoppingListAdapter);

        mFAB = (FloatingActionButton)rootView.findViewById(R.id.fab_add_product);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.key_edit_shopping_list), new ShoppingList());

                Intent intent = new Intent(getActivity(), ShoppingListActivity.class);
                intent.putExtra(getString(R.string.key_args), args);
                startActivityForResult(intent, ADD_SHOPPING_LIST);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ADD_SHOPPING_LIST && resultCode == AppCompatActivity.RESULT_OK) {
            Log.i(TAG, "onActivityResult REFRESH LOADER");
            getLoaderManager().restartLoader(SHOPPING_LIST_LOADER, null, this);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        Long userId = IdentityHandler.getActiveUserId(getContext());

        return new CursorLoader(
                getContext(),
                RetailorDBContract.SHOPPING_LIST_VENUE_JOIN_CONTENT_URI,
                new String[]{"shopping_list.local_id, shopping_list._id, shopping_list.name, shopping_list.description, " +
                             "shopping_list.version, venue._id, venue.name"},
                "shopping_list.user_id = ?",
                new String[]{userId.toString()},
                RetailorDBContract.ShoppingListEntry.TABLE_NAME + "." +
                RetailorDBContract.ShoppingListEntry.COLUMN_NAME + " ASC"
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoaderFinished");
        mShoppingListAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        mShoppingListAdapter.swapCursor(null);
    }

    private void openDetailActivity(ShoppingList shoppingList) {
        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.key_view_shopping_list), shoppingList);

        Intent intent = new Intent(getActivity(), ShoppingListActivity.class);
        intent.putExtra(getString(R.string.key_args), args);

        startActivity(intent);
    }

    public interface FragmentCallback {
        void handle(ShoppingList shoppingList);
    }

    /*
    public interface ContentObserver extends Serializable {
        void notifyInsert();
    }
    */
}
