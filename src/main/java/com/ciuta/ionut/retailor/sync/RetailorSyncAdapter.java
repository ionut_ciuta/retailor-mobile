package com.ciuta.ionut.retailor.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.ciuta.ionut.retailor.R;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RetailorSyncAdapter extends AbstractThreadedSyncAdapter {
    private final String TAG = this.getClass().getSimpleName();
    public static final int SYNC_SIGN_IN = 1;

    public RetailorSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        Log.d(TAG, "onPerformSync called");
        int syncType = extras.getInt(getContext().getString(R.string.key_sync_type));

        switch (syncType) {
            case SYNC_SIGN_IN:
                Log.i(TAG, "PERFORMING INITIAL SYNC");
                break;

            default:
                Log.e(TAG, "UNKNOW SYNC REQUEST");
        }
    }

    public static void syncImmediately(Context context, int type) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        bundle.putInt(context.getString(R.string.key_sync_type), type);

        Account syncAccount = AccountManager.get(context).getAccounts()[0];

        ContentResolver.requestSync(syncAccount,
                context.getString(R.string.content_authority),
                bundle);
    }
}
