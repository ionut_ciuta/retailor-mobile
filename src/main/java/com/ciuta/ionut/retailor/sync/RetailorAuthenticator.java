package com.ciuta.ionut.retailor.sync;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.AuthenticationActivity;
import com.ciuta.ionut.retailor.util.StringUtils;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
/*
 * Abstract base class for creating AccountAuthenticators
 * https://developer.android.com/reference/android/accounts/AbstractAccountAuthenticator.html
 */
public class RetailorAuthenticator extends AbstractAccountAuthenticator {
    private final String TAG = this.getClass().getSimpleName();
    private Context mContext;

    public RetailorAuthenticator(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public Bundle editProperties(
            AccountAuthenticatorResponse response,
            String accountType) {
        return null;
    }

    @Override
    public Bundle addAccount(
            AccountAuthenticatorResponse response,
            String accountType,
            String authTokenType,
            String[] requiredFeatures,
            Bundle options) throws NetworkErrorException {
        Log.d(TAG, "addAccount()");

        /*token should be null; even if not, there is only one type */
        authTokenType = mContext.getString(R.string.auth_token_type_full);

        /*Authenticator needs information from user to satisfy request*/
        Intent intent = new Intent(mContext, AuthenticationActivity.class);

        /*Container for the key intent created above*/
        Bundle resultBundle = new Bundle();

        /*Needed to pass the response of the authentication activity*/
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

        intent.putExtra(mContext.getString(R.string.auth_arg_account_type), accountType);
        intent.putExtra(mContext.getString(R.string.auth_arg_token_type), authTokenType);
        intent.putExtra(mContext.getString(R.string.auth_arg_new), true);

        resultBundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return resultBundle;
    }

    @Override
    public Bundle confirmCredentials(
            AccountAuthenticatorResponse response,
            Account account,
            Bundle options) throws NetworkErrorException {
        Log.d(TAG, "confirmCredentials()");
        return null;
    }

    @Override
    public Bundle getAuthToken(
            AccountAuthenticatorResponse response,
            Account account,
            String authTokenType,
            Bundle options) throws NetworkErrorException {
        Log.d(TAG, "getAuthToken()");
        Bundle resultBundle = new Bundle();

        if(!authTokenType.equals(mContext.getString(R.string.auth_token_type_full))) {
            /*RetailorToken has invalid type*/
            Log.e(TAG, "Invalid token type: " + authTokenType);
            resultBundle.putString(
                    AccountManager.KEY_ERROR_MESSAGE,
                    mContext.getString(R.string.auth_token_type_error));
        } else {
            /*RetailorToken has a valid type*/
            AccountManager accountManager = AccountManager.get(mContext);
            String token = accountManager.peekAuthToken(account, authTokenType);

            if(StringUtils.isEmpty(token)) {
                /*we need to authenticate the user*/
                String password = accountManager.getPassword(account);

                if(!StringUtils.isEmpty(password)) {
                    /*There is a password; it can be user to request token from server */
                    // TODO: 09.06.2016 insert server auth API CALL and add these
                    token = "dummy";
                    resultBundle.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                    resultBundle.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
                    resultBundle.putString(AccountManager.KEY_AUTHTOKEN, token);
                } else {
                    /*There is no password; user must insert it*/
                    Intent intent = new Intent(mContext, AuthenticationActivity.class);
                    intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
                    intent.putExtra(mContext.getString(R.string.auth_arg_account_name), account.name);
                    intent.putExtra(mContext.getString(R.string.auth_arg_account_type), account.type);
                    intent.putExtra(mContext.getString(R.string.auth_arg_token_type), authTokenType);
                    resultBundle.putParcelable(AccountManager.KEY_INTENT, intent);
                }
            }
        }

        return resultBundle;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        Log.d(TAG, "getAuthTokenLabel()");
        return "(label) " + authTokenType;
    }

    @Override
    public Bundle updateCredentials(
            AccountAuthenticatorResponse response,
            Account account,
            String authTokenType,
            Bundle options) throws NetworkErrorException {
        Log.d(TAG, "updateCredentials()");
        return null;
    }

    @Override
    public Bundle hasFeatures(
            AccountAuthenticatorResponse response,
            Account account,
            String[] features) throws NetworkErrorException {
        Log.d(TAG, "hasFeatures()");
        Bundle bundle = new Bundle();

        /* the account support all features*/
        bundle.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, true);
        return bundle;
    }
}
