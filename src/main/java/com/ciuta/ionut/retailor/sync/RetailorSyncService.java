package com.ciuta.ionut.retailor.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RetailorSyncService extends Service {
    private final String TAG = this.getClass().getSimpleName();
    private static final Object lock = new Object();
    private static RetailorSyncAdapter syncAdapter;

    @Override
    public void onCreate() {
        Log.d(TAG, "RetailorSyncService onCreate()");
        synchronized(lock) {
            if(syncAdapter == null) {
                syncAdapter = new RetailorSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }
}
