package com.ciuta.ionut.retailor.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RetailorAuthenticatorService extends Service {

    private RetailorAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        mAuthenticator = new RetailorAuthenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
