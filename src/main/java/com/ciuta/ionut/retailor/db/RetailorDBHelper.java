package com.ciuta.ionut.retailor.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ciuta.ionut.retailor.db.RetailorDBContract.*;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RetailorDBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 16;
    public static final String DB_NAME = "retailor.db";

    public RetailorDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String PK = "PRIMARY KEY AUTOINCREMENT";
        final String NOT_NULL   = " NOT NULL ";
        final String TYPE_INT   = " INTEGER  ";
        final String TYPE_REAL  = " REAL     ";
        final String TYPE_TEXT  = " TEXT     ";

        final String SQL_CREATE_VENUE_TABLE = "CREATE TABLE " + VenueEntry.TABLE_NAME + " (" +
                VenueEntry._ID                      + TYPE_INT   + PK        + ", " +
                VenueEntry.COLUMN_NAME              + TYPE_TEXT  + NOT_NULL  + ", " +
                VenueEntry.COLUMN_ADDRESS           + TYPE_TEXT  + NOT_NULL  + ", " +
                VenueEntry.COLUMN_DESCRIPTION       + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_X_COORD           + TYPE_REAL              + ", " +
                VenueEntry.COLUMN_Y_COORD           + TYPE_REAL              + ", " +
                VenueEntry.COLUMN_MONDAY_OPEN       + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_MONDAY_CLOSE      + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_TUESDAY_OPEN      + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_TUESDAY_CLOSE     + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_WEDNESDAY_OPEN    + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_WEDNESDAY_CLOSE   + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_THURSDAY_OPEN     + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_THURSDAY_CLOSE    + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_FRIDAY_OPEN       + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_FRIDAY_CLOSE      + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_SATURDAY_OPEN     + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_SATURDAY_CLOSE    + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_SUNDAY_OPEN       + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_SUNDAY_CLOSE      + TYPE_TEXT              + ", " +
                VenueEntry.COLUMN_VERSION           + TYPE_INT   + NOT_NULL  + ");";


        final String SQL_CREATE_SHOPPING_LIST_TABLE = "CREATE TABLE " + ShoppingListEntry.TABLE_NAME + " (" +
                ShoppingListEntry.COLUMN_LOCAL_ID       + TYPE_INT   + PK       + ", " +
                ShoppingListEntry._ID                   + TYPE_INT              + ", " +
                ShoppingListEntry.COLUMN_NAME           + TYPE_TEXT  + NOT_NULL + ", " +
                ShoppingListEntry.COLUMN_DESCRIPTION    + TYPE_TEXT             + ", " +
                ShoppingListEntry.COLUMN_VENUE_ID       + TYPE_INT   + NOT_NULL + ", " +
                ShoppingListEntry.COLUMN_USER_ID        + TYPE_INT   + NOT_NULL + ", " +
                ShoppingListEntry.COLUMN_VERSION        + TYPE_INT   + NOT_NULL + ", " +

                fk(ShoppingListEntry.COLUMN_VENUE_ID, VenueEntry.TABLE_NAME, VenueEntry._ID) + ", " +
                fk(ShoppingListEntry.COLUMN_USER_ID , UserEntry.TABLE_NAME , UserEntry._ID)  + ");";

        final String SQL_CREATE_USER_TABLE = "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                UserEntry._ID               + TYPE_INT  + PK        + ", "       +
                UserEntry.COLUMN_EMAIL      + TYPE_TEXT + NOT_NULL  + ", " +
                UserEntry.COLUMN_FIRSTNAME  + TYPE_TEXT + NOT_NULL  + ", "       +
                UserEntry.COLUMN_LASTNAME   + TYPE_TEXT + NOT_NULL  + ", "       +
                UserEntry.COLUMN_BIRTHDATE  + TYPE_TEXT             + ", "       +
                UserEntry.COLUMN_GENDER     + TYPE_TEXT             + ", "       +
                UserEntry.COLUMN_STATUS     + TYPE_INT  + NOT_NULL  + "default 0); " +
                VenueEntry.COLUMN_VERSION   + TYPE_INT  + NOT_NULL  + ");";

        final String SQL_CREATE_PRODUCT_TABLE = "CREATE TABLE " + ProductEntry.TABLE_NAME + " (" +
                ProductEntry._ID                + TYPE_INT  + PK        + ", " +
                ProductEntry.COLUMN_NAME        + TYPE_TEXT + NOT_NULL  + ", " +
                ProductEntry.COLUMN_PRICE       + TYPE_REAL + NOT_NULL  + ", " +
                ProductEntry.COLUMN_DESCRIPTION + TYPE_TEXT             + ", " +
                ProductEntry.COLUMN_CATEGORY_ID + TYPE_INT              + ", " +
                ProductEntry.COLUMN_VERSION     + TYPE_INT  + NOT_NULL  + ");";

        final String SQL_CREATE_LIST_ITEM_TABLE = "CREATE TABLE " + ListItemEntry.TABLE_NAME + " (" +
                ShoppingListEntry.COLUMN_LOCAL_ID   + TYPE_INT + PK         + ", " +
                ShoppingListEntry._ID               + TYPE_INT              + ", " +
                ListItemEntry.COLUMN_PRODUCT_ID     + TYPE_INT + NOT_NULL   + ", " +
                ListItemEntry.COLUMN_LIST_ID        + TYPE_INT + NOT_NULL   + ", " +
                ListItemEntry.COLUMN_TAKEN          + TYPE_INT + NOT_NULL   + " default 0, " +
                VenueEntry.COLUMN_VERSION           + TYPE_INT + NOT_NULL   + ", " +

                fk(ListItemEntry.COLUMN_PRODUCT_ID, ProductEntry.TABLE_NAME     , ProductEntry._ID)                  + ", " +
                fk(ListItemEntry.COLUMN_LIST_ID   , ShoppingListEntry.TABLE_NAME, ShoppingListEntry.COLUMN_LOCAL_ID) + ");";

        final String SQL_CREATE_PRODUCT_CATEGORY_TABLE = "CREATE TABLE " + ProductCategoryEntry.TABLE_NAME + " (" +
                ProductCategoryEntry._ID                + TYPE_INT  + PK         + ", " +
                ProductCategoryEntry.COLUMN_NAME        + TYPE_TEXT + NOT_NULL   + ", " +
                ProductCategoryEntry.COLUMN_PARENT_ID   + TYPE_INT               + ", " +
                ProductCategoryEntry.COLUMN_VENUE_ID    + TYPE_INT  + NOT_NULL   + ", " +
                VenueEntry.COLUMN_VERSION + TYPE_INT    + NOT_NULL               + ", " +

                fk(ProductCategoryEntry.COLUMN_PARENT_ID, ProductCategoryEntry.TABLE_NAME, ProductCategoryEntry._ID) + ", " +
                fk(ProductCategoryEntry.COLUMN_VENUE_ID , VenueEntry.TABLE_NAME,           VenueEntry._ID)           + ");";

        final String SQL_CREATE_NOTIFICATION_TABLE = "CREATE TABLE " + NotificationEntry.TABLE_NAME + " (" +
                NotificationEntry._ID                   + TYPE_INT  + PK        + ", " +
                NotificationEntry.COLUMN_TYPE           + TYPE_INT  + NOT_NULL  + ", " +
                NotificationEntry.COLUMN_USER_ID        + TYPE_INT              + ", " +
                NotificationEntry.COLUMN_MESSAGE	    + TYPE_TEXT 			+ ", " +
                NotificationEntry.COLUMN_DISCOUNT_CODE  + TYPE_TEXT 			+ ", " +
                NotificationEntry.COLUMN_LIST_ID 	    + TYPE_INT              + ", " +
                NotificationEntry.COLUMN_LIST_NAME 	    + TYPE_TEXT             + ", " +
                NotificationEntry.COLUMN_PRODUCT_ID     + TYPE_INT				+ ", " +
                NotificationEntry.COLUMN_PRODUCT_NAME   + TYPE_TEXT				+ ", " +
                NotificationEntry.COLUMN_PRODUCT_PRICE  + TYPE_REAL				+ ");";


        sqLiteDatabase.execSQL(SQL_CREATE_USER_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_VENUE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_SHOPPING_LIST_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_PRODUCT_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_LIST_ITEM_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_PRODUCT_CATEGORY_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_NOTIFICATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(drop(ListItemEntry.TABLE_NAME));
        sqLiteDatabase.execSQL(drop(ShoppingListEntry.TABLE_NAME));
        sqLiteDatabase.execSQL(drop(ProductEntry.TABLE_NAME));
        sqLiteDatabase.execSQL(drop(VenueEntry.TABLE_NAME));
        sqLiteDatabase.execSQL(drop(UserEntry.TABLE_NAME));
        sqLiteDatabase.execSQL(drop(ProductCategoryEntry.TABLE_NAME));
        sqLiteDatabase.execSQL(drop(NotificationEntry.TABLE_NAME));
        onCreate(sqLiteDatabase);
    }

    private String fk(String column, String table, String referred) {
        return "FOREIGN KEY (" + column + ") REFERENCES " + table + " (" + referred + ")";
    }

    private String drop(String table) {
        return "DROP TABLE IF EXISTS " + table + ";";
    }
}
