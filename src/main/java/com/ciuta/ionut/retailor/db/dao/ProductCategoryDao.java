package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentValues;
import android.content.Context;

import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.RetailorDBContract.ProductCategoryEntry;
import com.ciuta.ionut.retailor.pojo.ProductCategory;
import com.ciuta.ionut.retailor.pojo.Venue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductCategoryDao extends Dao<ProductCategory> {

    public ProductCategoryDao(Context context) {
        super(context, RetailorDBContract.ProductCategoryEntry.CONTENT_URI);
    }

    @Override
    public ContentValues getContentValues(ProductCategory entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProductCategoryEntry._ID, entity.getId());
        contentValues.put(ProductCategoryEntry.COLUMN_NAME, entity.getName());
        contentValues.put(ProductCategoryEntry.COLUMN_PARENT_ID, entity.getParentCategory().getId());
        contentValues.put(ProductCategoryEntry.COLUMN_VENUE_ID, entity.getVenue().getId());
        contentValues.put(ProductCategoryEntry.COLUMN_VERSION, entity.getVersion());
        return contentValues;
    }

    @Override
    public List<ProductCategory> selectAllAsList() {
        return null;
    }

    @Override
    public Long insert(ProductCategory entity) {
        return null;
    }

    @Override
    public Integer update(ProductCategory entity) {
        return null;
    }

    @Override
    public Integer delete(ProductCategory entity) {
        return null;
    }

    public Integer bulkInsertList(List<ProductCategory> categories, Long venueId) {
        int result = 0;
        List<ProductCategory> flatCategories = new ArrayList<>();
        flattenList(categories, flatCategories, null, venueId);
        result = bulkInsert(flatCategories);
        return result;
    }

    private void flattenList(List<ProductCategory> categories,
                             List<ProductCategory> flatCategories,
                             Long parentId,
                             Long venueId) {
        for(ProductCategory category : categories) {
            category.setVenue(new Venue()); category.getVenue().setId(venueId);
            category.setParentCategory(new ProductCategory()); category.getParentCategory().setId(parentId);
            flatCategories.add(category);

            if(category.getSubcategories().size() > 0) {
                flattenList(category.getSubcategories(), flatCategories, category.getId(), venueId);
            }
        }
    }

    public boolean hasSubcategories(Long categoryId) {
        int count = count("parent_id = ?", new String[]{categoryId.toString()});
        return count > 0;
    }
}
