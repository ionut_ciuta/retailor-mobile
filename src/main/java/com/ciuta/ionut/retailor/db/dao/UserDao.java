package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.RetailorDBContract.UserEntry;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.IdentityHandler;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class UserDao extends Dao<User> {
    private final String TAG = this.getClass().getSimpleName();

    public UserDao(Context context) {
        super(context, RetailorDBContract.UserEntry.CONTENT_URI);
    }

    @Override
    public ContentValues getContentValues(User entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserEntry._ID, entity.getId());
        contentValues.put(UserEntry.COLUMN_EMAIL, entity.getEmail());
        contentValues.put(UserEntry.COLUMN_FIRSTNAME, entity.getFirstName());
        contentValues.put(UserEntry.COLUMN_LASTNAME, entity.getLastName());
        contentValues.put(UserEntry.COLUMN_GENDER, entity.getGender());
        //contentValues.put(UserEntry.COLUMN_BIRTHDATE, entity.getBirthDate());
        contentValues.put(UserEntry.COLUMN_STATUS, User.USER_ACTIVE);
        return contentValues;
    }

    @Override
    public List<User> selectAllAsList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Long insert(User entity) {
        Uri newUri = mContext.getContentResolver().insert(mUri, getContentValues(entity));
        return ContentUris.parseId(newUri);
    }

    @Override
    public Integer bulkInsert(List<User> entities) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer update(User entity) {
        Long activeUserId = IdentityHandler.getActiveUserId(mContext);
        entity.setId(activeUserId);
        ContentValues contentValues = getContentValues(entity);

        return mContext.getContentResolver().update(
                mUri,
                contentValues,
                "_id = ?",
                new String[]{activeUserId.toString()}
        );
    }

    @Override
    public Integer delete(User entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Insert a new user as active
     * @param newUser the user to be inserted as active
     * @return newUser id
     */
    public Long insertActive(User newUser) {
        Long result = null;
        ContentValues contentValues = new ContentValues();

        try {
            deactivateUser();
            contentValues.put(UserEntry.COLUMN_STATUS, User.USER_ACTIVE);
            contentValues.put(UserEntry.COLUMN_EMAIL, newUser.getEmail());
            contentValues.put(UserEntry.COLUMN_LASTNAME, newUser.getLastName()   != null ? newUser.getLastName() : "");
            contentValues.put(UserEntry.COLUMN_FIRSTNAME, newUser.getFirstName() != null ? newUser.getFirstName() : "");
            Uri newUri = mContext.getContentResolver().insert(mUri, contentValues);
            result = ContentUris.parseId(newUri);
        } catch (Exception e) {
            ErrorHandler.handleException(e);
        }

        return result;
    }

    /**
     * Activates user with username after deactivating previous active user
     * @param username username of user to set active
     */
    public void activateUser(String username) {
        int count;
        ContentValues contentValues = new ContentValues();

        try {
            deactivateUser();
            contentValues.put(UserEntry.COLUMN_STATUS, User.USER_ACTIVE);

            count = mContext.getContentResolver().update(
                    mUri,
                    contentValues,
                    enclose(UserEntry.COLUMN_EMAIL),
                    params(username)
            );

            if(count > 1) {
                throw new Exception("Multiple users with same username");
            }

        } catch (Exception e) {
            ErrorHandler.handleException(e);
        }
    }

    /**
     * Deactivates the active user
     */
    public void deactivateUser() {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(UserEntry.COLUMN_STATUS, User.USER_INACTIVE);
            mContext.getContentResolver().update(
                    mUri,
                    contentValues,
                    enclose(UserEntry.COLUMN_STATUS),
                    params(User.USER_ACTIVE.toString())
            );
        } catch (Exception e) {
            ErrorHandler.handleException(e);
        }
    }

    public Long getActiveUserId() {
        Long result = 0L;
        Cursor c = mContext.getContentResolver().query(
                mUri,
                new String[]{UserEntry._ID},
                "status = ?",
                new String[]{User.USER_ACTIVE.toString()},
                null
        );

        if(c != null) {
            c.moveToFirst();
            result = c.getLong(c.getColumnIndex(UserEntry._ID));
            c.close();
        }

        return result;
    }

    public User getActiveUser() {
        User result = new User();

        Cursor c = mContext.getContentResolver().query(
                mUri,
                new String[]{"_id, firstname, lastname"},
                enclose(UserEntry.COLUMN_STATUS),
                params(User.USER_ACTIVE.toString()),
                null
        );

        if(c != null) {
            c.moveToFirst();
            result.setId(c.getLong(0));
            result.setFirstName(c.getString(1));
            result.setLastName(c.getString(2));
            c.close();
        }

        return result;
    }
}
