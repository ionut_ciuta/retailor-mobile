package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.ciuta.ionut.retailor.pojo.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class Dao<T extends Entity> {
    private final String TAG = this.getClass().getSimpleName();
    protected Uri mUri;
    protected Context mContext;

    public Dao(Context context) {
        this.mContext = context;
    }

    protected Dao(Context context, Uri uri) {
        this.mContext = context;
        this.mUri = uri;
    }

    public Cursor selectAll() {
        return mContext.getContentResolver().query(
                mUri,
                new String[]{"*"},
                null,
                null,
                null
        );
    }

    public Integer deleteAll() {
        return mContext.getContentResolver().delete(
                mUri,
                null,
                null
        );
    }

    public Integer bulkInsert(List<T> entities) {
        ContentValues[] values = new ContentValues[entities.size()];
        for(int i = 0; i < entities.size(); i++) {
            values[i] = getContentValues(entities.get(i));
        }
        return mContext.getContentResolver().bulkInsert(mUri, values);
    }

    public Integer bulkInsertWithIndexCheck(List<T> entities) {
        List<ContentValues> valuesList = new ArrayList<>();
        List<Long> index = getIndex();

        for(int i = 0; i < entities.size(); i++) {
            if(!index.contains(entities.get(i).getId())) {
                valuesList.add(getContentValues(entities.get(i)));
            }
        }

        if(valuesList.size() > 0) {
            ContentValues[] values = new ContentValues[valuesList.size()];
            int i = 0;
            for(ContentValues cv : valuesList) {
                values[i++] = cv;
            }
            return mContext.getContentResolver().bulkInsert(mUri, values);
        } else {
            return 0;
        }
    }

    public Integer count(String selection, String[] selectionArgs) {
        int count = 0;
        Cursor cursor = mContext.getContentResolver().query(
                mUri,
                new String[] {"count(*) AS count"},
                selection,
                selectionArgs,
                null
        );

        if(cursor != null) {
            cursor.moveToFirst();
            count = cursor.getInt(0);
            cursor.close();
        }

        return count;
    }

    public List<Long> getIndex() {
        List<Long> index = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(
                mUri,
                new String[]{"_id"},
                null,
                null,
                "_id ASC"
        );

        if(cursor != null) {
            while(cursor.moveToNext()) {
                index.add(cursor.getLong(cursor.getColumnIndex(BaseColumns._ID)));
            }
            cursor.close();
        }

        return index;
    }

    protected String enclose(String... params) {
        String result = params[0] + " = ? ";
        for(int i = 1; i < params.length; i++) {
            result += " AND " + params[i] + " = ? ";
        }
        return result;
    }

    protected String[] params(String... values) {
        String[] result = new String[values.length];
        System.arraycopy(values, 0, result, 0, values.length);
        return result;
    }

    public abstract ContentValues getContentValues(T entity);
    public abstract List<T> selectAllAsList();
    public abstract Long insert(T entity);
    public abstract Integer update(T entity);
    public abstract Integer delete(T entity);
}
