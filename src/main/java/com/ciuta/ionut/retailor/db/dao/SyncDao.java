package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.ciuta.ionut.retailor.pojo.Syncable;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class SyncDao<T extends Syncable> extends Dao<T> {
    private final String COLUMN_ID      = "_id";
    private final String COLUMN_VERSION = "version";

    protected SyncDao(Context context, Uri uri) {
        super(context, uri);
    }

    protected ContentValues getContentValuesForSync(Syncable entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ID, entity.getId());
        contentValues.put(COLUMN_VERSION, entity.getVersion());
        return contentValues;
    }

    public abstract int onUpdate(T entity);
}
