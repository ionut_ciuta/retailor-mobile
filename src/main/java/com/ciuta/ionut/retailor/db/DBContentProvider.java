package com.ciuta.ionut.retailor.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ciuta.ionut.retailor.db.RetailorDBContract.*;
/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DBContentProvider extends ContentProvider {
    private final String TAG = getClass().getSimpleName();
    private static final UriMatcher uriMatcher;
    private static SQLiteQueryBuilder mShoppingListWithVenue;
    private static SQLiteQueryBuilder mListItemWithProduct;
    private RetailorDBHelper dbHelper;

    public static final int VENUE                    = 100;
    public static final int SHOPPING_LIST            = 200;
    public static final int PRODUCT                  = 300;
    public static final int LIST_ITEM                = 400;
    public static final int USER                     = 500;
    public static final int PRODUCT_CATEGORY         = 600;
    public static final int NOTIFICATION             = 700;
    public static final int SHOPPING_LIST_JOIN_VENUE = 901;
    public static final int LIST_ITEM_JOIN_PRODUCT   = 902;

    static {
        uriMatcher = initUriMatcher();

        mShoppingListWithVenue = new SQLiteQueryBuilder();
        mShoppingListWithVenue.setTables(
                        ShoppingListEntry.TABLE_NAME + " INNER JOIN " +
                        VenueEntry.TABLE_NAME        + " ON "         +
                        ShoppingListEntry.TABLE_NAME + "." + ShoppingListEntry.COLUMN_VENUE_ID + " = " +
                        VenueEntry.TABLE_NAME        + "." + VenueEntry._ID);

        mListItemWithProduct = new SQLiteQueryBuilder();
        mListItemWithProduct.setTables(
                        ListItemEntry.TABLE_NAME + " INNER JOIN " +
                        ProductEntry.TABLE_NAME  + " ON " +
                        ListItemEntry.TABLE_NAME + "." + ListItemEntry.COLUMN_PRODUCT_ID + " = " +
                        ProductEntry.TABLE_NAME  + "." + ProductEntry._ID);
    }

    public static UriMatcher initUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        String authority = RetailorDBContract.CONTENT_AUTHORITY;

        uriMatcher.addURI(authority, RetailorDBContract.PATH_VENUE, VENUE);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_SHOPPING_LIST, SHOPPING_LIST);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_PRODUCT, PRODUCT);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_LIST_ITEM, LIST_ITEM);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_USER, USER);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_SHOPPING_LIST_VENUE_JOIN, SHOPPING_LIST_JOIN_VENUE);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_LIST_ITEM_PRODUCT_JOIN, LIST_ITEM_JOIN_PRODUCT);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_PRODUCT_CATEGORY, PRODUCT_CATEGORY);
        uriMatcher.addURI(authority, RetailorDBContract.PATH_NOTIFICATION, NOTIFICATION);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new RetailorDBHelper(getContext());
        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        int match = uriMatcher.match(uri);

        switch (match) {
            //if the URI points to the list of venues, return type is DIR of venue entries
            case VENUE:
                return VenueEntry.CONTENT_TYPE;

            //if the URI points to the list of slists, return type is DIR of slists entries
            case SHOPPING_LIST:
                return ShoppingListEntry.CONTENT_TYPE;

            case PRODUCT:
                return ProductEntry.CONTENT_TYPE;

            case LIST_ITEM:
                return ListItemEntry.CONTENT_TYPE;

            case USER:
                return UserEntry.CONTENT_TYPE;

            case PRODUCT_CATEGORY:
                return ProductEntry.CONTENT_TYPE;
            //if the URI points to a single object, we would have CONTENT_ITEM_TYPE
            //something

            case NOTIFICATION:
                return RetailorDBContract.NotificationEntry.CONTENT_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown URI");
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.i(TAG, "QUERY");
        String table = matchUriToTable(uri);
        Cursor resultCursor = select(table, projection, selection, selectionArgs, sortOrder);
        resultCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return resultCursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        Log.i(TAG, "INSERT");
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String table = matchUriToTable(uri);
        long resultId = insert(table, contentValues);

        getContext().getContentResolver().notifyChange(uri, null);
        return buildNewUri(uri, resultId);
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        Log.i(TAG, "BULK INSERT");
        String table = matchUriToTable(uri);
        int result = bulkInsert(table, values);

        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        String table = matchUriToTable(uri);
        int result = delete(table, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        Log.i(TAG, "UPDATE");
        String table = matchUriToTable(uri);
        int result = update(table, values, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    private String matchUriToTable(Uri uri) {
        String table;

        switch (uriMatcher.match(uri)) {
            case VENUE:
                table = VenueEntry.TABLE_NAME;
                break;

            case SHOPPING_LIST:
                table = ShoppingListEntry.TABLE_NAME;
                break;

            case PRODUCT:
                table = ProductEntry.TABLE_NAME;
                break;

            case LIST_ITEM:
                table = ListItemEntry.TABLE_NAME;
                break;

            case USER:
                table = UserEntry.TABLE_NAME;
                break;

            case PRODUCT_CATEGORY:
                table = ProductCategoryEntry.TABLE_NAME;
                break;

            case NOTIFICATION:
                table = NotificationEntry.TABLE_NAME;
                break;

            case SHOPPING_LIST_JOIN_VENUE:
                table = ShoppingListEntry.TABLE_NAME + " JOIN " +
                        VenueEntry.TABLE_NAME        + " ON "         +
                        ShoppingListEntry.TABLE_NAME + "." + ShoppingListEntry.COLUMN_VENUE_ID + " = " +
                        VenueEntry.TABLE_NAME        + "." + VenueEntry._ID;
                break;

            case LIST_ITEM_JOIN_PRODUCT:
                table = ListItemEntry.TABLE_NAME + " JOIN " +
                        ProductEntry.TABLE_NAME  + " ON " +
                        ListItemEntry.TABLE_NAME + "." + ListItemEntry.COLUMN_PRODUCT_ID + " = " +
                        ProductEntry.TABLE_NAME  + "." + ProductEntry._ID;
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri " + uri.toString());
        }

        return table;
    }

    private Uri buildNewUri(Uri uri, long newId) {
        Uri result;

        switch (uriMatcher.match(uri)) {
            case VENUE:
                result = VenueEntry.buildVenueUri(newId);
                break;

            case SHOPPING_LIST:
                result = ShoppingListEntry.buildShoppingListUri(newId);
                break;

            case PRODUCT:
                result = ProductEntry.buildProductUri(newId);
                break;

            case LIST_ITEM:
                result = ListItemEntry.buildListItemUri(newId);
                break;

            case USER:
                result = UserEntry.buildUserUri(newId);
                break;

            case PRODUCT_CATEGORY:
                result = ProductCategoryEntry.buildProductCategoryItemUri(newId);
                break;

            case NOTIFICATION:
                result = NotificationEntry.buildNotificationItemUri(newId);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri " + uri.toString());
        }

        return result;
    }

    private Cursor select(String table, String[] projection, String selection,
                         String[] selectionArgs, String sortOrder) {
        return dbHelper.getReadableDatabase().query(
                table,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
    }

    private long insert(String table, ContentValues contentValues) {
        return dbHelper.getWritableDatabase().insert(table, null, contentValues);
    }

    private int bulkInsert(String table, ContentValues[] contentValues) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int retCount = 0;

        db.beginTransaction();
        try {
            for(ContentValues values : contentValues) {
                long id = db.insert(
                        table,
                        null,
                        values);

                if(id > 0) retCount++;
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }

        return retCount;
    }

    private int update(String table, ContentValues contentValues, String selection,
                       String[] selectionArgs) {
        return dbHelper.getWritableDatabase().update(
                table,
                contentValues,
                selection,
                selectionArgs);
    }

    private int delete(String table, String selection, String[] selectionArgs) {
        return dbHelper.getWritableDatabase().delete(
                table,
                selection,
                selectionArgs);
    }

    private void notify(Uri uri, long result) {
        if(result > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
    }
}
