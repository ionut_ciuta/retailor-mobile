package com.ciuta.ionut.retailor.db;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RetailorDBContract {
    //Name of the content provider
    public static final String CONTENT_AUTHORITY = "com.ciuta.ionut.retailor";

    //The base of all content provider URIs
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_VENUE                       = "venue";
    public static final String PATH_SHOPPING_LIST               = "shopping_list";
    public static final String PATH_PRODUCT                     = "product";
    public static final String PATH_LIST_ITEM                   = "shopping_list_item";
    public static final String PATH_USER                        = "user";
    public static final String PATH_PRODUCT_CATEGORY            = "product_category";
    public static final String PATH_NOTIFICATION                = "notification";
    public static final String PATH_SHOPPING_LIST_VENUE_JOIN    = "shopping_list_venue_join";
    public static final String PATH_LIST_ITEM_PRODUCT_JOIN      = "list_item_product_join";

    public static final Uri SHOPPING_LIST_VENUE_JOIN_CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_SHOPPING_LIST_VENUE_JOIN).build();
    public static final Uri LIST_ITEM_PRODUCT_JOIN_CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_LIST_ITEM_PRODUCT_JOIN).build();

    public static final class VenueEntry implements BaseColumns {
        public static final String TABLE_NAME                   = "venue";
        public static final String COLUMN_NAME                  = "name";
        public static final String COLUMN_ADDRESS               = "address";
        public static final String COLUMN_DESCRIPTION           = "description";
        public static final String COLUMN_X_COORD               = "x_coord";
        public static final String COLUMN_Y_COORD               = "y_coord";
        public static final String COLUMN_MONDAY_OPEN           = "monday_open";
        public static final String COLUMN_MONDAY_CLOSE          = "monday_close";
        public static final String COLUMN_TUESDAY_OPEN          = "tuesday_open";
        public static final String COLUMN_TUESDAY_CLOSE         = "tuesday_close";
        public static final String COLUMN_WEDNESDAY_OPEN        = "wednesday_open";
        public static final String COLUMN_WEDNESDAY_CLOSE       = "wednesday_close";
        public static final String COLUMN_THURSDAY_OPEN         = "thursday_open";
        public static final String COLUMN_THURSDAY_CLOSE        = "thursday_close";
        public static final String COLUMN_FRIDAY_OPEN           = "friday_open";
        public static final String COLUMN_FRIDAY_CLOSE          = "friday_close";
        public static final String COLUMN_SATURDAY_OPEN         = "saturday_open";
        public static final String COLUMN_SATURDAY_CLOSE        = "saturday_close";
        public static final String COLUMN_SUNDAY_OPEN           = "sunday_open";
        public static final String COLUMN_SUNDAY_CLOSE          = "sunday_close";
        public static final String COLUMN_VERSION               = "version";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_VENUE).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_VENUE;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_VENUE;

        public static Uri buildVenueUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ShoppingListEntry implements BaseColumns {
        public static final String TABLE_NAME           = "shopping_list";
        public static final String COLUMN_LOCAL_ID      = "local_id";
        public static final String COLUMN_NAME          = "name";
        public static final String COLUMN_DESCRIPTION   = "description";
        public static final String COLUMN_VENUE_ID      = "venue_id";
        public static final String COLUMN_USER_ID       = "user_id";
        public static final String COLUMN_VERSION       = "version";
        public static final String COLUMN_VENUE_KEY     = "shopping_list_venue_fk";
        public static final String COLUMN_USER_KEY      = "shopping_list_user_fk";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SHOPPING_LIST).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SHOPPING_LIST;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SHOPPING_LIST;

        public static Uri buildShoppingListUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class UserEntry implements BaseColumns {
        public static final String TABLE_NAME       = "user";
        public static final String COLUMN_FIRSTNAME = "firstname";
        public static final String COLUMN_LASTNAME  = "lastname";
        public static final String COLUMN_EMAIL     = "mail";
        public static final String COLUMN_GENDER    = "gender";
        public static final String COLUMN_BIRTHDATE = "birthdate";
        public static final String COLUMN_STATUS    = "status";
        public static final String COLUMN_VERSION   = "version";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USER;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USER;

        public static Uri buildUserUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ProductEntry implements BaseColumns {
        public static final String TABLE_NAME           = "product";
        public static final String COLUMN_NAME          = "name";
        public static final String COLUMN_PRICE         = "price";
        public static final String COLUMN_DESCRIPTION   = "description";
        public static final String COLUMN_VERSION       = "version";
        public static final String COLUMN_CATEGORY_ID   = "category_id";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PRODUCT).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCT;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCT;

        public static Uri buildProductUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ListItemEntry implements BaseColumns {
        public static final String TABLE_NAME           = "list_item";
        public static final String COLUMN_LOCAL_ID      = "local_id";
        public static final String COLUMN_PRODUCT_ID    = "product_id";
        public static final String COLUMN_PRODUCT_KEY   = "list_item_product_fk";
        public static final String COLUMN_LIST_ID       = "list_id";
        public static final String COLUMN_LIST_KEY      = "list_item_list_fk";
        public static final String COLUMN_TAKEN         = "taken";
        public static final String COLUMN_VERSION       = "version";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LIST_ITEM).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LIST_ITEM;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LIST_ITEM;

        public static Uri buildListItemUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ProductCategoryEntry implements BaseColumns {
        public static final String TABLE_NAME          = "product_category";
        public static final String COLUMN_NAME         = "name";
        public static final String COLUMN_PARENT_ID    = "parent_id";
        public static final String COLUMN_VENUE_ID     = "venue_id";
        public static final String COLUMN_VERSION      = "version";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PRODUCT_CATEGORY).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCT_CATEGORY;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCT_CATEGORY;

        public static Uri buildProductCategoryItemUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class NotificationEntry implements BaseColumns {
        public static final String TABLE_NAME           = "notification";
        public static final String COLUMN_USER_ID       = "user_id";
        public static final String COLUMN_TYPE          = "type";
        public static final String COLUMN_MESSAGE       = "message";
        public static final String COLUMN_DISCOUNT_CODE = "discount_code";
        public static final String COLUMN_LIST_ID       = "list_id";
        public static final String COLUMN_LIST_NAME     = "list_name";
        public static final String COLUMN_PRODUCT_ID    = "product_id";
        public static final String COLUMN_PRODUCT_NAME  = "product_name";
        public static final String COLUMN_PRODUCT_PRICE = "product_price";


        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_NOTIFICATION).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NOTIFICATION;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NOTIFICATION;

        public static Uri buildNotificationItemUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
