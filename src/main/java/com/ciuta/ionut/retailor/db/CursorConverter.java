package com.ciuta.ionut.retailor.db;

import android.database.Cursor;

import com.ciuta.ionut.retailor.db.RetailorDBContract.*;
import com.ciuta.ionut.retailor.pojo.ListItem;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.pojo.ProductCategory;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.pojo.Venue;

import java.util.Date;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class CursorConverter {

    public static Venue toVenue(Cursor c) {
        Venue venue = new Venue();
        venue.setId(pLong(c, VenueEntry._ID));
        venue.setName(pString(c, VenueEntry.COLUMN_NAME));
        venue.setDescription(pString(c, VenueEntry.COLUMN_DESCRIPTION));
        venue.setAddress(pString(c, VenueEntry.COLUMN_ADDRESS));
        venue.setxCoord(pLong(c, VenueEntry.COLUMN_X_COORD));
        venue.setyCoord(pLong(c, VenueEntry.COLUMN_Y_COORD));
        venue.setVersion(pLong(c, VenueEntry.COLUMN_VERSION));
        venue.setMondayOpen(pString(c, VenueEntry.COLUMN_MONDAY_OPEN));
        venue.setMondayClose(pString(c, VenueEntry.COLUMN_MONDAY_CLOSE));
        venue.setTuestayOpen(pString(c, VenueEntry.COLUMN_TUESDAY_OPEN));
        venue.setTuestayClose(pString(c, VenueEntry.COLUMN_TUESDAY_CLOSE));
        venue.setWednesdayOpen(pString(c, VenueEntry.COLUMN_WEDNESDAY_OPEN));
        venue.setWednesdayClose(pString(c, VenueEntry.COLUMN_WEDNESDAY_CLOSE));
        venue.setThursdayOpen(pString(c, VenueEntry.COLUMN_THURSDAY_OPEN));
        venue.setThursdayClose(pString(c, VenueEntry.COLUMN_THURSDAY_CLOSE));
        venue.setFridayOpen(pString(c, VenueEntry.COLUMN_FRIDAY_OPEN));
        venue.setFridayClose(pString(c, VenueEntry.COLUMN_FRIDAY_CLOSE));
        venue.setSaturdayOpen(pString(c, VenueEntry.COLUMN_SATURDAY_OPEN));
        venue.setSaturdayClose(pString(c, VenueEntry.COLUMN_SATURDAY_CLOSE));
        venue.setSundayOpen(pString(c, VenueEntry.COLUMN_SUNDAY_OPEN));
        venue.setSundayClose(pString(c, VenueEntry.COLUMN_SUNDAY_CLOSE));
        return venue;
    }

    public static ShoppingList toShoppingList(Cursor c) {
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.setId(pLong(c, ShoppingListEntry._ID));
        shoppingList.setName(pString(c, ShoppingListEntry.COLUMN_NAME));
        shoppingList.setDescription(pString(c, ShoppingListEntry.COLUMN_DESCRIPTION));
        shoppingList.setVersion(pLong(c, ShoppingListEntry.COLUMN_VERSION));

        // TODO: 11.06.2016 fix this
        shoppingList.setCreationDate(new Date());

        Venue venue = toVenue(c);
        shoppingList.setVenue(venue);
        return shoppingList;
    }

    public static ListItem toListItem(Cursor c) {
        ListItem item = new ListItem();
        item.setProduct(new Product());
        item.setLocalId(pLong(c, ListItemEntry.COLUMN_LOCAL_ID));
        item.setId(c.getLong(1));
        item.setTaken(pInt(c, ListItemEntry.COLUMN_TAKEN) != 0);
        item.getProduct().setId(c.getLong(5));
        return item;
    }

    public static ProductCategory toProductCategory(Cursor c) {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setId(pLong(c, ProductCategoryEntry._ID));
        productCategory.setVersion(pLong(c, ProductCategoryEntry.COLUMN_VERSION));
        productCategory.setName(pString(c, ProductCategoryEntry.COLUMN_NAME));
        return productCategory;
    }

    public static User toUser(Cursor c) {
        User user = new User();
        user.setId(pLong(c, UserEntry._ID));
        user.setEmail(pString(c, UserEntry.COLUMN_EMAIL));
        user.setStatus(pInt(c, UserEntry.COLUMN_STATUS));
        user.setLastName(pString(c, UserEntry.COLUMN_LASTNAME));
        user.setFirstName(pString(c, UserEntry.COLUMN_FIRSTNAME));
        return user;
    }

    private static String pString(Cursor c, String column) {
        return c.getString(c.getColumnIndex(column));
    }

    private static long pLong(Cursor c, String column) {
        return c.getLong(c.getColumnIndex(column));
    }

    private static int pInt(Cursor c, String column) {
        return c.getInt(c.getColumnIndex(column));
    }
}
