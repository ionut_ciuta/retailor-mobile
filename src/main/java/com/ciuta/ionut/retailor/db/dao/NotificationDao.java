package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.ciuta.ionut.retailor.db.RetailorDBContract.*;
import com.ciuta.ionut.retailor.pojo.notifications.DiscountNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RecommendationNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotificationType;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class NotificationDao extends Dao<RetailorNotification> {
    private final String TAG = getClass().getSimpleName();

    public NotificationDao(Context context) {
        super(context, NotificationEntry.CONTENT_URI);
    }

    @Override
    public ContentValues getContentValues(RetailorNotification entity) {
        ContentValues result = null;

        switch (entity.getType()) {
            case RetailorNotificationType.MESSAGE:
                Log.i(TAG, "MESSAGE NOTIFICATIONS SHOULD NOT BE STORED");
                break;

            case RetailorNotificationType.DISCOUNT:
                Log.i(TAG, "STORING DISCOUNT NOTIFICATION");
                result = getDiscountContentValues((DiscountNotification)entity);
                break;

            case RetailorNotificationType.RECOMNENDATION:
                Log.i(TAG, "STORING RECOMMENDATION NOTIFICATION");
                result = getRecommendationContentValues((RecommendationNotification)entity);
                break;
        }

        return result;
    }

    private ContentValues getDiscountContentValues(DiscountNotification notification) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NotificationEntry.COLUMN_USER_ID, notification.getUserId());
        contentValues.put(NotificationEntry.COLUMN_TYPE, notification.getType());
        contentValues.put(NotificationEntry.COLUMN_DISCOUNT_CODE, notification.getCode());
        contentValues.put(NotificationEntry.COLUMN_MESSAGE, notification.getMessage());
        return contentValues;
    }

    private ContentValues getRecommendationContentValues(RecommendationNotification notification) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NotificationEntry.COLUMN_USER_ID, notification.getUserId());
        contentValues.put(NotificationEntry.COLUMN_TYPE, notification.getType());
        contentValues.put(NotificationEntry.COLUMN_LIST_ID, notification.getShoppingListId());
        contentValues.put(NotificationEntry.COLUMN_LIST_NAME, notification.getShoppingListName());
        contentValues.put(NotificationEntry.COLUMN_PRODUCT_ID, notification.getProductId());
        contentValues.put(NotificationEntry.COLUMN_PRODUCT_NAME, notification.getProductName());
        contentValues.put(NotificationEntry.COLUMN_PRODUCT_PRICE, notification.getProductPrice());
        return contentValues;
    }

    @Override
    public List<RetailorNotification> selectAllAsList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Long insert(RetailorNotification entity) {
        Uri newUri = mContext.getContentResolver().insert(
                mUri,
                getContentValues(entity)
        );
        return ContentUris.parseId(newUri);
    }

    @Override
    public Integer update(RetailorNotification entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer delete(RetailorNotification entity) {
        return mContext.getContentResolver().delete(
                mUri,
                "_id = ?",
                new String[]{entity.getId().toString()}
        );
    }
}
