package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.ciuta.ionut.retailor.db.RetailorDBContract.ProductEntry;
import com.ciuta.ionut.retailor.pojo.Product;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductDao extends Dao<Product> {
    private final String TAG = this.getClass().getSimpleName();

    public ProductDao(Context context) {
        super(context);
        mUri = ProductEntry.CONTENT_URI;
    }

    @Override
    public ContentValues getContentValues(Product entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProductEntry._ID, entity.getId());
        contentValues.put(ProductEntry.COLUMN_VERSION, entity.getVersion());
        contentValues.put(ProductEntry.COLUMN_NAME, entity.getName());
        contentValues.put(ProductEntry.COLUMN_PRICE, entity.getPrice());
        contentValues.put(ProductEntry.COLUMN_DESCRIPTION, entity.getDescription());
        return contentValues;
    }

    @Override
    public List<Product> selectAllAsList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Long insert(Product entity) {
        Uri uri = mContext.getContentResolver().insert(mUri, getContentValues(entity));
        return ContentUris.parseId(uri);
    }

    @Override
    public Integer update(Product entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer delete(Product entity) {
        throw new UnsupportedOperationException();
    }
}
