package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.RetailorDBContract.VenueEntry;
import com.ciuta.ionut.retailor.pojo.Venue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueDao extends Dao<Venue> {
    private final String TAG = getClass().getSimpleName();
    public VenueDao(Context context) {
        super(context, RetailorDBContract.VenueEntry.CONTENT_URI);
    }

    @Override
    public ContentValues getContentValues(Venue entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(VenueEntry._ID, entity.getId());
        contentValues.put(VenueEntry.COLUMN_NAME, entity.getName());
        contentValues.put(VenueEntry.COLUMN_ADDRESS, entity.getAddress());
        contentValues.put(VenueEntry.COLUMN_DESCRIPTION, entity.getDescription());
        contentValues.put(VenueEntry.COLUMN_X_COORD, entity.getxCoord());
        contentValues.put(VenueEntry.COLUMN_Y_COORD, entity.getyCoord());
        contentValues.put(VenueEntry.COLUMN_VERSION, entity.getVersion());
        contentValues.put(VenueEntry.COLUMN_MONDAY_OPEN, entity.getMondayOpen());
        contentValues.put(VenueEntry.COLUMN_MONDAY_CLOSE, entity.getMondayClose());
        contentValues.put(VenueEntry.COLUMN_TUESDAY_OPEN, entity.getTuestayOpen());
        contentValues.put(VenueEntry.COLUMN_TUESDAY_CLOSE, entity.getTuestayClose());
        contentValues.put(VenueEntry.COLUMN_WEDNESDAY_OPEN, entity.getWednesdayOpen());
        contentValues.put(VenueEntry.COLUMN_WEDNESDAY_CLOSE, entity.getWednesdayClose());
        contentValues.put(VenueEntry.COLUMN_THURSDAY_OPEN, entity.getThursdayOpen());
        contentValues.put(VenueEntry.COLUMN_THURSDAY_CLOSE, entity.getThursdayClose());
        contentValues.put(VenueEntry.COLUMN_FRIDAY_OPEN, entity.getFridayOpen());
        contentValues.put(VenueEntry.COLUMN_FRIDAY_CLOSE, entity.getFridayClose());
        contentValues.put(VenueEntry.COLUMN_SATURDAY_OPEN, entity.getSaturdayOpen());
        contentValues.put(VenueEntry.COLUMN_SATURDAY_CLOSE, entity.getSaturdayClose());
        contentValues.put(VenueEntry.COLUMN_SUNDAY_OPEN, entity.getSundayOpen());
        contentValues.put(VenueEntry.COLUMN_SUNDAY_CLOSE, entity.getSaturdayClose());
        return contentValues;
    }

    public Venue getVenue(Long id) {
        Venue venue = null;
        Cursor cursor = mContext.getContentResolver().query(
                mUri,
                new String[]{"*"},
                "_id = ?",
                new String[]{id.toString()},
                null
        );

        if(cursor != null && cursor.moveToFirst()) {
            venue = new Venue();
            venue.setId(cursor.getLong(0));
            venue.setName(cursor.getString(1));
            venue.setAddress(cursor.getString(2));
        }

        return venue;
    }

    @Override
    public List<Venue> selectAllAsList() {
        List<Venue> venues = null;
        Cursor cursor = selectAll();

        if(cursor.getCount() > 0) {
            venues = new ArrayList<>();
            while(cursor.moveToNext()) {
                Venue venue = new Venue();
                venue.setId(cursor.getLong(0));
                venue.setName(cursor.getString(1));
                venue.setAddress(cursor.getString(2));
                venues.add(venue);
            }
        } else {
            Log.e(TAG, "NO VENUES");
        }

        return venues;
    }

    @Override
    public Long insert(Venue entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer update(Venue entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer delete(Venue entity) {
        throw new UnsupportedOperationException();
    }
}
