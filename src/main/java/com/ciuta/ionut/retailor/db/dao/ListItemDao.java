package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ciuta.ionut.retailor.db.RetailorDBContract.ListItemEntry;
import com.ciuta.ionut.retailor.pojo.ListItem;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ListItemDao extends SyncDao<ListItem>{
    private final String TAG = this.getClass().getSimpleName();

    public ListItemDao(Context context) {
        super(context, ListItemEntry.CONTENT_URI);
    }

    @Override
    public ContentValues getContentValues(ListItem entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ListItemEntry.COLUMN_LOCAL_ID, entity.getLocalId());
        contentValues.put(ListItemEntry._ID, entity.getId());
        contentValues.put(ListItemEntry.COLUMN_TAKEN, (entity.getTaken() ? 1 : 0));
        contentValues.put(ListItemEntry.COLUMN_VERSION, entity.getVersion());
        contentValues.put(ListItemEntry.COLUMN_LIST_ID, entity.getShoppingList().getLocalId());
        contentValues.put(ListItemEntry.COLUMN_PRODUCT_ID, entity.getProduct().getId());
        return contentValues;
    }

    @Override
    public int onUpdate(ListItem entity) {
        ContentValues syncedContentValues = getContentValuesForSync(entity);
        return mContext.getContentResolver().update(
                mUri,
                syncedContentValues,
                ListItemEntry.COLUMN_LOCAL_ID + " = ?",
                new String[]{entity.getLocalId().toString()}
        );
    }

    public int count(Long listId) {
        int result = 0;
        Cursor cursor = mContext.getContentResolver().query(
                mUri,
                new String[] {"count(*) AS count"},
                "list_id = ?",
                new String[]{listId.toString()},
                null
        );

        if(cursor != null) {
            cursor.moveToFirst();
            result = cursor.getInt(0);
            cursor.close();
        }

        return result;
    }

    public void deleteFromList(Long listId) {
        mContext.getContentResolver().delete(
                mUri,
                "list_id = ?",
                new String[]{listId.toString()}
        );
    }

    @Override
    public List<ListItem> selectAllAsList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Long insert(ListItem entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer update(ListItem entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer delete(ListItem entity) {
        throw new UnsupportedOperationException();
    }
}
