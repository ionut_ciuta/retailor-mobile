package com.ciuta.ionut.retailor.db.dao;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.ciuta.ionut.retailor.db.RetailorDBContract.ShoppingListEntry;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.util.IdentityHandler;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingListDao extends SyncDao<ShoppingList> {
    private final String TAG = this.getClass().toString();
    public ShoppingListDao(Context context) {
        super(context, ShoppingListEntry.CONTENT_URI);
    }

    @Override
    public ContentValues getContentValues(ShoppingList entity) {
        if(entity.getId() != null && entity.getLocalId() == null) {
            entity.setLocalId(entity.getId());
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(ShoppingListEntry.COLUMN_LOCAL_ID, entity.getLocalId());
        contentValues.put(ShoppingListEntry._ID, entity.getId());
        contentValues.put(ShoppingListEntry.COLUMN_VENUE_ID, entity.getVenue().getId());
        contentValues.put(ShoppingListEntry.COLUMN_NAME, entity.getName());
        contentValues.put(ShoppingListEntry.COLUMN_DESCRIPTION, entity.getDescription());
        contentValues.put(ShoppingListEntry.COLUMN_VERSION, entity.getVersion());

        if(entity.getUser() != null) {
            contentValues.put(ShoppingListEntry.COLUMN_USER_ID, entity.getUser().getId());
        }

        return contentValues;
    }

    @Override
    public int onUpdate(ShoppingList entity) {
        ContentValues syncedContentValues = getContentValuesForSync(entity);
        return mContext.getContentResolver().update(
                mUri,
                syncedContentValues,
                ShoppingListEntry.COLUMN_LOCAL_ID + " = ?",
                new String[]{entity.getLocalId().toString()}
        );
    }

    @Override
    public List<ShoppingList> selectAllAsList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Long insert(ShoppingList entity) {
        Uri newUri = mContext.getContentResolver().insert(
                mUri,
                getContentValues(entity)
        );
        return ContentUris.parseId(newUri);
    }

    @Override
    public Integer update(ShoppingList entity) {
        ContentValues contentValue = getContentValues(entity);
        return mContext.getContentResolver().update(
                mUri,
                contentValue,
                ShoppingListEntry._ID + " = ?",
                new String[]{entity.getId().toString()}
        );
    }

    @Override
    public Integer delete(ShoppingList entity) {
        return null;
    }

    @Override
    public Integer bulkInsert(List<ShoppingList> entities) {
        ContentValues[] values = new ContentValues[entities.size()];
        Long activeUserId = IdentityHandler.getActiveUserId(mContext);

        for(int i = 0; i < entities.size(); i++) {
            values[i] = getContentValues(entities.get(i));
            values[i].put(ShoppingListEntry.COLUMN_USER_ID, activeUserId);
        }
        return mContext.getContentResolver().bulkInsert(mUri, values);
    }
}
