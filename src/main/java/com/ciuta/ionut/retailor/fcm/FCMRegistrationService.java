package com.ciuta.ionut.retailor.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.pojo.FCMTokenContainer;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class FCMRegistrationService extends IntentService {
    private final String TAG = getClass().getSimpleName();
    private static final String name = "REGISTRATION_SERVICE";

    public FCMRegistrationService() {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        FirebaseInstanceId instanceId = FirebaseInstanceId.getInstance();
        String senderId = getResources().getString(R.string.gcm_defaultSenderId);
        try {
            String token = instanceId.getToken();
            Log.i(TAG, "FCM REG. TOKEN: " + token);

            String storedToken = IdentityHandler.getFcmToken(this);
            if(token != null && !token.equals(storedToken)) {
                IdentityHandler.storeFcmToken(token, this);
                sendTokenToServer(token);
            }

            IdentityHandler.storeFcmToken(token, this);
            sendTokenToServer(token);
        } catch (Exception e) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            preferences.edit().putBoolean(getString(R.string.key_sp_sent_fcm), false).apply();
            ErrorHandler.handleException(e);
        }
    }

    private void sendTokenToServer(String token) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.edit().putBoolean(getString(R.string.key_sp_sent_fcm), true).apply();

        VolleyQueueSingleton.getInstance(this).addRequest(new VolleyRequest<User>(
                Request.Method.PUT,
                NetUtils.getUserUrl(),
                User.class,
                new FCMTokenContainer(token),
                NetUtils.getAuthenticationHeader(IdentityHandler.getRetailorToken(this)),
                new Response.Listener<User>() {
                    @Override
                    public void onResponse(User response) {
                        Log.i(TAG, "FCM TOKEN SENT WITH SUCCESS");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ErrorHandler.handleVolleyError(error);
                    }
                }
        ));
    }
}
