package com.ciuta.ionut.retailor.fcm;

import android.content.Intent;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RetailorInstanceIDListenerService extends FirebaseInstanceIdService{

    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, FCMRegistrationService.class);
        startService(intent);
    }

}
