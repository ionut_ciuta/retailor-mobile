package com.ciuta.ionut.retailor.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.VenueActivity;
import com.ciuta.ionut.retailor.db.dao.NotificationDao;
import com.ciuta.ionut.retailor.pojo.notifications.DiscountNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RecommendationNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotificationType;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.messaging.RemoteMessage.*;

import java.util.Map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class FCMMessageHandler extends FirebaseMessagingService {
    private final String TAG = getClass().getSimpleName();
    private final int MESSAGE_NOTIFICATION_ID = 1;

    private final String KEY_DATA_TYPE      = "dataType";
    private final String KEY_VENUE_ID       = "venueId";
    private final String KEY_LIST_ID        = "shoppingListId";
    private final String KEY_LIST_NAME      = "shoppingListName";
    private final String KEY_PRODUCT_ID     = "productId";
    private final String KEY_PRODUCT_NAME   = "productName";
    private final String KEY_PRODUCT_PRICE  = "productPrice";
    private final String KEY_MESSAGE        = "message";
    private final String KEY_CODE           = "code";
    private final String KEY_MINUTES_LEFT   = "minutesLeft";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.i(TAG, "RECEIVED NOTIFICATION");
        handleNotification(remoteMessage);
    }

    private void handleNotification(RemoteMessage remoteMessage) {
        Map<String, String> notificationData = remoteMessage.getData();
        Integer type = Integer.parseInt(notificationData.get(KEY_DATA_TYPE));
        switch (type) {
            case RetailorNotificationType.DISCOUNT:
                Log.i(TAG, "RECEIVED DISCOUNT");
                handleDiscountNotification(remoteMessage);
                break;

            case RetailorNotificationType.MESSAGE:
                Log.i(TAG, "RECEIVED MESSAGE");
                handleMessageNotification(remoteMessage);
                break;

            case RetailorNotificationType.RECOMNENDATION:
                Log.i(TAG, "RECEIVED RECOMMENDATION");
                handleRecommendationNotification(remoteMessage);
                break;
        }
    }

    public void handleDiscountNotification(RemoteMessage data) {
        Log.i(TAG, "HANDLE DISCOUNT");
        NotificationCompat.Builder mBuilder = getPartialBuilder(data.getNotification());
        storeDiscountNotification(data.getData());

        Intent intent = new Intent(this, VenueActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        mBuilder.setSmallIcon(R.drawable.ic_shop);
        displayNotification(mBuilder.build());
    }

    public void handleRecommendationNotification(RemoteMessage data) {
        Log.i(TAG, "HANDLE RECOMMENDATION");
        NotificationCompat.Builder mBuilder = getPartialBuilder(data.getNotification());
        storeRecommendationNotification(data.getData());

        Intent intent = new Intent(this, VenueActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        mBuilder.setSmallIcon(R.drawable.ic_shop);
        displayNotification(mBuilder.build());
    }

    public void handleMessageNotification(RemoteMessage data) {
        Log.i(TAG, "HANDLE MESSAGE");
        NotificationCompat.Builder mBuilder = getPartialBuilder(data.getNotification());
        Long venueId = Long.parseLong(data.getData().get(KEY_VENUE_ID));

        Bundle args = new Bundle();
        args.putLong(getString(R.string.key_venue_id), venueId);

        Intent intent = new Intent(this, VenueActivity.class);
        intent.putExtra(getString(R.string.key_args), args);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        mBuilder.setSmallIcon(R.drawable.ic_shop);
        mBuilder.addAction(R.drawable.ic_shop,
                           getString(R.string.action_notification_check_message),
                           PendingIntent.getActivity(this, 0, intent, 0));

        displayNotification(mBuilder.build());
    }

    private void storeDiscountNotification(Map<String, String> data) {
        DiscountNotification obj = new DiscountNotification();
        obj.setUserId(IdentityHandler.getActiveUserId(this));
        obj.setType(RetailorNotificationType.DISCOUNT);
        obj.setMessage(data.get(KEY_MESSAGE));
        obj.setCode(data.get(KEY_CODE));
        new NotificationDao(this).insert(obj);
    }

    private void storeRecommendationNotification(Map<String, String> data) {
        RecommendationNotification obj = new RecommendationNotification();
        obj.setUserId(IdentityHandler.getActiveUserId(this));
        obj.setType(RetailorNotificationType.RECOMNENDATION);
        obj.setShoppingListId(Long.parseLong(data.get(KEY_LIST_ID)));
        obj.setShoppingListName(data.get(KEY_LIST_NAME));
        obj.setProductId(Long.parseLong(data.get(KEY_PRODUCT_ID)));
        obj.setProductName(data.get(KEY_PRODUCT_NAME));
        obj.setProductPrice(Float.parseFloat(data.get(KEY_PRODUCT_PRICE)));
        new NotificationDao(this).insert(obj);
    }

    private NotificationCompat.Builder getPartialBuilder(Notification notification) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setContentTitle(notification.getTitle());
        mBuilder.setContentText(notification.getBody());
        return mBuilder;
    }

    public void displayNotification(android.app.Notification notification) {
        NotificationManager mNotificationManager = (NotificationManager)
                getBaseContext().getSystemService(NOTIFICATION_SERVICE);

        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notification);
    }
}
