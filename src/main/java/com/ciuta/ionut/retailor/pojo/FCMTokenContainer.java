package com.ciuta.ionut.retailor.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class FCMTokenContainer implements Serializable {
    private List<FCMToken> fireBaseIds = new ArrayList<>();

    public FCMTokenContainer() {

    }

    public FCMTokenContainer(String token) {
        this.fireBaseIds.add(new FCMToken(token));
    }

    public List<FCMToken> getFireBaseIds() {
        return fireBaseIds;
    }

    public void setFireBaseIds(List<FCMToken> fireBaseIds) {
        this.fireBaseIds = fireBaseIds;
    }
}
