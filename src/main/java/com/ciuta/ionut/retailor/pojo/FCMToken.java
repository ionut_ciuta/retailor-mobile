package com.ciuta.ionut.retailor.pojo;

import java.io.Serializable;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class FCMToken implements Serializable {
    private String fireBaseId;

    public FCMToken() {

    }

    public FCMToken(String fireBaseId) {
        this.fireBaseId = fireBaseId;
    }

    public String getFireBaseId() {
        return fireBaseId;
    }

    public void setFireBaseId(String fireBaseId) {
        this.fireBaseId = fireBaseId;
    }
}
