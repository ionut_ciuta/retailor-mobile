package com.ciuta.ionut.retailor.pojo;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class Syncable extends Entity {
    protected Long localId;

    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }
}
