package com.ciuta.ionut.retailor.pojo.notifications;

import com.ciuta.ionut.retailor.pojo.ShoppingList;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RecommendationNotification extends RetailorNotification {
    private Long userId;
    private Long shoppingListId;
    private String shoppingListName;
    private Long productId;
    private String productName;
    private Float productPrice;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(Long shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    public String getShoppingListName() {
        return shoppingListName;
    }

    public void setShoppingListName(String shoppingListName) {
        this.shoppingListName = shoppingListName;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Float productPrice) {
        this.productPrice = productPrice;
    }
}
