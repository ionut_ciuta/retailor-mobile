package com.ciuta.ionut.retailor.pojo;

import java.io.Serializable;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ListItem extends Syncable {
    private ShoppingList shoppingList;
    private Product product;
    private Boolean taken = false;

    public ShoppingList getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }
}
