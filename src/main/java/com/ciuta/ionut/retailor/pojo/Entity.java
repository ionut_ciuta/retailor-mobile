package com.ciuta.ionut.retailor.pojo;

import java.io.Serializable;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class Entity implements Serializable {
    protected Long id;
    protected Long version = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
