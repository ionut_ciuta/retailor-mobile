package com.ciuta.ionut.retailor.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingList extends Syncable {
    private String name;
    private String description;
    private Date creationDate = new Date();
    private Venue venue;
    private User user;
    private List<ListItem> products = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ListItem> getProducts() {
        return products;
    }

    public void setProducts(List<ListItem> products) {
        this.products = products;
    }

    public void addItem(ListItem item) {
        this.products.add(item);
    }

    @Override
    public String toString() {
        return "ShoppingList{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", venue=" + venue +
                '}';
    }
}
