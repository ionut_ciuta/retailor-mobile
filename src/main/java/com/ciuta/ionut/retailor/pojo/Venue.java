package com.ciuta.ionut.retailor.pojo;

import android.util.Log;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class Venue extends Entity{
    private String name;
    private String address;
    private String description;
    private List<ShoppingList> shoppingLists;
    private Long xCoord;
    private Long yCoord;
    private String mondayOpen;
    private String mondayClose;
    private String tuestayOpen;
    private String tuestayClose;
    private String wednesdayOpen;
    private String wednesdayClose;
    private String thursdayOpen;
    private String thursdayClose;
    private String fridayOpen;
    private String fridayClose;
    private String saturdayOpen;
    private String saturdayClose;
    private String sundayOpen;
    private String sundayClose;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ShoppingList> getShoppingLists() {
        return shoppingLists;
    }

    public void setShoppingLists(List<ShoppingList> shoppingLists) {
        this.shoppingLists = shoppingLists;
    }

    public Long getxCoord() {
        return xCoord;
    }

    public void setxCoord(Long xCoord) {
        this.xCoord = xCoord;
    }

    public Long getyCoord() {
        return yCoord;
    }

    public void setyCoord(Long yCoord) {
        this.yCoord = yCoord;
    }

    public String getMondayOpen() {
        return mondayOpen;
    }

    public void setMondayOpen(String mondayOpen) {
        this.mondayOpen = mondayOpen;
    }

    public String getMondayClose() {
        return mondayClose;
    }

    public void setMondayClose(String mondayClose) {
        this.mondayClose = mondayClose;
    }

    public String getTuestayOpen() {
        return tuestayOpen;
    }

    public void setTuestayOpen(String tuestayOpen) {
        this.tuestayOpen = tuestayOpen;
    }

    public String getTuestayClose() {
        return tuestayClose;
    }

    public void setTuestayClose(String tuestayClose) {
        this.tuestayClose = tuestayClose;
    }

    public String getWednesdayOpen() {
        return wednesdayOpen;
    }

    public void setWednesdayOpen(String wednesdayOpen) {
        this.wednesdayOpen = wednesdayOpen;
    }

    public String getWednesdayClose() {
        return wednesdayClose;
    }

    public void setWednesdayClose(String wednesdayClose) {
        this.wednesdayClose = wednesdayClose;
    }

    public String getThursdayOpen() {
        return thursdayOpen;
    }

    public void setThursdayOpen(String thursdayOpen) {
        this.thursdayOpen = thursdayOpen;
    }

    public String getThursdayClose() {
        return thursdayClose;
    }

    public void setThursdayClose(String thursdayClose) {
        this.thursdayClose = thursdayClose;
    }

    public String getFridayOpen() {
        return fridayOpen;
    }

    public void setFridayOpen(String fridayOpen) {
        this.fridayOpen = fridayOpen;
    }

    public String getFridayClose() {
        return fridayClose;
    }

    public void setFridayClose(String fridayClose) {
        this.fridayClose = fridayClose;
    }

    public String getSaturdayOpen() {
        return saturdayOpen;
    }

    public void setSaturdayOpen(String saturdayOpen) {
        this.saturdayOpen = saturdayOpen;
    }

    public String getSaturdayClose() {
        return saturdayClose;
    }

    public void setSaturdayClose(String saturdayClose) {
        this.saturdayClose = saturdayClose;
    }

    public String getSundayOpen() {
        return sundayOpen;
    }

    public void setSundayOpen(String sundayOpen) {
        this.sundayOpen = sundayOpen;
    }

    public String getSundayClose() {
        return sundayClose;
    }

    public void setSundayClose(String sundayClose) {
        this.sundayClose = sundayClose;
    }

    @Override
    public String toString() {
        return "Venue{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public VenueStatus getVenueStatus() {
        String open = "", close = "";
        VenueStatus result = new VenueStatus();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.d("VENUE", currentHour + "");

        switch (day) {
            case Calendar.MONDAY:
                open = getMondayOpen();
                close = getMondayClose();
                break;

            case Calendar.TUESDAY:
                open = getTuestayOpen();
                close = getTuestayClose();
                break;

            case Calendar.WEDNESDAY:
                open = getWednesdayOpen();
                close = getWednesdayClose();
                break;

            case Calendar.THURSDAY:
                open = getThursdayOpen();
                close = getThursdayClose();
                break;

            case Calendar.FRIDAY:
                open = getFridayOpen();
                close = getFridayClose();
                break;

            case Calendar.SATURDAY:
                open = getSaturdayOpen();
                close = getSaturdayClose();
                break;

            case Calendar.SUNDAY:
                open = getSundayOpen();
                close = getSundayClose();
                break;
        }

        return getStatus(open, close, currentHour);
    }

    private VenueStatus getStatus(String open, String close, int currentHour) {
        VenueStatus result = new VenueStatus();
        String openHour, closeHour;
        int openHourNumeric, closeHourNumeric;

        openHour = open.substring(0, 2);
        closeHour = close.substring(0, 2);

        openHourNumeric = Integer.parseInt(openHour);
        closeHourNumeric = Integer.parseInt(closeHour);

        if(currentHour > openHourNumeric && currentHour < closeHourNumeric) {
            Log.d("VENUE", "CLOSES AT: " + close);
            result.setMessage("Closes at " + close);
            result.setStatus(true);
        } else {
            Log.d("VENUE", "OPENS AT: " + open);
            result.setMessage("Opens at " + open);
            result.setStatus(false);
        }

        return result;
    }
}
