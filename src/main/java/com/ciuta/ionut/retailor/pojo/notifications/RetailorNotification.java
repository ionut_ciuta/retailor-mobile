package com.ciuta.ionut.retailor.pojo.notifications;

import com.ciuta.ionut.retailor.pojo.Entity;

import java.io.Serializable;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class RetailorNotification extends Entity {
    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
