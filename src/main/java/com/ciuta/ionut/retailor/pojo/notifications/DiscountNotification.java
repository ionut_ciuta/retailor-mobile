package com.ciuta.ionut.retailor.pojo.notifications;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DiscountNotification extends RetailorNotification {
    private Long userId;
    private String message;
    private String code;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
