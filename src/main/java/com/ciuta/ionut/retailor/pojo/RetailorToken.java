package com.ciuta.ionut.retailor.pojo;

import java.io.Serializable;

public class RetailorToken implements Serializable {
	private static final long serialVersionUID = 1L;
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
