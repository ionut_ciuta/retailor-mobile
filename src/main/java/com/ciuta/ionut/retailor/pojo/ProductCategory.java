package com.ciuta.ionut.retailor.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductCategory extends Entity {
    private String name;
    private ProductCategory parentCategory;
    private Venue venue;
    private List<ProductCategory> subcategories = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductCategory getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(ProductCategory parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public List<ProductCategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<ProductCategory> subcategories) {
        this.subcategories = subcategories;
    }

    public void addSubcategory(ProductCategory subcategory) {
        this.subcategories.add(subcategory);
    }

    public boolean isParentCategory() {
        return subcategories.size() != 0;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
                "name='" + name + '\'' +
                ", subcategories=" + subcategories +
               '}';
    }
}
