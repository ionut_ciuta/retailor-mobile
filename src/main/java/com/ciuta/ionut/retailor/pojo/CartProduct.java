package com.ciuta.ionut.retailor.pojo;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class CartProduct extends Entity{
    private Product product;
    private ShoppingList list;
    private Boolean bought;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ShoppingList getList() {
        return list;
    }

    public void setList(ShoppingList list) {
        this.list = list;
    }

    public Boolean getBought() {
        return bought;
    }

    public void setBought(Boolean bought) {
        this.bought = bought;
    }
}
