package com.ciuta.ionut.retailor.pojo.notifications;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RetailorNotificationType {
    public static final int DISCOUNT = 1;
    public static final int RECOMNENDATION = 2;
    public static final int MESSAGE = 3;
}
