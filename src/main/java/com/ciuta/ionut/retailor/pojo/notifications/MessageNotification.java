package com.ciuta.ionut.retailor.pojo.notifications;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class MessageNotification extends RetailorNotification {
    private String message;
    private Long venueId;

    public Long getVenueId() {
        return venueId;
    }

    public void setVenueId(Long venueId) {
        this.venueId = venueId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
