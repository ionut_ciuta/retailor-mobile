package com.ciuta.ionut.retailor.adapters;

import android.database.Cursor;
import android.view.View;

import com.ciuta.ionut.retailor.db.RetailorDBContract.*;
import com.ciuta.ionut.retailor.fragments.ProductListFragment;
import com.ciuta.ionut.retailor.pojo.ListItem;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.viewholders.ListItemCursorViewHolder;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ListItemCursorAdapter extends RvCursorAdapter<ListItem, ListItemCursorViewHolder> {
    private ProductListFragment.Callback fragmentCallback;

    public ListItemCursorAdapter(Cursor cursor, int layoutId, View emptyView,
                                 ProductListFragment.Callback fragmentCallback) {
        super(cursor, layoutId, emptyView);
        this.fragmentCallback = fragmentCallback;
    }

    @Override
    protected ListItemCursorViewHolder getItemVhInstance(View itemView) {
        return new ListItemCursorViewHolder(itemView, new OnItemClickListener() {
            @Override
            public void onClickHandle(int itemPosition) {
                fragmentCallback.handle(getEntity(itemPosition));
            }
        });
    }

    @Override
    protected ListItem getEntity(int position) {
        mCursor.moveToPosition(position);
        ListItem item = new ListItem();
        item.setLocalId(mCursor.getLong(mCursor.getColumnIndex(ListItemEntry.COLUMN_LOCAL_ID)));
        item.setId(mCursor.getLong(mCursor.getColumnIndex(ListItemEntry._ID)));
        item.setTaken(mCursor.getInt(mCursor.getColumnIndex(ListItemEntry.COLUMN_TAKEN)) == 1);

        item.setProduct(new Product());
        item.getProduct().setName(mCursor.getString(mCursor.getColumnIndex(ProductEntry.COLUMN_NAME)));
        item.getProduct().setPrice(mCursor.getFloat(mCursor.getColumnIndex(ProductEntry.COLUMN_PRICE)));
        return item;
    }
}
