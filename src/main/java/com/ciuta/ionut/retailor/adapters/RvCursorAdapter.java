package com.ciuta.ionut.retailor.adapters;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciuta.ionut.retailor.viewholders.RvCursorViewHolder;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class RvCursorAdapter<T, VH extends RvCursorViewHolder<T>> extends RecyclerView.Adapter<VH> {
    Cursor mCursor;
    private int mLayoutId;
    private View mEmptyLayout;

    public RvCursorAdapter(Cursor cursor, int layoutId, View mEmptyLayout) {
        this.mCursor = cursor;
        this.mLayoutId = layoutId;
        this.mEmptyLayout = mEmptyLayout;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(mLayoutId, parent, false);
        return getItemVhInstance(itemView);
    }

    protected abstract VH getItemVhInstance(View itemView);

    @Override
    public void onBindViewHolder(VH holder, int position) {
        T obj = getEntity(position);
        holder.configure(obj);
    }

    @Override
    public int getItemCount() {
        int size = 0;

        if(mCursor != null) {
            size = mCursor.getCount();
        }

        return size;
    }

    /*
     * Replaces current cursor with a new one and updates UI;
     * If the cursor is empty, the empty view should be displayed
     */
    public void swapCursor(Cursor cursor) {
        mCursor = cursor;
        notifyDataSetChanged();
        if (mCursor != null) {
            if(mCursor.getCount() == 0) {
                mEmptyLayout.setVisibility(View.VISIBLE);
            } else {
                mEmptyLayout.setVisibility(View.GONE);
            }
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    /*
     * Use cursor to get element of type T; T should define a constructor relevant only for
     * UI management; for example, a Shop object should contain all useful data for displaying
     * the list such as name, address and so on.
     */
    protected abstract T getEntity(int position);

    public Cursor getCursor() {
        return mCursor;
    }

    public interface OnItemClickListener {
        void onClickHandle(int itemPosition);
    }
}
