package com.ciuta.ionut.retailor.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciuta.ionut.retailor.activities.MapActivity;
import com.ciuta.ionut.retailor.map.DrawerEntry;
import com.ciuta.ionut.retailor.map.MapHeader;
import com.ciuta.ionut.retailor.map.MapProduct;
import com.ciuta.ionut.retailor.map.MapRecommendation;
import com.ciuta.ionut.retailor.map.MapSale;
import com.ciuta.ionut.retailor.viewholders.DrawerViewHolder;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RvDrawerAdapter extends RvAdapter<DrawerEntry, DrawerViewHolder> {
    private final String TAG = getClass().getSimpleName();
    private int mHeaderViewId, mEntryViewId;
    private MapActivity.ActivityCallback mActivityCallback;

    public RvDrawerAdapter(List<DrawerEntry> content,
                           int headerLayoutId,
                           int entryLayoutId,
                           View mEmptyLayout,
                           MapActivity.ActivityCallback callback) {
        super(content, -1, mEmptyLayout);
        this.mHeaderViewId = headerLayoutId;
        this.mEntryViewId = entryLayoutId;
        this.mActivityCallback = callback;
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case DrawerEntry.TYPE_HEADER:
                itemView = inflater.inflate(mHeaderViewId, parent, false);
                break;

            case DrawerEntry.TYPE_PRODUCT:
            case DrawerEntry.TYPE_SALE:
            case DrawerEntry.TYPE_RECOMMENDATION:
                itemView = inflater.inflate(mEntryViewId, parent, false);
                break;

            default:
                throw new UnsupportedOperationException();
        }

        return getItemVhInstance(itemView, viewType);
    }

    protected DrawerViewHolder getItemVhInstance(View itemView, final int itemType) {
        switch (itemType) {
            case DrawerEntry.TYPE_HEADER:
                return new DrawerViewHolder(itemView, new OnItemClickListener() {
                    @Override
                    public void onClickHandler(int itemPosition) {
                        Log.i(TAG, "HEADER");
                        mActivityCallback.handle((MapHeader) getItem(itemType));
                    }
                });


            case DrawerEntry.TYPE_RECOMMENDATION:
                return new DrawerViewHolder(itemView, new OnItemClickListener() {
                    @Override
                    public void onClickHandler(int itemPosition) {
                        Log.i(TAG, "RECOMMENDATION");
                        mActivityCallback.draw((MapRecommendation) getItem(itemPosition));
                    }
                });

            case DrawerEntry.TYPE_PRODUCT:
                return new DrawerViewHolder(itemView, new OnItemClickListener() {
                    @Override
                    public void onClickHandler(int itemPosition) {
                        Log.i(TAG, "PRODUCT");
                        mActivityCallback.draw((MapProduct) getItem(itemPosition));
                    }
                });

            case DrawerEntry.TYPE_SALE:
                return new DrawerViewHolder(itemView, new OnItemClickListener() {
                    @Override
                    public void onClickHandler(int itemPosition) {
                        Log.i(TAG, "SALE");
                        mActivityCallback.draw((MapSale) getItem(itemPosition));
                    }
                });

            default:
                throw  new UnsupportedOperationException();
        }
    }

    @Override
    protected DrawerViewHolder getItemVhInstance(View itemView) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getItemViewType(int position) {
        DrawerEntry entry = content.get(position);
        return entry.getType();
    }

    public void removeItem(DrawerEntry entry) {
        notifyItemRemoved(content.indexOf(entry));
    }
}
