package com.ciuta.ionut.retailor.adapters;

import android.util.Log;
import android.view.View;

import com.ciuta.ionut.retailor.fragments.ProductListFragment;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.viewholders.SimpleProductViewHolder;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductAdapter extends RvAdapter<Product, SimpleProductViewHolder> {
    private final String TAG = this.getClass().getSimpleName();
    private ProductListFragment.Callback fragmentCallback;


    public ProductAdapter(List<Product> content, int layoutId, View emptyView,
                          ProductListFragment.Callback fragmentCallback) {
        super(content, layoutId, emptyView);
        this.fragmentCallback = fragmentCallback;
    }

    @Override
    protected SimpleProductViewHolder getItemVhInstance(final View itemView) {
        return new SimpleProductViewHolder(itemView, new RvAdapter.OnItemClickListener() {
            @Override
            public void onClickHandler(int itemPosition) {
                fragmentCallback.handle(getItem(itemPosition));
                notifyItemRemoved(itemPosition);
                Log.i(TAG, "REMOVED ITEM");
            }
        });
    }
}
