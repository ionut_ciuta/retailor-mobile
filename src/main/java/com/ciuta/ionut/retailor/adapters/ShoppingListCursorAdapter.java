package com.ciuta.ionut.retailor.adapters;

import android.database.Cursor;
import android.view.View;

import com.ciuta.ionut.retailor.fragments.ShoppingListsHomeFragment;
import com.ciuta.ionut.retailor.fragments.VenueDetailFragment;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.pojo.Venue;
import com.ciuta.ionut.retailor.viewholders.ShoppingListCursorViewHolder;

import java.util.Date;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingListCursorAdapter extends
        RvCursorAdapter<ShoppingList, ShoppingListCursorViewHolder> {
    final private String TAG = this.getClass().getSimpleName();
    private ShoppingListsHomeFragment.FragmentCallback homeListCallback;
    private VenueDetailFragment.FragmentCallback venueDetailCallback;

    public ShoppingListCursorAdapter(Cursor cursor, int layoutId, View emptyView,
                                     ShoppingListsHomeFragment.FragmentCallback callback) {
        super(cursor, layoutId, emptyView);
        this.homeListCallback = callback;
    }

    public ShoppingListCursorAdapter(Cursor cursor, int layoutId, View emptyView,
                                     VenueDetailFragment.FragmentCallback callback) {
        super(cursor, layoutId, emptyView);
        this.venueDetailCallback = callback;
    }

    @Override
    protected ShoppingListCursorViewHolder getItemVhInstance(View itemView) {
        return new ShoppingListCursorViewHolder(itemView, new OnItemClickListener() {
            @Override
            public void onClickHandle(int itemPosition) {
                ShoppingList item = getEntity(itemPosition);

                if(homeListCallback != null) {
                    homeListCallback.handle(item);
                    return;
                }

                if(venueDetailCallback != null) {
                    venueDetailCallback.handle(item);
                    return;
                }
            }
        });
    }

    @Override
    protected ShoppingList getEntity(int position) {
        mCursor.moveToPosition(position);
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.setLocalId(mCursor.getLong(0));
        shoppingList.setId(mCursor.getLong(1));
        shoppingList.setName(mCursor.getString(2));
        shoppingList.setDescription(mCursor.getString(3));
        shoppingList.setVersion(mCursor.getLong(4));
        shoppingList.setVenue(new Venue());
        shoppingList.getVenue().setId(mCursor.getLong(5));
        shoppingList.getVenue().setName(mCursor.getString(6));

        // TODO: 12.06.2016 fix this
        shoppingList.setCreationDate(new Date());
        return shoppingList;
    }
}
