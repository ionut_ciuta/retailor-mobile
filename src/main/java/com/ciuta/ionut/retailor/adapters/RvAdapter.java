package com.ciuta.ionut.retailor.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciuta.ionut.retailor.viewholders.RvViewHolder;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class RvAdapter<T, VH extends RvViewHolder<T>> extends RecyclerView.Adapter<VH> {
    private final String TAG = this.getClass().toString();
    protected List<T> content;
    private int layoutId;
    private View mEmptyLayout;

    public RvAdapter(List<T> content, int layoutId, View mEmptyLayout) {
        this.content = content;
        this.layoutId = layoutId;
        this.mEmptyLayout = mEmptyLayout;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutId, parent, false);
        return getItemVhInstance(itemView);
    }

    protected abstract VH getItemVhInstance(View itemView);

    @Override
    public void onBindViewHolder(VH holder, int position) {
        T obj = content.get(position);
        holder.configure(obj);
    }

    @Override
    public int getItemCount() {
        int result;

        if(content != null) {
            result = content.size();
        } else {
            result = 0;
        }

        return result;
    }

    public void setContent(List<T> newContent) {
        if(newContent.size() == 0) {
            mEmptyLayout.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.GONE);
        }

        content = newContent;
        notifyDataSetChanged();
        Log.i(TAG, getItemCount() + "");
    }

    public T getItem(int itemPosition) {
        return content.get(itemPosition);
    }

    public void appendContent(T obj) {
        content.add(obj);
        notifyItemInserted(getItemCount() - 1);
    }

    public interface OnItemClickListener {
        void onClickHandler(int itemPosition);
    }
}
