package com.ciuta.ionut.retailor.adapters;

import android.view.View;

import com.ciuta.ionut.retailor.fragments.ProductCategoryListFragment;
import com.ciuta.ionut.retailor.pojo.ProductCategory;
import com.ciuta.ionut.retailor.viewholders.ProductCategoryViewHolder;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductCategoryAdapter extends RvAdapter<ProductCategory, ProductCategoryViewHolder> {
    private ProductCategoryListFragment.Callback callback;

    public ProductCategoryAdapter(List<ProductCategory> content, int layoutId, View emptyView,
                                  ProductCategoryListFragment.Callback fragmentCallback) {
        super(content, layoutId, emptyView);
        this.callback = fragmentCallback;
    }

    @Override
    protected ProductCategoryViewHolder getItemVhInstance(View itemView) {
        return new ProductCategoryViewHolder(itemView, new RvAdapter.OnItemClickListener() {
            @Override
            public void onClickHandler(int itemPosition) {
                callback.handle(getItem(itemPosition));
            }
        });
    }
}
