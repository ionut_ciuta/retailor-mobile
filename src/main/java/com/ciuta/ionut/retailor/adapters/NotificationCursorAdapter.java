package com.ciuta.ionut.retailor.adapters;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciuta.ionut.retailor.db.RetailorDBContract.*;
import com.ciuta.ionut.retailor.fragments.NotificationsHomeFragment;
import com.ciuta.ionut.retailor.fragments.NotificationsHomeFragment.*;
import com.ciuta.ionut.retailor.pojo.notifications.DiscountNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RecommendationNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotificationType;
import com.ciuta.ionut.retailor.viewholders.DiscountCursorViewHolder;
import com.ciuta.ionut.retailor.viewholders.RecommendationCursorViewHolder;
import com.ciuta.ionut.retailor.viewholders.RetailorNotificationCursorViewHolder;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class NotificationCursorAdapter
        extends RvCursorAdapter<RetailorNotification, RetailorNotificationCursorViewHolder> {
    private final String TAG = getClass().getSimpleName();
    private int discountVenueId, recommendationLayoutId;
    private NotificationsHomeFragment.FragmentCallback fragmentCallback;
    private Context mContext;

    public NotificationCursorAdapter(Context context,
                                     Cursor cursor,
                                     int discountLayoutId,
                                     int recommendationLayoutId,
                                     View emptyView,
                                     FragmentCallback callback) {
        super(cursor, -1, emptyView);
        this.mContext = context;
        this.discountVenueId = discountLayoutId;
        this.recommendationLayoutId = recommendationLayoutId;
        this.fragmentCallback = callback;
    }

    @Override
    public RetailorNotificationCursorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        int type;

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case RetailorNotificationType.DISCOUNT:
                type = RetailorNotificationType.DISCOUNT;
                itemView = inflater.inflate(discountVenueId, parent, false);
                break;

            case RetailorNotificationType.RECOMNENDATION:
                type = RetailorNotificationType.RECOMNENDATION;
                itemView = inflater.inflate(recommendationLayoutId, parent, false);
                break;

            default:
                Log.e(TAG, "UNKNOWN NOTIFICATION TYPE");
                type = -1;
                itemView = null;
        }

        return getItemVhInstance(itemView, type);
    }

    protected RetailorNotificationCursorViewHolder getItemVhInstance(View itemView, int type) {
        switch (type) {
            case RetailorNotificationType.DISCOUNT:
                return new DiscountCursorViewHolder(
                        itemView,
                        new NotificationActionHandler() {
                            @Override
                            public void handleAction(int position) {
                                Log.i(TAG, "HANDLE DISCOUNT ACTION");
                                fragmentCallback.handleDiscount(
                                        (DiscountNotification)getEntity(position));
                            }

                            @Override
                            public void dismiss(int position) {
                                Log.i(TAG, "HANDLE DISCOUNT DISMISS");
                                notifyItemRemoved(position);
                                fragmentCallback.dismissNotification(getEntity(position));
                            }
                        });

            case RetailorNotificationType.RECOMNENDATION:
                return new RecommendationCursorViewHolder(
                        itemView,
                        new NotificationActionHandler() {
                            @Override
                            public void handleAction(int position) {
                                Log.i(TAG, "HANDLE RECOMMENDATION ACTION");
                                fragmentCallback.handleRecommendation(
                                        (RecommendationNotification)getEntity(position));
                            }

                            @Override
                            public void dismiss(int position) {
                                Log.i(TAG, "HANDLE RECOMMENDATION DISMISS");
                                notifyItemRemoved(position);
                                fragmentCallback.dismissNotification(getEntity(position));
                            }
                        });

            default:
                throw new UnsupportedOperationException();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected RetailorNotificationCursorViewHolder getItemVhInstance(View itemView) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getItemViewType(int position) {
        int type, result;
        mCursor.moveToPosition(position);
        type = mCursor.getInt(mCursor.getColumnIndex(NotificationEntry.COLUMN_TYPE));
        switch (type) {
            case RetailorNotificationType.DISCOUNT:
                result = RetailorNotificationType.DISCOUNT;
                break;

            case RetailorNotificationType.RECOMNENDATION:
                result = RetailorNotificationType.RECOMNENDATION;
                break;

            default:
                Log.e(TAG, "UNKNOWN NOTIFICATION VIEW TYPE");
                result = -1;
        }

        return result;
    }

    @Override
    protected RetailorNotification getEntity(int position) {
        RetailorNotification result;
        int type = getItemViewType(position);

        mCursor.moveToPosition(position);
        switch (type) {
            case RetailorNotificationType.DISCOUNT:
                DiscountNotification discount = new DiscountNotification();
                discount.setId(mCursor.getLong(mCursor.getColumnIndex(NotificationEntry._ID)));
                discount.setType(mCursor.getInt(mCursor.getColumnIndex(NotificationEntry.COLUMN_TYPE)));
                discount.setCode(mCursor.getString(mCursor.getColumnIndex(NotificationEntry.COLUMN_DISCOUNT_CODE)));
                discount.setMessage(mCursor.getString(mCursor.getColumnIndex(NotificationEntry.COLUMN_MESSAGE)));
                result = discount;
                break;

            case RetailorNotificationType.RECOMNENDATION:
                RecommendationNotification recommendation = new RecommendationNotification();
                recommendation.setId(mCursor.getLong(mCursor.getColumnIndex(NotificationEntry._ID)));
                recommendation.setType(mCursor.getInt(mCursor.getColumnIndex(NotificationEntry.COLUMN_TYPE)));
                recommendation.setShoppingListId(mCursor.getLong(mCursor.getColumnIndex(NotificationEntry.COLUMN_LIST_ID)));
                recommendation.setShoppingListName(mCursor.getString(mCursor.getColumnIndex(NotificationEntry.COLUMN_LIST_NAME)));
                recommendation.setProductId(mCursor.getLong(mCursor.getColumnIndex(NotificationEntry.COLUMN_PRODUCT_ID)));
                recommendation.setProductName(mCursor.getString(mCursor.getColumnIndex(NotificationEntry.COLUMN_PRODUCT_NAME)));
                recommendation.setProductPrice(mCursor.getFloat(mCursor.getColumnIndex(NotificationEntry.COLUMN_PRODUCT_PRICE)));
                result = recommendation;
                break;

            default:
                Log.e(TAG, "UNKNOWN NOTIFICATION ENTITY TYPE");
                result = null;
        }

        return result;
    }

    public interface NotificationActionHandler {
        void handleAction(int position);
        void dismiss(int position);
    }
}
