package com.ciuta.ionut.retailor.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.db.CursorConverter;
import com.ciuta.ionut.retailor.fragments.ProductCategoryListFragment;
import com.ciuta.ionut.retailor.pojo.ProductCategory;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.viewholders.ProductCategoryCursorViewHolder;
import com.squareup.picasso.Picasso;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductCategoryCursorAdapter
        extends RvCursorAdapter<ProductCategory, ProductCategoryCursorViewHolder> {
    private final String TAG = this.getClass().toString();

    private ProductCategoryListFragment.Callback callback;
    private Picasso mPicassoInstance;

    public ProductCategoryCursorAdapter(Context context, Cursor cursor, int layoutId, View emptyView,
                                        ProductCategoryListFragment.Callback callback) {
        super(cursor, layoutId, emptyView);
        this.callback = callback;
        this.mPicassoInstance = Picasso.with(context);
    }

    @Override
    public void onBindViewHolder(ProductCategoryCursorViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        mPicassoInstance.load(NetUtils.getCategoryIconUrl(getEntity(position).getId()))
                        .error(R.drawable.placeholder_category)
                        .into(holder.image);
    }

    @Override
    protected ProductCategoryCursorViewHolder getItemVhInstance(View itemView) {
        return new ProductCategoryCursorViewHolder(itemView, new OnItemClickListener() {
            @Override
            public void onClickHandle(int itemPosition) {
                callback.handle(getEntity(itemPosition));
            }
        });
    }

    @Override
    protected ProductCategory getEntity(int position) {
        mCursor.moveToPosition(position);
        return CursorConverter.toProductCategory(mCursor);
    }
}
