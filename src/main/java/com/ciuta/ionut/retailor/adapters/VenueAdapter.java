package com.ciuta.ionut.retailor.adapters;

import android.view.View;

import com.ciuta.ionut.retailor.dialogs.VenueDialog;
import com.ciuta.ionut.retailor.pojo.Venue;
import com.ciuta.ionut.retailor.viewholders.VenueViewHolder;

import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueAdapter extends RvAdapter<Venue, VenueViewHolder> {
    private VenueDialog.VenueSelector mVenueHandler;

    public VenueAdapter(List<Venue> content, int layoutId,
                        VenueDialog.VenueSelector handler) {
        super(content, layoutId, null);
        mVenueHandler = handler;
    }

    @Override
    public int getItemCount() {
        int result;

        if(content != null) {
            result = content.size();
        } else {
            result = 0;
        }

        return result;
    }

    @Override
    protected VenueViewHolder getItemVhInstance(View itemView) {
        return new VenueViewHolder(itemView, new OnItemClickListener() {
            @Override
            public void onClickHandler(int itemPosition) {
                mVenueHandler.handle(getItem(itemPosition));
            }
        });
    }
}
