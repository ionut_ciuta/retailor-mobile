package com.ciuta.ionut.retailor.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.db.CursorConverter;
import com.ciuta.ionut.retailor.fragments.VenuesHomeFragment;
import com.ciuta.ionut.retailor.pojo.Venue;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.viewholders.VenueCursorViewHolder;
import com.squareup.picasso.Picasso;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueCursorAdapter extends RvCursorAdapter<Venue, VenueCursorViewHolder> {
    private VenuesHomeFragment.Callback mFragmentCallback;
    private Picasso mPicassoInstance;

    public VenueCursorAdapter(Context context, Cursor cursor, int layoutId, View emptyView,
                              VenuesHomeFragment.Callback fragmentCallback) {
        super(cursor, layoutId, emptyView);
        this.mFragmentCallback = fragmentCallback;
        this.mPicassoInstance = Picasso.with(context);
    }

    @Override
    public void onBindViewHolder(VenueCursorViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        mPicassoInstance.load(NetUtils.getVenueIconUrl(getEntity(position).getId()))
                        .error(R.drawable.placeholder_venue)
                        .into(holder.background);
    }

    @Override
    protected VenueCursorViewHolder getItemVhInstance(View itemView) {
        return new VenueCursorViewHolder(itemView, new OnItemClickListener() {
            @Override
            public void onClickHandle(int itemPostion) {
                mFragmentCallback.handle(getEntity(itemPostion));
            }
        });
    }

    @Override
    protected Venue getEntity(int position) {
        mCursor.moveToPosition(position);
        return CursorConverter.toVenue(mCursor);
    }
}
