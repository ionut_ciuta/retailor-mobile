package com.ciuta.ionut.retailor.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ciuta.ionut.retailor.R;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class IdentityHandler {
    private static final String TAG = "IdentityHandler.java";

    public static void storeActiveUserId(Long activeUserId, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putLong(context.getString(R.string.key_sp_id), activeUserId).apply();
    }

    public static void storeRetailorToken(String token, Context context) {
        storeToken(context.getString(R.string.key_sp_token_retailor), token, context);
    }

    public static void storeFcmToken(String token, Context context) {
        storeToken(context.getString(R.string.key_sp_token_fcm), token, context);
    }

    public static Long getActiveUserId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(context.getString(R.string.key_sp_id), -1L);
    }

    public static String getRetailorToken(Context context) {
        String token = getToken(context.getString(R.string.key_sp_token_retailor), context);
        Log.i(TAG, "FETCH RETAILOR TOKEN: " + token);
        return token;
    }

    public static String getFcmToken(Context context) {
        String token = getToken(context.getString(R.string.key_sp_token_fcm), context);
        Log.i(TAG, "FETCH FCM TOKEN: " + token);
        return token;
    }

    private static void storeToken(String key, String token, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(key, token)
                .apply();
    }

    private static String getToken(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }
}
