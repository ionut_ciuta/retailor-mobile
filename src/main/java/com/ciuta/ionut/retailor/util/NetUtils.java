package com.ciuta.ionut.retailor.util;

import android.net.Uri;

import java.util.HashMap;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class NetUtils {
    private static final String SCHEME                      = "http";
    private static final String SERVER                      = "192.168.0.102:8080";
    private static final String API                         = "api";

    private static final String URL_SIGNUP                  = "signup";
    private static final String URL_LOGIN                   = "login";
    private static final String URL_USER                    = "user";
    private static final String URL_VENUES                  = "venues";
    private static final String URL_VENUE_CATEGORIES_TREE   = "tree";
    private static final String URL_SHOPPING_LISTS          = "shopping_lists";
    private static final String URL_PRODUCT_CATEGORIES      = "product_categories";
    private static final String URL_PRODUCTS                = "products";
    private static final String URL_ICONS                   = "icons";
    private static final String URL_PATH                    = "compute_path";

    private static final String ICON_ENTITY_USER        = "0";
    private static final String ICON_ENTITY_VENUE       = "1";
    private static final String ICON_ENTITY_CATEGORY    = "2";

    private static final String ICON_TYPE_THUMB         = "0";
    private static final String ICON_TYPE_FULL          = "1";

    public static HashMap<String, String> getAuthenticationHeader(String token) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("token", token);
        return headers;
    }

    private static Uri.Builder getURL() {
        Uri.Builder urlBuilder = new Uri.Builder();
        urlBuilder = urlBuilder.scheme(SCHEME)
                        .encodedAuthority(SERVER)
                        .appendPath(API);
        return urlBuilder;
    }

    public static String getLoginUrl() {
        return getURL().appendPath(URL_USER)
                       .appendPath(URL_LOGIN)
                       .toString();
    }

    public static String getSignupUrl() {
        return getURL().appendPath(URL_USER)
                .appendPath(URL_SIGNUP)
                .toString();
    }

    public static String getVenuesUrl() {
        return getURL().appendPath(URL_VENUES)
                       .toString();
    }

    public static String getCategoryTreeUrl(Long venueId) {
        return getURL().appendPath(URL_PRODUCT_CATEGORIES)
                       .appendPath(URL_VENUE_CATEGORIES_TREE)
                       .appendPath(venueId.toString())
                       .toString();
    }

    public static String getUserUrl() {
        return getURL().appendPath(URL_USER)
                       .toString();
    }

    public static String getShoppingListsUrl() {
        return getURL().appendPath(URL_SHOPPING_LISTS)
                       .toString();
    }

    public static String getShoppingListUrl(Long listId) {
        return getURL().appendPath(URL_SHOPPING_LISTS)
                       .appendPath(listId.toString())
                       .toString();
    }

    public static String getProductsUrl(Long categoryId) {
        return getURL().appendPath(URL_PRODUCTS)
                       .appendQueryParameter("in_category", categoryId.toString())
                       .toString();
    }

    public static String getUserIconUrl(Long userId) {
        return getIconUrl(ICON_ENTITY_USER, userId);
    }

    public static String getVenueIconUrl(Long venueId) {
        return getIconUrl(ICON_ENTITY_VENUE, venueId);
    }

    public static String getCategoryIconUrl(Long categoryId) {
        return getIconUrl(ICON_ENTITY_CATEGORY, categoryId);
    }

    public static String getIconUrl(String entityType, Long id) {
        return getURL().appendPath(URL_ICONS)
                .appendPath(entityType)
                .appendPath(id.toString())
                .appendPath(ICON_TYPE_THUMB)
                .toString();
    }

    public static String getPathUrl(Long listId, String ip) {
        return getURL().appendPath(URL_PATH)
                       .appendPath(listId.toString())
                       .appendQueryParameter("ip", ip)
                       .toString();
    }
}
