package com.ciuta.ionut.retailor.util;

import java.util.Date;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public interface DateHandler {
    void onDateSelected(Date date);
}
