package com.ciuta.ionut.retailor.util;

import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.pojo.ProductCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DummyData {
    public static List<ProductCategory> getCategories() {
        List<ProductCategory> productCategories = new ArrayList<>();

        ProductCategory sweets = new ProductCategory();
        sweets.setId(3L);
        sweets.setVersion(1L);
        sweets.setName("Sweets");

        ProductCategory beer = new ProductCategory();
        beer.setId(4L);
        beer.setVersion(1L);
        beer.setName("Beer");

        ProductCategory food = new ProductCategory();
        food.setId(1L);
        food.setVersion(1L);
        food.setName("Food");

        ProductCategory drinks = new ProductCategory();
        drinks.setId(2L);
        drinks.setVersion(1L);
        drinks.setName("Drinks");

        food.addSubcategory(sweets);
        drinks.addSubcategory(beer);

        productCategories.add(food);
        productCategories.add(drinks);
        return productCategories;
    }

    public static List<Product> getSweets() {
        List<Product> sweets = new ArrayList<>();

        Product crackers = new Product();
        crackers.setId(2L);
        crackers.setVersion(1L);
        crackers.setName("Crackers");
        crackers.setDescription("Some crackers");
        crackers.setPrice((float)8.2);

        Product chocolate = new Product();
        chocolate.setId(1L);
        chocolate.setVersion(1L);
        chocolate.setName("Chocolate");
        chocolate.setDescription("Some chocolate");
        chocolate.setPrice((float)8.2);

        sweets.add(chocolate);
        sweets.add(crackers);
        return sweets;
    }

    public static List<Product> getDrinks() {
        List<Product> drinks = new ArrayList<>();

        Product tuborg = new Product();
        tuborg.setId(3L);
        tuborg.setVersion(1L);
        tuborg.setName("Tuborg");
        tuborg.setDescription("Some beer");
        tuborg.setPrice((float)5.1);

        Product stejarul = new Product();
        stejarul.setId(3L);
        tuborg.setVersion(1L);
        tuborg.setName("Stajarul");
        tuborg.setDescription("Some beer");
        tuborg.setPrice((float)2.1);

        drinks.add(tuborg);
        drinks.add(stejarul);
        return drinks;
    }
}
