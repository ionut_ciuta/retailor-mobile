package com.ciuta.ionut.retailor.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.activities.AuthenticationActivity;
import com.ciuta.ionut.retailor.db.dao.UserDao;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ErrorHandler {
    private static final String VOLLEY_TAG = "VOLLEY ERROR HANDLER";
    private static final String EXCEPTION_TAG = "EXCEPTION";

    public static void handleVolleyError(VolleyError error) {
        String message, code;
        if(error != null && error.networkResponse != null) {
            try {
                code = String.valueOf(error.networkResponse.statusCode);
                message = new String(error.networkResponse.data, "UTF-8");
                Log.e(VOLLEY_TAG, code + " => " + message);
            } catch (Exception e) {
                Log.e(VOLLEY_TAG, e.getMessage());
                error.printStackTrace();
            }
        } else {
            Log.e(VOLLEY_TAG, "NO INTERNET CONNECTION");
        }
    }

    public static void handleVolleyError(VolleyError error, Context context) {
        String message, code;
        if(error != null && error.networkResponse != null) {
            try {
                code = String.valueOf(error.networkResponse.statusCode);
                message = new String(error.networkResponse.data, "UTF-8");

                if(code.equals("401")) {
                    Toast.makeText(
                            context,
                            context.getString(R.string.message_error_invalidtoken),
                            Toast.LENGTH_SHORT
                    ).show();
                    new SignOutTask().execute(context);
                }

                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                Log.e(VOLLEY_TAG, code + " => " + message);
            } catch (Exception e) {
                Log.e(VOLLEY_TAG, e.getMessage());
                error.printStackTrace();
            }
        } else {
            Log.e(VOLLEY_TAG, "NO INTERNET CONNECTION");
            Toast.makeText(
                    context,
                    context.getString(R.string.message_error_nointernet),
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    public static void handleException(Exception e) {
        Log.e(EXCEPTION_TAG, e.getMessage());
        e.printStackTrace();
    }

    private static class SignOutTask extends AsyncTask<Context, Void, Context> {
        Account[] accounts;

        @Override
        protected Context doInBackground(Context... params) {
            AccountManager accountManager = AccountManager.get(params[0]);
            accounts = accountManager.getAccountsByType(
                    params[0].getString(R.string.retailor_account_type));

            if(accounts.length == 1) {
                String token = accountManager.peekAuthToken(
                        accounts[0], params[0].getString(R.string.auth_token_type_full));
                accountManager.invalidateAuthToken(
                        params[0].getString(R.string.retailor_account_type), token);
                new UserDao(params[0]).deactivateUser();
            } else {
                // TODO: 10.06.2016 handle multiple accounts
                throw new UnsupportedOperationException("Multiple accounts!");
            }

            return params[0];
        }

        @Override
        protected void onPostExecute(Context context) {
            Bundle authBundle = new Bundle();
            authBundle.putBoolean(context.getString(R.string.auth_arg_new), false);
            authBundle.putBoolean(context.getString(R.string.auth_arg_just_finish), true);
            authBundle.putString(context.getString(R.string.auth_arg_account_type), accounts[0].type);
            authBundle.putString(context.getString(R.string.auth_arg_account_name), accounts[0].name);
            authBundle.putString(context.getString(R.string.auth_arg_token_type), context.getString(R.string.auth_token_type_full));
            Intent intent = new Intent(context, AuthenticationActivity.class);
            intent.putExtras(authBundle);
            context.startActivity(intent);
        }
    }
}
