package com.ciuta.ionut.retailor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.db.dao.VenueDao;
import com.ciuta.ionut.retailor.fragments.VenueDetailFragment;
import com.ciuta.ionut.retailor.pojo.Venue;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueActivity extends ContainerActivity {
    private final String TAG = getClass().getSimpleName();

    @Override
    protected void setup(Intent intent) {
        if(hasArgs(intent)) {
            Bundle args = getArgs(intent);

            if(args.containsKey(getString(R.string.key_venue))) {
                Venue arg = (Venue) args.getSerializable(getString(R.string.key_venue));
                setFragment(VenueDetailFragment.getInstance(arg));
                return;
            }

            if(args.containsKey(getString(R.string.key_venue_id))) {
                Long venueId = (Long) args.getLong(getString(R.string.key_venue_id));
                Venue venue = new VenueDao(this).getVenue(venueId);
                setFragment(VenueDetailFragment.getInstance(venue));
                return;
            }

        } else {
            Log.e(TAG, "You got here by mistake.");
        }
    }
}
