package com.ciuta.ionut.retailor.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.db.dao.ShoppingListDao;
import com.ciuta.ionut.retailor.db.dao.UserDao;
import com.ciuta.ionut.retailor.db.dao.VenueDao;
import com.ciuta.ionut.retailor.fragments.SignInFragment;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.pojo.Venue;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

import java.util.Arrays;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class AuthenticationActivity extends AccountAuthenticatorActivity {
    private final String TAG = this.getClass().getSimpleName();
    private final int TASK_COUNT = 3;
    private int ready = 0;

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_container);
        String accountName = getIntent().getStringExtra(getString(R.string.auth_arg_account_name));

        SignInFragment signInFragment = SignInFragment.getInstance(
                accountName,
                new AuthenticationHandler() {
                    @Override
                    public void onAuthenticationDone(User user, String token) {
                        manageAccounts(user, token);
                    }

                    @Override
                    public void onAuthenticationError(Exception e) {
                        Log.e(TAG, "Authentication error");
                        Toast.makeText(
                                getBaseContext(),
                                getString(R.string.message_error_signin),
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                },
                new FragmentHandler() {
                    @Override
                    public void handle(Fragment fragment) {
                        setFragmentAndEnableBack(fragment);
                    }
                }
        );

        setFragment(signInFragment);
    }

    private void manageAccounts(User user, String token) {
        IdentityHandler.storeRetailorToken(token, this);

        String accountType = getIntent().getStringExtra(getString(R.string.auth_arg_account_type));
        String tokenType = getIntent().getStringExtra(getString(R.string.auth_arg_token_type));
        Log.i(TAG, "manageAccounts: " + accountType + " " + tokenType);

        String accountName = user.getEmail();
        String password = user.getPassword();
        Log.i(TAG, "manageAccounts: " + accountName + " " + password);

        /* Contains necessary data for account manager to setup account*/
        Bundle authResult = new Bundle();
        authResult.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
        authResult.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
        authResult.putString(AccountManager.KEY_PASSWORD, password);
        authResult.putString(AccountManager.KEY_AUTHTOKEN, token);

        /* Result is ok, even if data will not be downloaded */
        Intent authIntent = new Intent();
        authIntent.putExtras(authResult);
        setAccountAuthenticatorResult(authResult);
        setResult(RESULT_OK, authIntent);

        AccountManager accountManager = AccountManager.get(this);
        Account account = new Account(accountName, accountType);

        Boolean newAccount = getIntent().getBooleanExtra(getString(R.string.auth_arg_new), false);
        UserDao userDao = new UserDao(this);
        if (newAccount) {
            Log.i(TAG, "manageAccounts - ADDING NEW ACCOUNT: " + accountName);
            accountManager.addAccountExplicitly(account, password, null);
            accountManager.setAuthToken(account, tokenType, token);

            Long activeUserId = userDao.insertActive(user);
            IdentityHandler.storeActiveUserId(activeUserId, this);
            Log.i(TAG, "ACTIVE USER ID: " + activeUserId);

            syncUserData(accountName, token);
        } else {
            Log.i(TAG, "manageAccounts - SETUP EXISTING: (" + accountName + ", " + password + ")");
            accountManager.setPassword(account, password);
            accountManager.setAuthToken(account, tokenType, token);

            userDao.activateUser(accountName);
            Long activeUserId = userDao.getActiveUserId();
            Log.i(TAG, "ACTIVE USER ID: " + activeUserId);
            IdentityHandler.storeActiveUserId(activeUserId, this);

            navigate();
        }
    }

    private void syncUserData(String accountName, String token) {
        final UserDao userDao = new UserDao(this);
        final VenueDao venueDao = new VenueDao(this);
        final ShoppingListDao shoppingListDao = new ShoppingListDao(this);

        Log.i(TAG, "NEW USER FOUND; FETCH DATA");
        VolleyQueueSingleton.getInstance(this).addRequest(new VolleyRequest<>(
                NetUtils.getUserUrl(),
                User.class,
                NetUtils.getAuthenticationHeader(token),
                new Response.Listener<User>() {
                    @Override
                    public void onResponse(User response) {
                        Log.i(TAG, "FETCHED USER");
                        userDao.update(response);
                        markReady();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(TAG, "UNABLE TO FETCH USER");
                        ErrorHandler.handleVolleyError(error);
                        markReady();
                    }
                }
        ));

        VolleyQueueSingleton.getInstance(this).addRequest(new VolleyRequest<>(
                NetUtils.getVenuesUrl(),
                Venue[].class,
                NetUtils.getAuthenticationHeader(token),
                new Response.Listener<Venue[]>() {
                    @Override
                    public void onResponse(Venue[] response) {
                        Log.i(TAG, "FETCHED VENUES");
                        venueDao.bulkInsert(Arrays.asList(response));
                        markReady();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "UNABLE TO FETCH VENUES");
                        ErrorHandler.handleVolleyError(error);
                        markReady();
                    }
                }
        ).setTag(this.getString(R.string.volley_tag_venues)));

        VolleyQueueSingleton.getInstance(this).addRequest(new VolleyRequest<>(
                NetUtils.getShoppingListsUrl(),
                ShoppingList[].class,
                NetUtils.getAuthenticationHeader(token),
                new Response.Listener<ShoppingList[]>() {
                    @Override
                    public void onResponse(ShoppingList[] response) {
                        Log.i(TAG, "FETCHED SHOPPING LISTS");
                        shoppingListDao.bulkInsert(Arrays.asList(response));
                        markReady();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "UNABLE TO FETCH LISTS");
                        ErrorHandler.handleVolleyError(error);
                        markReady();
                    }
                }
        ).setTag(this.getString(R.string.volley_tag_lists)));
    }

    protected void setFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_content, fragment)
                .commit();
    }

    protected void setFragmentAndEnableBack(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_content, fragment)
                .addToBackStack(null)
                .commit();
    }

    private synchronized void markReady() {
        Log.i(TAG, "Tasks ready: " + ++ready);
        if(ready == TASK_COUNT) {
            navigate();
        }
    }

    private void navigate() {
        if(getIntent().getBooleanExtra(getString(R.string.auth_arg_go_home), false)) {
            startActivity(new Intent(this, HomeActivity.class));
        }
        finish();
    }

    public interface AuthenticationHandler {
        void onAuthenticationDone(User user, String token);
        void onAuthenticationError(Exception exception);
    }

    public interface FragmentHandler {
        void handle(Fragment fragment);
    }
}