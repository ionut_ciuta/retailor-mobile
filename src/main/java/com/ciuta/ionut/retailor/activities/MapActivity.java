package com.ciuta.ionut.retailor.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvDrawerAdapter;
import com.ciuta.ionut.retailor.db.dao.ListItemDao;
import com.ciuta.ionut.retailor.db.dao.ProductDao;
import com.ciuta.ionut.retailor.map.MapHighlight;
import com.ciuta.ionut.retailor.map.MapRecommendation;
import com.ciuta.ionut.retailor.map.Pair;
import com.ciuta.ionut.retailor.map.PathComponent;
import com.ciuta.ionut.retailor.map.ComputedPath;
import com.ciuta.ionut.retailor.map.DrawerEntry;
import com.ciuta.ionut.retailor.map.MapHeader;
import com.ciuta.ionut.retailor.map.MapProduct;
import com.ciuta.ionut.retailor.map.MapSale;
import com.ciuta.ionut.retailor.map.Point2D;
import com.ciuta.ionut.retailor.pojo.ListItem;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.ciuta.ionut.retailor.util.IdentityHandler;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.ciuta.ionut.retailor.volley.VolleyQueueSingleton;
import com.ciuta.ionut.retailor.volley.VolleyRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class MapActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();
    private final long TEST_LIST_ID = 1L;
    private final String CMX_IP = "10.10.20.168";
    private final int X_OFFSET = 0;
    private final int Y_OFFSET = 72;
    private final float SCREEN_W = 1920;
    private final float SCREEN_H = 1080;
    private final float MAP_W = 1700;
    private final float MAP_H = 810;
    private final float RATIO = Math.min(SCREEN_W/MAP_W, SCREEN_H/MAP_H);

    /*LIST*/
    private ShoppingList mShoppingList;
    private boolean mDirtyList = false;
    private Product mCurrentProduct;

    /*PATH*/
    private FrameLayout mFrameLayout;
    private ImageView mIvMap;
    private Animation.AnimationListener mAnimationListener;
    private ComputedPath mPath;
    private int mCurrentComponent = 0;
    private ArrayList<PathComponent> mComponents = new ArrayList<>();
    private ArrayList<Animation> mAnimations = new ArrayList<>();

    /*HIGHLIGHTS*/
    private ArrayList<MapHighlight> mHighlights = new ArrayList<>();
    private int mHighlightIndex = -1;
    private TextView mHighlightInfo;
    private ImageButton mAddButton;
    private ImageButton mRefresh;
    private ProgressBar mProgressBar;

    private RecyclerView mDrawerRv;
    private RvDrawerAdapter mDrawerAdapter;

    private void initViews() {
        mFrameLayout = (FrameLayout)findViewById(R.id.fl_map);
        mIvMap = (ImageView)findViewById(R.id.iv_map);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mRefresh = (ImageButton)findViewById(R.id.btn_redraw);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDirtyList) {
                    saveList();
                } else {
                    Toast.makeText(getBaseContext(), "The list was not modified.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mHighlightInfo = (TextView)findViewById(R.id.tv_highlight);
        mAddButton = (ImageButton) findViewById(R.id.btn_add_list);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "ADD TO LIST: " + mCurrentProduct.getName());
                Toast.makeText(getBaseContext(), "Added to list!", Toast.LENGTH_SHORT).show();

                ShoppingList dummyList = new ShoppingList();
                dummyList.setLocalId(mShoppingList.getLocalId());
                dummyList.setId(mShoppingList.getId());

                ListItem newItem = new ListItem();
                newItem.setProduct(mCurrentProduct);
                newItem.setShoppingList(dummyList);
                mShoppingList.getProducts().add(newItem);
                mDirtyList = true;

                if(mRefresh.getVisibility() != View.VISIBLE) {
                    mRefresh.startAnimation(AnimationUtils.loadAnimation(
                            getBaseContext(),
                            R.anim.fade_in
                    ));
                    mRefresh.setVisibility(View.VISIBLE);
                }
            }
        });


        mDrawerRv = (RecyclerView)findViewById(R.id.rvDrawer);
        mDrawerAdapter = new RvDrawerAdapter(
                null,
                R.layout.item_drawer_header,
                R.layout.item_drawer_entry,
                findViewById(R.id.tvEmpty),
                new ActivityCallback() {
                    @Override
                    public void draw(MapProduct product) {
                        Log.i(TAG, "DRAW PRODUCT");
                        MapHighlight mHighlight = new MapHighlight(
                                getBaseContext(),
                                product.getX(),
                                product.getY(),
                                product.getType()
                        );
                        addHighlightToMap(mHighlight);
                        mHighlightInfo.setVisibility(View.VISIBLE);
                        mHighlightInfo.setText(product.getName());
                        mHighlightInfo.setBackgroundColor(0xff0277BD);
                        mAddButton.setVisibility(View.GONE);
                    }

                    @Override
                    public void draw(MapSale sale) {
                        Log.i(TAG, "DRAW SALE");
                        Log.i(TAG, "DRAW PRODUCT");
                        MapHighlight mHighlight = new MapHighlight(
                                getBaseContext(),
                                sale.getX(),
                                sale.getY(),
                                sale.getType()
                        );
                        addHighlightToMap(mHighlight);
                        mHighlightInfo.setVisibility(View.VISIBLE);
                        mHighlightInfo.setText(sale.getText());
                        mHighlightInfo.setBackgroundColor(0xffB61C1C);
                        mAddButton.setVisibility(View.VISIBLE);

                        mCurrentProduct = mPath.getSales().get(
                                sale.getOriginalListIndex()).getFirst();
                    }

                    @Override
                    public void draw(MapRecommendation recommendation) {
                        Log.i(TAG, "DRAW SALE");
                        Log.i(TAG, "DRAW PRODUCT");
                        MapHighlight mHighlight = new MapHighlight(
                                getBaseContext(),
                                recommendation.getX(),
                                recommendation.getY(),
                                recommendation.getType()
                        );
                        addHighlightToMap(mHighlight);
                        mHighlightInfo.setVisibility(View.VISIBLE);
                        mHighlightInfo.setText(recommendation.getName());
                        mHighlightInfo.setBackgroundColor(0xffFFA726);
                        mAddButton.setVisibility(View.VISIBLE);

                        mCurrentProduct = mPath.getRecommendations().get(
                                recommendation.getOriginalListIndex()).getFirst();
                    }

                    @Override
                    public void handle(MapHeader header) {
                        Log.i(TAG, "HANDLE HEADER");
                    }
                });
        mDrawerRv.setAdapter(mDrawerAdapter);
        mDrawerRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void addComponentToMap(PathComponent component) {
        if(mFrameLayout != null) {
            mFrameLayout.addView(component);
        }
    }

    private void addHighlightToMap(MapHighlight highlight) {
        removeHighlightFromMap();

        if(mFrameLayout != null) {
            mFrameLayout.addView(highlight);
            mHighlights.add(highlight);
            mHighlightIndex++;
        }
    }

    private void removeHighlightFromMap() {
        if(mHighlightIndex != -1) {
            mHighlights.get(mHighlightIndex).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        try {
            Bundle args = getIntent().getBundleExtra(getString(R.string.key_args));
            mShoppingList = (ShoppingList) args.getSerializable(getString(R.string.key_list_id));
            if(mShoppingList == null) {
                throw new UnsupportedOperationException();
            }
        } catch (Exception e) {
            Log.e(TAG, "HOW DID I GET HERE?");
            e.printStackTrace();
        }

        enableFullscreen();
        setContentView(R.layout.activity_map);
        initViews();

        mAnimationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                showComponent();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animateComponent();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPath();
    }

    private void createMenu() {
        Log.i(TAG, "Creating menu");
        List<MapProduct> mapProducts = new ArrayList<>();
        List<MapSale> mapSales = new ArrayList<>();
        List<MapRecommendation> mapRecommendations = new ArrayList<>();
        List<DrawerEntry> menuEntries = new ArrayList<>();

        MapHeader headerProducts = new MapHeader("PRODUCTS", MapHeader.HEADER_TYPE_PRODUCT);
        MapHeader headerSales = new MapHeader("SALES", MapHeader.HEADER_TYPE_SALES);
        MapHeader headerRecomm = new MapHeader("RECOMMENDATIONS", MapHeader.HEADER_TYPE_RECOMMENDATION);

        for(Pair<Product, Point2D> pair : mPath.getListItems()) {
            mapProducts.add(new MapProduct(
                    (pair.getSecond().getX() + X_OFFSET) * RATIO,
                    (pair.getSecond().getY() + Y_OFFSET) * RATIO,
                    pair.getFirst().getName()
            ));
        }

        if(mPath.getRecommendations() != null) {
            for (int i = 0; i < mPath.getRecommendations().size(); i++) {
                Pair<Product, Point2D> pair = mPath.getRecommendations().get(i);
                mapRecommendations.add(new MapRecommendation(
                        i,
                        (pair.getSecond().getX() + X_OFFSET) * RATIO,
                        (pair.getSecond().getY() + Y_OFFSET) * RATIO,
                        pair.getFirst().getName()
                ));
            }
        }

        if(mPath.getSales() != null) {
            for (int i = 0; i < mPath.getSales().size(); i++) {
                Pair<Product, Point2D> pair = mPath.getSales().get(i);
                mapSales.add(new MapSale(
                        i,
                        (pair.getSecond().getX() + X_OFFSET) * RATIO,
                        (pair.getSecond().getY() + Y_OFFSET) * RATIO,
                        pair.getFirst().getName()
                ));
            }
        }

        menuEntries.add(headerSales);
        menuEntries.addAll(mapSales);
        menuEntries.add(headerRecomm);
        menuEntries.addAll(mapRecommendations);
        menuEntries.add(headerProducts);
        menuEntries.addAll(mapProducts);
        mDrawerAdapter.setContent(menuEntries);
    }

    private void drawPath() {
        Log.i(TAG, "DRAWING PATH");
        List<Point2D> points = mPath.getPathCoordinates();
        for(int i = 0; i < points.size()-1; i++) {
            Point2D startPoint = points.get(i);
            Point2D endPoint = points.get(i+1);
            Log.d(TAG, "START: " + startPoint.toString() + " END: " + endPoint.toString());

            PathComponent component = new PathComponent(
                    this,
                    (startPoint.getX() + X_OFFSET) * RATIO,
                    (startPoint.getY() + Y_OFFSET) * RATIO,
                    (endPoint.getX() + X_OFFSET) * RATIO,
                    (endPoint.getY() + Y_OFFSET) * RATIO,
                    i == 0
            );
            component.setVisibility(View.GONE);

            Animation componentAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
            componentAnim.setAnimationListener(mAnimationListener);
            mAnimations.add(componentAnim);
            mComponents.add(component);
        }

        animateComponent();
    }

    private void animateComponent() {
        if(mCurrentComponent < mComponents.size()) {
            addComponentToMap(mComponents.get(mCurrentComponent));
            mComponents.get(mCurrentComponent).startAnimation(mAnimations.get(mCurrentComponent));
        } else {
            Log.d(TAG, "DONE");
        }
    }

    private void showComponent() {
        mComponents.get(mCurrentComponent).setVisibility(View.VISIBLE);
        mCurrentComponent++;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int[] startCoords = new int[2];
        mIvMap.getLocationOnScreen(startCoords);
        Log.i(TAG, startCoords[0] + "");
        Log.i(TAG, startCoords[1] + "");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void enableFullscreen() {
        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void saveList() {
        final ListItemDao itemDao = new ListItemDao(this);
        final ProductDao productDao = new ProductDao(this);

        mProgressBar.setVisibility(View.VISIBLE);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Log.d(TAG, "ASYNC ADDING PRODUCTS");
                String token = IdentityHandler.getRetailorToken(getBaseContext());
                VolleyQueueSingleton.getInstance(getBaseContext()).addRequest(new VolleyRequest<>(
                        NetUtils.getShoppingListsUrl(),
                        ShoppingList.class,
                        mShoppingList,
                        NetUtils.getAuthenticationHeader(token),
                        new Response.Listener<ShoppingList>() {
                            @Override
                            public void onResponse(ShoppingList response) {
                                Log.i(TAG, "SAVED: INSERTING NEW PRODUCTS");
                                List<Product> newProducts = extractProductsFromList(response);
                                productDao.bulkInsertWithIndexCheck(newProducts);

                                Log.i(TAG, "SAVED: CLEANING LIST; INSERTING NEW ITEMS");
                                itemDao.deleteFromList(mShoppingList.getId());
                                itemDao.bulkInsert(response.getProducts());

                                mProgressBar.setVisibility(View.GONE);

                                prepareNewPath();
                                getPath();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                mProgressBar.setVisibility(View.GONE);
                                ErrorHandler.handleVolleyError(error, getBaseContext());
                            }
                        }
                ).setTag(getString(R.string.volley_tag_items)));
                return null;
            }
        }.execute();
    }

    private void prepareNewPath() {
        for(PathComponent component : mComponents) {
            component.clearAnimation();
            component.setVisibility(View.GONE);
        }

        mCurrentProduct = null;
        mComponents = new ArrayList<>();
        mAnimations = new ArrayList<>();
        mHighlights = new ArrayList<>();
        mCurrentComponent = 0;
        mHighlightIndex = -1;
        mDirtyList = false;
        mHighlightInfo.setVisibility(View.GONE);
        mRefresh.clearAnimation();
        mRefresh.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
    }


    private void getPath() {
        mProgressBar.setVisibility(View.VISIBLE);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                VolleyQueueSingleton.getInstance(getBaseContext()).addRequest(new VolleyRequest<>(
                        NetUtils.getPathUrl(mShoppingList.getId(), CMX_IP),
                        ComputedPath.class,
                        NetUtils.getAuthenticationHeader(IdentityHandler.getRetailorToken(getBaseContext())),
                        new Response.Listener<ComputedPath>() {
                            @Override
                            public void onResponse(ComputedPath response) {
                                Log.i(TAG, "RECEIVED PATH");
                                mPath = response;
                                createMenu();
                                drawPath();
                                mProgressBar.setVisibility(View.GONE);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ErrorHandler.handleVolleyError(error, getBaseContext());
                                mProgressBar.setVisibility(View.GONE);
                            }
                        }
                ));
                return null;
            }
        }.execute();
    }

    private List<Product> extractProductsFromList(ShoppingList list) {
        List<Product> products = new ArrayList<>();
        for(ListItem listItem : list.getProducts()) {
            products.add(listItem.getProduct());
            listItem.setShoppingList(mShoppingList);
        }
        return products;
    }

    public interface ActivityCallback {
        void draw(MapProduct product);
        void draw(MapSale sale);
        void draw(MapRecommendation recommendation);
        void handle(MapHeader header);
    }
}
