package com.ciuta.ionut.retailor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.fragments.ProductCategoryListFragment;
import com.ciuta.ionut.retailor.pojo.Cart;
import com.ciuta.ionut.retailor.pojo.Product;
import com.ciuta.ionut.retailor.pojo.Venue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class CatalogueActivity extends ContainerActivity {
    private final String TAG = this.getClass().getSimpleName();
    private List<Product> selectedProducts = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_catalogue, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_done:
                Log.d(TAG, "DONE ADDING PRODUCTS");
                Bundle args = new Bundle();
                args.putSerializable(getString(R.string.key_added_products), new Cart(selectedProducts));

                Intent result = new Intent();
                result.putExtra(getString(R.string.key_args), args);

                setResult(RESULT_OK, result);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void setup(Intent intent) {
        if(hasArgs(intent)) {
            Bundle args = getArgs(intent);
            Venue venue = (Venue) args.getSerializable(getString(R.string.key_venue));

            if(venue == null) {
                Log.e(TAG, "NO VENUE SET");
                finish();
                return;
            }

            setFragment(ProductCategoryListFragment.getInstance(
                venue.getId(),
                new FragmentHandler() {
                    @Override
                    public void handle(Fragment fragment) {
                        Log.d(TAG, "CHANGING FRAGMENT");
                        setFragmentAndEnableBack(fragment);
                    }
                },
                new ProductHandler() {
                    @Override
                    public void add(Product product) {
                        Log.d(TAG, "ADDING PRODUCT: " + product.getName());
                        selectedProducts.add(product);
                        Log.d(TAG, selectedProducts.toString());
                    }

                    @Override
                    public void remove(Product product) {
                        Log.d(TAG, "REMOVING PRODUCT: " + product.getName());
                        selectedProducts.remove(product);
                        Log.d(TAG, selectedProducts.toString());
                    }
                })
            );
        }
    }

    public interface ProductHandler {
        void add(Product product);
        void remove(Product product);
    }

    public interface FragmentHandler {
        void handle(Fragment fragment);
    }
}
