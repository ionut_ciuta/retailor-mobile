package com.ciuta.ionut.retailor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.ciuta.ionut.retailor.R;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class ContainerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        setup(getIntent());
    }

    /*An activity of this kind should always receive an entity as argument*/
    protected boolean hasArgs(Intent intent) {
        return intent.hasExtra(getString(R.string.key_args));
    }

    /*An activity of this kind should always receive an entity as argument*/
    protected Bundle getArgs(Intent intent) {
        return intent.getBundleExtra(getString(R.string.key_args));
    }

    /*Used to set the content fragment*/
    protected abstract void setup(Intent intent);

    /*Used to set the primary fragment*/
    protected void setFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_content, fragment)
                .commit();
    }

    protected void setFragmentAndEnableBack(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_content, fragment)
                .addToBackStack(null)
                .commit();
    }
}
