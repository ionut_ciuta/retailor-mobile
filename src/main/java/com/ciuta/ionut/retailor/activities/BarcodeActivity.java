package com.ciuta.ionut.retailor.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.db.dao.NotificationDao;
import com.ciuta.ionut.retailor.pojo.notifications.DiscountNotification;
import com.ciuta.ionut.retailor.util.ErrorHandler;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class BarcodeActivity extends ContainerActivity {
    private final String TAG = getClass().getSimpleName();
    private final int DISCOUNT_VALLABILITY = 30000;
    private final int DISCOUNT_VALABILITY_DECREMENT = 1000;

    private DiscountNotification mDiscountNotification;
    private Toolbar mToolbar;
    private Boolean mHandled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setupToolbar(getString(R.string.title_discount_barcode));

        ImageView mIvBarcode = (ImageView)findViewById(R.id.iv_code);
        try {
            Bitmap bitmap = encodeAsBitmap(mDiscountNotification.getCode(),
                                           BarcodeFormat.CODE_128, 600, 300);
            mIvBarcode.setImageBitmap(bitmap);
        } catch (Exception e) {
            ErrorHandler.handleException(e);
        }

        TextView mTvCode = (TextView)findViewById(R.id.tv_code);
        mTvCode.setText(mDiscountNotification.getCode());

        final TextView mTvCounter = (TextView)findViewById(R.id.tv_counter);
        new CountDownTimer(DISCOUNT_VALLABILITY, DISCOUNT_VALABILITY_DECREMENT) {

            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                Long counter = millisUntilFinished / DISCOUNT_VALABILITY_DECREMENT;
                if (mTvCounter != null) {
                    mTvCounter.setText(counter.toString());
                }
            }

            public void onFinish() {
                if (mTvCounter != null) {
                    mTvCounter.setText(getString(R.string.message_expired));
                    //handleTimeOver();
                }
            }
        }.start();
    }

    @Override
    protected void onDestroy() {
        if(!mHandled) {
            //new NotificationDao(this).delete(mDiscountNotification);
        }

        Log.d(TAG, "FINISH BARCODE ACTIVITY");
        super.onDestroy();
    }

    private void handleTimeOver() {
        Log.d(TAG, "TIME OVER");
        mHandled = true;
        new NotificationDao(this).delete(mDiscountNotification);
        finish();
    }

    @Override
    protected void setup(Intent intent) {
        Bundle args = intent.getBundleExtra(getString(R.string.key_args));
        if(args != null && args.containsKey(getString(R.string.key_discount))) {
            mDiscountNotification = (DiscountNotification) args.get(getString(R.string.key_discount));
        } else {
            throw new UnsupportedOperationException();
        }
    }

    protected void setupToolbar(String title) {
        if(mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            if(title != null) {
                getSupportActionBar().setTitle(title);
            }

        } else {
            throw new UnsupportedOperationException();
        }
    }

    private Bitmap encodeAsBitmap(String code, BarcodeFormat format, int width, int height) {
        final int WHITE = 0xFFFFFFFF;
        final int BLACK = 0xFF000000;

        Map<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");

        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix resultMatrix = null;
        try {
            resultMatrix = writer.encode(code, format, width, height, hints);
        } catch (WriterException e) {
            ErrorHandler.handleException(e);
        }

        int resultWidth = resultMatrix.getWidth();
        int resultHeight = resultMatrix.getHeight();

        int[] pixels = new int[resultWidth * resultHeight];
        for (int y = 0; y < resultHeight; y++) {
            int offset = y * resultWidth;
            for (int x = 0; x < resultWidth; x++) {
                pixels[offset + x] = resultMatrix.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap resultBitmap = Bitmap.createBitmap(resultWidth, resultHeight, Bitmap.Config.ARGB_8888);
        resultBitmap.setPixels(pixels, 0, resultWidth, 0, 0, resultWidth, resultHeight);
        return resultBitmap;
    }
}
