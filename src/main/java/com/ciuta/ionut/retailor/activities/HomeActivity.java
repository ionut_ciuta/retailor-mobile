package com.ciuta.ionut.retailor.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.db.RetailorDBContract;
import com.ciuta.ionut.retailor.db.dao.UserDao;
import com.ciuta.ionut.retailor.fcm.FCMRegistrationService;
import com.ciuta.ionut.retailor.fragments.NotificationsHomeFragment;
import com.ciuta.ionut.retailor.fragments.ShoppingListsHomeFragment;
import com.ciuta.ionut.retailor.fragments.VenuesHomeFragment;
import com.ciuta.ionut.retailor.pojo.User;
import com.ciuta.ionut.retailor.util.NetUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.squareup.picasso.Picasso;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class HomeActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{
    private final String TAG = this.getClass().getSimpleName();
    private final int USER_LOADER = 0;

    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawer;
    private NavigationView mNavView;
    private ImageView mAvatarImageView;
    private TextView mUserFullName, mUserEmail;

    private boolean freshActivity = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");

        if(checkPlayServices()) {
            Log.i(TAG, "STARTING FCMRegistrationService");
            Intent intent = new Intent(this, FCMRegistrationService.class);
            startService(intent);
        }

        setContentView(R.layout.activity_home);
        getSupportLoaderManager().initLoader(USER_LOADER, null, this);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawer = (DrawerLayout) findViewById(R.id.layout_drawer);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar,
                R.string.title_dashboard, R.string.title_dashboard);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawer.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mNavView = (NavigationView)findViewById(R.id.view_navigation);
        mNavView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem item) {
                        return onDrawerItemSelected(item);
                    }
                });

        mAvatarImageView = (ImageView)mNavView.getHeaderView(0).findViewById(R.id.avatar);
        mUserFullName = (TextView)mNavView.getHeaderView(0).findViewById(R.id.tv_user_fullname);
        mUserEmail = (TextView)mNavView.getHeaderView(0).findViewById(R.id.tv_user_email);

        if(freshActivity) {
            setFragment(new NotificationsHomeFragment(), false);
            freshActivity = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        return new CursorLoader(
                this,
                RetailorDBContract.UserEntry.CONTENT_URI,
                new String[]{"_id", "firstname", "lastname", "mail"},
                "status = ?",
                new String[]{User.USER_ACTIVE.toString()},
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        if(data.getCount() > 0) {
            Log.i(TAG, "SETTING DRAWER INFO");
            data.moveToFirst();
            mUserFullName.setText(data.getString(1) + " " + data.getString(2));
            mUserEmail.setText(data.getString(3));

            Picasso.with(this)
                    .load(NetUtils.getUserIconUrl(data.getLong(0)))
                    .error(R.drawable.placeholder_user)
                    .into(mAvatarImageView);
        } else {
            Log.e(TAG, "NO ACTIVE USERS");
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
    }

    private boolean onDrawerItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.go_to_home:
                setFragment(new NotificationsHomeFragment(), true);
                break;

            case R.id.go_to_lists:
                setFragment(new ShoppingListsHomeFragment(), true);
                break;

            case R.id.go_to_venues:
                setFragment(new VenuesHomeFragment(), true);
                break;

            case R.id.go_to_settings:
                break;

            case R.id.sign_out:
                new SignOutTask(this).execute(true);
                break;
        }

        mDrawer.closeDrawers();
        return true;
    }

    private void setFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.layout_fragment_frame, fragment);

        if(addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

    /**
     * Async task user for executing the sign out procedure
     */
    private class SignOutTask extends AsyncTask<Boolean, Void, Boolean> {
        private Context mContext;

        public SignOutTask(Context context) {
            this.mContext = context;
        }

        AccountManager accountManager = AccountManager.get(getBaseContext());
        Account[] accounts = accountManager.getAccountsByType(getString(R.string.retailor_account_type));

        @Override
        protected Boolean doInBackground(Boolean... params) {
            if(accounts.length == 1) {
                Log.i(TAG, "CURRENT ACCOUNT: " + accounts[0].name);
                String token = accountManager.peekAuthToken(
                                accounts[0], getString(R.string.auth_token_type_full));
                accountManager.invalidateAuthToken(
                                getString(R.string.retailor_account_type), token);
                new UserDao(mContext).deactivateUser();
                Log.i(TAG, "USER DEACTIVATED");
            } else {
                // TODO: 10.06.2016 handle multiple accounts
                throw new UnsupportedOperationException("Multiple accounts!");
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Log.i(TAG, "Async task done sleeping");
            Bundle authBundle = new Bundle();
            authBundle.putBoolean(getString(R.string.auth_arg_new), false);
            authBundle.putBoolean(getString(R.string.auth_arg_go_home), true);
            authBundle.putString(getString(R.string.auth_arg_account_type), accounts[0].type);
            authBundle.putString(getString(R.string.auth_arg_account_name), accounts[0].name);
            authBundle.putString(getString(R.string.auth_arg_token_type), getString(R.string.auth_token_type_full));
            Intent intent = new Intent(mContext, AuthenticationActivity.class);
            intent.putExtras(authBundle);
            startActivity(intent);
            finish();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS) {
            if(apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 9000).show();
            } else {
                Log.i(TAG, "DEVICES IS NOT SUPPORTED");
            }
            return false;
        }
        return true;
    }
}
