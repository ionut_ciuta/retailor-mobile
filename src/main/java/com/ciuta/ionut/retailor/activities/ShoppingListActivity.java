package com.ciuta.ionut.retailor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.fragments.ShoppingListDetailFragment;
import com.ciuta.ionut.retailor.fragments.ShoppingListFormFragment;
import com.ciuta.ionut.retailor.pojo.ShoppingList;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingListActivity extends ContainerActivity {

    @Override
    protected void setup(Intent intent) {
        if(hasArgs(intent)) {
            Bundle args = getArgs(intent);

            if(args.containsKey(getString(R.string.key_view_shopping_list))) {
                Object arg = args.getSerializable(getString(R.string.key_view_shopping_list));
                setFragment(ShoppingListDetailFragment.getInstance(
                        (ShoppingList)arg,
                        new FragmentHandler() {
                            @Override
                            public void handle(Fragment fragment) {
                                setFragmentAndEnableBack(fragment);
                            }
                        }));
                return;
            }

            if(args.containsKey(getString(R.string.key_edit_shopping_list))) {
                Object arg = args.getSerializable(getString(R.string.key_edit_shopping_list));
                setFragment(ShoppingListFormFragment.getInstance((ShoppingList)arg));
                return;
            }
        }
    }

    public interface FragmentHandler {
        void handle(Fragment fragment);
    }
}
