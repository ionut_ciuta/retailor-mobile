package com.ciuta.ionut.retailor.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ciuta.ionut.retailor.R;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class NavigatorActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountManager accountManager = AccountManager.get(this);
        Account[] accounts = accountManager.getAccounts();
        Intent intent;

        if(accounts.length == 0) {
            Log.d(TAG, "NO ACCOUNTS");
            Bundle authBundle = new Bundle();
            authBundle.putString(getString(R.string.auth_arg_account_type),
                                 getString(R.string.retailor_account_type));
            authBundle.putString(getString(R.string.auth_arg_token_type),
                                 getString(R.string.auth_token_type_full));
            authBundle.putBoolean(getString(R.string.auth_arg_new), true);
            authBundle.putBoolean(getString(R.string.auth_arg_go_home), true);

            intent = new Intent(this, AuthenticationActivity.class);
            intent.putExtras(authBundle);
        } else if (accounts.length == 1){
            String token = accountManager.peekAuthToken(
                            accounts[0],getString(R.string.auth_token_type_full));

            if(token == null) {
                Log.i(TAG, accounts[0].name + " NOT SIGNED IN!");
                Bundle authBundle = new Bundle();
                authBundle.putString(getString(R.string.auth_arg_account_type), accounts[0].type);
                authBundle.putString(getString(R.string.auth_arg_account_name), accounts[0].name);
                authBundle.putBoolean(getString(R.string.auth_arg_new), false);
                authBundle.putBoolean(getString(R.string.auth_arg_go_home), true);
                authBundle.putString(getString(R.string.auth_arg_token_type),
                        getString(R.string.auth_token_type_full));

                intent = new Intent(this, AuthenticationActivity.class);
                intent.putExtras(authBundle);
            } else {
                intent = new Intent(this, HomeActivity.class);
            }

        } else {
            // TODO: 10.06.2016 maybe take care of this
            throw new UnsupportedOperationException("Multiple accounts");
        }

        startActivity(intent);
        finish();
    }
}
