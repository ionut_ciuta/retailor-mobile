package com.ciuta.ionut.retailor.volley;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VolleyRequest<T> extends JsonRequest<T> {
    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private final Map<String, String> headers;

    /**
     * Base constructor called by the others; whole point is to convert json to pojo at the end
     * @param method        GET, POST etc.
     * @param url           endpoint; should use NetUtils
     * @param clazz         used to parse Json from server
     * @param requestBody   object sent to server
     * @param headers       headers used
     * @param listener      response handler
     * @param errorListener error handler
     */
    public VolleyRequest(int method, String url, Class<T> clazz,
                         String requestBody, Map<String, String> headers,
                         Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        this.clazz = clazz;
        this.headers = headers;
    }

    /**
     * Constructor called to send to server directly by using the pojo
     * @param method        GET, POST etc.
     * @param url           endpoint; should use NetUtils
     * @param clazz         used to parse Json from server
     * @param requestBody   object sent to server
     * @param headers       headers used
     * @param listener      response handler
     * @param errorListener error handler
     */
    public VolleyRequest(int method, String url, Class<T> clazz,
                         Object requestBody, Map<String, String> headers,
                         Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(method, url, clazz, new Gson().toJson(requestBody), headers, listener, errorListener);
    }

    /**
     * Constructor for generic GET, without auth
     * @param url           endpoint; should use NetUtils
     * @param clazz         used to parse Json from server
     * @param listener      response handler
     * @param errorListener error handler
     */
    public VolleyRequest(String url, Class<T> clazz,
                         Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(Method.GET, url, clazz, null, null, listener, errorListener);
    }

    /**
     * Constructor for generic GET, with auth
     * @param url
     * @param clazz
     * @param headers
     * @param listener
     * @param errorListener
     */
    public VolleyRequest(String url, Class<T> clazz, HashMap<String, String> headers,
                         Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(Method.GET, url, clazz, null, headers, listener, errorListener);
    }

    /**
     * Constructor for generic POST with authentication
     * @param url
     * @param clazz
     * @param pojo
     * @param listener
     * @param errorListener
     */
    public VolleyRequest(String url, Class<T> clazz, Object pojo,
                         Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(Method.POST, url, clazz, pojo, null, listener, errorListener);
    }

    /**
     * Constructor for generic POST with authentication
     * @param url
     * @param clazz
     * @param pojo
     * @param headers
     * @param listener
     * @param errorListener
     */
    public VolleyRequest(String url, Class<T> clazz, Object pojo, HashMap<String, String> headers,
                         Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(Method.POST, url, clazz, pojo, headers, listener, errorListener);
    }

    /*public static <T> VolleyRequest<T> get(String url, Class<T> clazz,
                                           Response.Listener<T> listener,
                                           Response.ErrorListener errorListener) {
        return new VolleyRequest<T>(Method.GET, url, clazz, null, listener, errorListener);
    }

    public static <T> VolleyRequest<T> post(String url, Class clazz,
                                            Response.Listener<T> listener,
                                            Response.ErrorListener errorListener) {
        return new VolleyRequest<T>(Method.POST, url, clazz, null, listener, errorListener);
    }*/

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));

            return Response.success(
                    gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }
}