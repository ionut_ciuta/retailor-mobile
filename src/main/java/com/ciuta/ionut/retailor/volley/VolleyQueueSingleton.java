package com.ciuta.ionut.retailor.volley;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Ionut on 4/17/2016.
 */
public class VolleyQueueSingleton {

    private static VolleyQueueSingleton mInstance;
    private static RequestQueue mRequestQueue;
    //private ImageLoader mImageLoader;
    private static Context mContext;

    private VolleyQueueSingleton(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyQueueSingleton getInstance(Context context) {
        if(mInstance == null) {
            return mInstance = new VolleyQueueSingleton(context);
        }

        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if(mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addRequest(Request<T> request) {
        getRequestQueue().add(request);
    }

    public static void cancelRequest(String tag) {
        mRequestQueue.cancelAll(tag);
    }
}