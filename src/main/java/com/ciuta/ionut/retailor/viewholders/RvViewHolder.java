package com.ciuta.ionut.retailor.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ciuta.ionut.retailor.adapters.RvAdapter;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class RvViewHolder<T> extends RecyclerView.ViewHolder
                        implements View.OnClickListener {
    protected RvAdapter.OnItemClickListener rvOnClickListener;

    public RvViewHolder(View itemView, RvAdapter.OnItemClickListener listener) {
        super(itemView);
        this.rvOnClickListener = listener;
    }

    public abstract void configure(T obj);
}
