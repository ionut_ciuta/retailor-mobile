package com.ciuta.ionut.retailor.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvCursorAdapter;
import com.ciuta.ionut.retailor.pojo.Venue;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueCursorViewHolder extends RvCursorViewHolder<Venue> {
    public TextView name, address;
    public ImageView background;

    public VenueCursorViewHolder(View itemView, RvCursorAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        name = (TextView) itemView.findViewById(R.id.venue_name);
        address = (TextView) itemView.findViewById(R.id.venue_address);
        background = (ImageView) itemView.findViewById(R.id.iv_venue);
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(Venue venue) {
        name.setText(venue.getName());
        address.setText(venue.getAddress());
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(
                view.getContext(),
                name.getText().toString(),
                Toast.LENGTH_SHORT)
                .show();
        super.rvOnClickListener.onClickHandle(getAdapterPosition());
    }
}
