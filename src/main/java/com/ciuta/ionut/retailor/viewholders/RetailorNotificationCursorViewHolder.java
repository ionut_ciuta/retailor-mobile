package com.ciuta.ionut.retailor.viewholders;

import android.view.View;

import com.ciuta.ionut.retailor.adapters.RvCursorAdapter;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotification;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class RetailorNotificationCursorViewHolder
        extends RvCursorViewHolder<RetailorNotification> {

    public RetailorNotificationCursorViewHolder(View itemView,
                                                RvCursorAdapter.OnItemClickListener listener) {
        super(itemView, listener);
    }
}
