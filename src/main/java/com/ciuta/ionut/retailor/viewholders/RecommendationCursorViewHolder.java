package com.ciuta.ionut.retailor.viewholders;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.NotificationCursorAdapter.NotificationActionHandler;
import com.ciuta.ionut.retailor.pojo.notifications.RecommendationNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotification;

import java.util.Locale;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class RecommendationCursorViewHolder extends RetailorNotificationCursorViewHolder {
    private final String TAG = getClass().getSimpleName();
    private TextView message;
    private Button positive, negative;
    private NotificationActionHandler actionHandler;

    public RecommendationCursorViewHolder(View itemView, NotificationActionHandler handler) {
        super(itemView, null);
        actionHandler = handler;
        message = (TextView)itemView.findViewById(R.id.tv_recommendation_message);
        positive = (Button)itemView.findViewById(R.id.button_recommendation_use);
        negative = (Button)itemView.findViewById(R.id.button_recommendation_dismiss);
        positive.setOnClickListener(this);
        negative.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void configure(RetailorNotification obj) {
        RecommendationNotification notification = (RecommendationNotification) obj;
        message.setText("Would you like to add " + notification.getProductName() +
                        " to your list " + notification.getShoppingListName() +
                        " for only " + String.format(Locale.ENGLISH, "%.2f", notification.getProductPrice()) + " RON?");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_recommendation_dismiss:
                Log.d(TAG, "RECOMMENDATION ACTION");
                actionHandler.dismiss(getAdapterPosition());
                break;

            case R.id.button_recommendation_use:
                Log.d(TAG, "RECOMMENDATION DISMISS");
                actionHandler.handleAction(getAdapterPosition());
                break;
        }
    }
}
