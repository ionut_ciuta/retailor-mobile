package com.ciuta.ionut.retailor.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvCursorAdapter;
import com.ciuta.ionut.retailor.pojo.ProductCategory;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductCategoryCursorViewHolder extends RvCursorViewHolder<ProductCategory> {
    private final String TAG = this.getClass().getSimpleName();
    public TextView name;
    public ImageView image;

    public ProductCategoryCursorViewHolder(View itemView,
                                           RvCursorAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        name = (TextView)itemView.findViewById(R.id.category_name);
        image = (ImageView) itemView.findViewById(R.id.iv_category);
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(ProductCategory obj) {
        name.setText(obj.getName());
    }

    @Override
    public void onClick(View v) {
        super.rvOnClickListener.onClickHandle(getAdapterPosition());
    }
}
