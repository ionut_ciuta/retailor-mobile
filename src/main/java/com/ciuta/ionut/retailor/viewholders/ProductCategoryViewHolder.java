package com.ciuta.ionut.retailor.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvAdapter;
import com.ciuta.ionut.retailor.pojo.ProductCategory;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ProductCategoryViewHolder extends RvViewHolder<ProductCategory> {
    private TextView name;
    private ImageView image;

    public ProductCategoryViewHolder(View itemView, RvAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        name = (TextView)itemView.findViewById(R.id.category_name);
        image = (ImageView) itemView.findViewById(R.id.iv_category);
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(ProductCategory obj) {
        name.setText(obj.getName());
    }

    @Override
    public void onClick(View view) {
        super.rvOnClickListener.onClickHandler(getAdapterPosition());
    }
}
