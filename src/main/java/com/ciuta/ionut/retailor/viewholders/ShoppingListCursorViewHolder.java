package com.ciuta.ionut.retailor.viewholders;

import android.graphics.PorterDuff;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvCursorAdapter;
import com.ciuta.ionut.retailor.pojo.ShoppingList;
import com.ciuta.ionut.retailor.util.StringUtils;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ShoppingListCursorViewHolder extends RvCursorViewHolder<ShoppingList> {
    private TextView name, createdOnDate, venue;
    private ImageView status;

    public ShoppingListCursorViewHolder(View itemView, RvCursorAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        name = (TextView)itemView.findViewById(R.id.shopping_list_name);
        createdOnDate = (TextView)itemView.findViewById(R.id.shopping_list_date);
        venue = (TextView)itemView.findViewById(R.id.shopping_list_venue);
        status = (ImageView) itemView.findViewById(R.id.shopping_list_status_icon);
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(ShoppingList obj) {
        name.setText(obj.getName());
        createdOnDate.setText(StringUtils.formatDate(obj.getCreationDate()));

        if(venue != null) {
            venue.setText(obj.getVenue().getName());
        }

        // TODO: 11.06.2016 check status
        /*if(status != null) {
            if(getLayoutPosition() % 3 == 0) {
                status.setImageResource(R.drawable.ic_incomplete);
                status.setColorFilter(R.color.primary_dark, PorterDuff.Mode.MULTIPLY);
            } else {
                status.setImageResource(R.drawable.ic_thumb);
                status.setColorFilter(R.color.accent, PorterDuff.Mode.MULTIPLY);
            }
        }*/
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        super.rvOnClickListener.onClickHandle(position);
    }
}
