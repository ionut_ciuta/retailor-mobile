package com.ciuta.ionut.retailor.viewholders;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvAdapter;
import com.ciuta.ionut.retailor.map.DrawerEntry;
import com.ciuta.ionut.retailor.map.MapHeader;
import com.ciuta.ionut.retailor.map.MapProduct;
import com.ciuta.ionut.retailor.map.MapRecommendation;
import com.ciuta.ionut.retailor.map.MapSale;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DrawerViewHolder extends RvViewHolder<DrawerEntry> {
    private final String TAG = getClass().getSimpleName();
    protected TextView text;

    public DrawerViewHolder(View itemView, RvAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        text = (TextView) itemView.findViewById(R.id.tv_drawer_item_text);
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(DrawerEntry obj) {
        switch (obj.getType()) {
            case DrawerEntry.TYPE_HEADER:
                Log.i(TAG, "MAP HEADER");
                MapHeader header = (MapHeader) obj;
                text.setText(header.getTitle());
                switch (header.getType()) {
                    case MapHeader.HEADER_TYPE_PRODUCT:
                        text.setTextColor(0xff0277BD);
                        break;

                    case MapHeader.HEADER_TYPE_SALES:
                        text.setTextColor(0xffF44377);
                        break;

                    case MapHeader.HEADER_TYPE_RECOMMENDATION:
                        text.setTextColor(0xffFFA726);
                        break;
                }
                return;

            case DrawerEntry.TYPE_PRODUCT:
                Log.i(TAG, "MAP HEADER");
                text.setText(((MapProduct) obj).getName());
                return;

            case DrawerEntry.TYPE_SALE:
                Log.i(TAG, "MAP HEADER");
                text.setText(((MapSale) obj).getText());
                return;

            case DrawerEntry.TYPE_RECOMMENDATION:
                Log.i(TAG, "MAP HEADER");
                text.setText(((MapRecommendation) obj).getName());
                return;
        }
    }

    @Override
    public void onClick(View v) {
        rvOnClickListener.onClickHandler(getAdapterPosition());
    }
}
