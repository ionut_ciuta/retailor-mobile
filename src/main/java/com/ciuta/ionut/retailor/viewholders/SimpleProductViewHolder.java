package com.ciuta.ionut.retailor.viewholders;

import android.view.View;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.ProductAdapter;
import com.ciuta.ionut.retailor.pojo.Product;

import java.util.Locale;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class SimpleProductViewHolder extends RvViewHolder<Product> {
    private TextView name, details, price;

    public SimpleProductViewHolder(View itemView, ProductAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        name = (TextView)itemView.findViewById(R.id.product_name);
        price = (TextView)itemView.findViewById(R.id.product_price);
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(Product obj) {
        name.setText(obj.getName());
        price.setText(String.format(Locale.ENGLISH, "%.2f", obj.getPrice()) + " RON");
    }

    @Override
    public void onClick(View view) {
        super.rvOnClickListener.onClickHandler(getAdapterPosition());
    }
}
