package com.ciuta.ionut.retailor.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ciuta.ionut.retailor.adapters.RvCursorAdapter;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public abstract class RvCursorViewHolder<T> extends RecyclerView.ViewHolder
                        implements View.OnClickListener {
    protected RvCursorAdapter.OnItemClickListener rvOnClickListener;

    public RvCursorViewHolder(View itemView, RvCursorAdapter.OnItemClickListener listener) {
        super(itemView);
        this.rvOnClickListener = listener;
    }

    public abstract void configure(T obj);

}