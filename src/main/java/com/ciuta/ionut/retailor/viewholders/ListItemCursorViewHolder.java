package com.ciuta.ionut.retailor.viewholders;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvCursorAdapter;
import com.ciuta.ionut.retailor.pojo.ListItem;

import java.util.Locale;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class ListItemCursorViewHolder extends RvCursorViewHolder<ListItem> {
    private TextView name, price;

    public ListItemCursorViewHolder(View itemView, RvCursorAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        name = (TextView)itemView.findViewById(R.id.product_name);
        price = (TextView)itemView.findViewById(R.id.product_price);
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(ListItem obj) {
        name.setText(obj.getProduct().getName());
        price.setText(String.format(Locale.ENGLISH, "%.2f", obj.getProduct().getPrice()) + " RON");

        if(obj.getTaken()) {
            price.setTextColor(Color.parseColor("#FFA726"));
        }
    }

    @Override
    public void onClick(View v) {
        super.rvOnClickListener.onClickHandle(getAdapterPosition());
    }
}
