package com.ciuta.ionut.retailor.viewholders;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.NotificationCursorAdapter.NotificationActionHandler;
import com.ciuta.ionut.retailor.adapters.RvCursorAdapter;
import com.ciuta.ionut.retailor.pojo.notifications.DiscountNotification;
import com.ciuta.ionut.retailor.pojo.notifications.RetailorNotification;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class DiscountCursorViewHolder extends RetailorNotificationCursorViewHolder {
    private final String TAG = getClass().getSimpleName();

    private TextView message;
    private Button positive, negative;
    private NotificationActionHandler actionHandler;

    public DiscountCursorViewHolder(View itemView, NotificationActionHandler handler) {
        super(itemView, null);
        actionHandler = handler;
        message = (TextView) itemView.findViewById(R.id.tv_discount_message);
        positive = (Button) itemView.findViewById(R.id.button_discount_use);
        negative = (Button) itemView.findViewById(R.id.button_discount_dismiss);
        positive.setOnClickListener(this);
        negative.setOnClickListener(this);
    }

    @Override
    public void configure(RetailorNotification obj) {
        message.setText(((DiscountNotification)obj).getMessage());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_discount_use:
                Log.d(TAG, "DISCOUNT ACTION");
                actionHandler.handleAction(getAdapterPosition());
                break;

            case R.id.button_discount_dismiss:
                Log.d(TAG, "DISCOUNT DISMISS");
                actionHandler.dismiss(getAdapterPosition());
                break;
        }
    }
}
