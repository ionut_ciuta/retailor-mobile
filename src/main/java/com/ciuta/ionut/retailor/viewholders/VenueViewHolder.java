package com.ciuta.ionut.retailor.viewholders;

import android.view.View;
import android.widget.TextView;

import com.ciuta.ionut.retailor.R;
import com.ciuta.ionut.retailor.adapters.RvAdapter;
import com.ciuta.ionut.retailor.pojo.Venue;

/**
 * Created by Ciuta Ioan Gratian as part of Retailor Diploma Project.
 */
public class VenueViewHolder extends RvViewHolder<Venue> {
    private TextView mVenueName, mVenueAddress;
    private RvAdapter.OnItemClickListener mListener;

    public VenueViewHolder(View itemView, RvAdapter.OnItemClickListener listener) {
        super(itemView, listener);
        mVenueName = (TextView)itemView.findViewById(R.id.tv_venue_name);
        mVenueAddress = (TextView)itemView.findViewById(R.id.tv_venue_address);
        mListener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void configure(Venue obj) {
        mVenueName.setText(obj.getName());
        mVenueAddress.setText(obj.getAddress());
    }

    @Override
    public void onClick(View v) {
        mListener.onClickHandler(getAdapterPosition());
    }
}
